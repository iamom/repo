/************************************************************************
 PROJECT:        Horizon CSD
 MODULE NAME:    creport.c

 DESCRIPTION:    Reporting  Routines JANUS

 ----------------------------------------------------------------------
 Copyright (c) 2016 Pitney Bowes Inc.
 37 Executive Drive
 Danbury, Connecticut  06810
 ----------------------------------------------------------------------
 * HISTORY:

 13-Apr-18 sa002pe on FPHX 02.12 shelton branch
 Commented out fnfReadDebitCertParmsSplitFont.  Never used.

 22-May-17 Jennifer Hao on FPHX 02.12 cienet branch
 Commented out fnfCurrentDateTime & fnfFlexDebitSOMAscii because they aren't used by G9.
 *
 * 2018.08.13 CWB - In the tFieldOpXRef conversion table:
 *	   	+ Fixed the conversion type for these operator types because it was still set for a BE processor:
 *					DRFOP_BE_HEX_2,
 *					DRFOP_BE_HEX_4,
 * 	   	+ Fixed the conversion type of these operator types because the name was changed:
 *					DRFOP_BE_INT
 *					DRFOP_LE_INT
 *	 	+ Added new entries for these operator types, because they are supported:
 *					DRFOP_LE_HEX_2,
 *					DRFOP_LE_HEX_4,
 *					DRFOP_BE_SINT,
 *					DRFOP_LE_SINT,
 *	 - Removed the "Special Case" for PieceCount in fnfReadPSDParms() because the
 *	   normal conversion has been fixed to work correctly.
 *
 **************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "global.h"
#include "ctype.h"

#include "asrapi.h"
//#include "batreg.h"
#include "bob.h"
#include "bobutils.h"
#include "cjunior.h"
#include "clock.h"
#include "cm.h"
#include "cmos.h" 
#include "custdat.h"
#include "Cwrapper.h"
#include "datdict.h"
#include "dcapi.h"
//#include "deptaccount.h"
#include "extreports.h"
#include "features.h"
#include "fdrmcptask.h"
#include "FWRAPPER.h"
#include "glob2bob.h"
#include "ipsd.h"
#include "oi_intellilink.h"
//#include "oideptaccount.h"
//#include "oirateservice.h"
//#include "oireport.h"
#include "oit.h"
#include "pbos.h"    
#include "permits.h"
#include "platform.h"
#include "printer.h"

#ifndef K700
#include "psoctask.h"
#endif

//#include "rateSvcDCAI.h"        // fnRateGetIndiciaInfo()
#include "RateAdpt.h"
//#include "ratesvcpublic.h"
#include "report1.h"            // fnFormatTime(), fnFormatDate()
#include "textmsg.h"
#include "unistring.h"
#include "utils.h"              // fnMoney2Unicode()
#include "oifields.h"
#include "hal.h"
#include "flashutil.h"
#include "api_mach_ctrl.h"
#include "settingsmgr.h"

/*#define UNIT_TESTING 1*/

//-------------------------------------------------------------
//  Prototypes for external functions 
//  that are not prototyped in an include file yet:
//-------------------------------------------------------------
// From cmutils.c:
extern UINT8 fnGetPrintSerialNumber(UINT8 *dest);

// From dummy.c:
extern unsigned long fnReadPMVersion(void);

extern unsigned char fnGetSupportedCountryID(void);
extern BOOL fnGetPieceID(UINT32 *ulPieceID);

// From api_match_ctrl.c
extern unichar *fnGetIndiciaString();
extern char* fnGetProdId();

//-------------------------------------------------------------
//  External Variable Declarations
//-------------------------------------------------------------

// From custdat.c:
extern char CMOSPrintedIndiciaNum[PRINTED_INDICIA_NUM_SZ];
extern PF_RECOVERY_LOG CMOSpfDebitLogRec;  //CMOS power fail recovery log record
extern char CMOSCustomerID_RRN[CUSTOMERID_RRN_SZ];  //for Swiss
extern tRefundData CMOS2RefundData;

#if defined (JANUS) && !defined (K700)		// should be Janus or G9
extern EXP_MTR_CMOS_DATA exp_mtr_cmos;
#endif

extern T_CMOSBalanceInquiryLog CMOSBalanceInquiryLog;

// From oireport.c:
extern BOOL GTS_multi_receipt;
//extern USPS_ph_report_this_item *pGTS_multi_receipt_data;
extern unsigned short GTS_receipt_count;
// extern USPS_ph_report_this_item gUSPS_ph_report_this_item_data;   NOT USED

extern char CMOSIndiciaLanguageCode[INDICIA_LANGUAGE_CODE_SZ];

extern ushort usIndiciaPrintFlags;
extern ushort usPermitPrintFlags;
extern ushort usDateTimePrintFlags;

//-------------------------------------------------------------
// Local Macro Definitions:
//-------------------------------------------------------------
#define BOB_TABLE_DUMMY_VAR_ID  0xFFFF

//-------------------------------------------------------------
// Prototypes for local functions:
//-------------------------------------------------------------

// Report Table functions:
UINT16 fnfDummyRptFcn(void * pStrOrValue, UINT16 usVarID);
UINT16 fnfFlexDebitBrazilPostAscii(void * pStrOrValue, UINT16 usVarID);
UINT16 fnfMailingDateFlex(void * pStrOrValue, UINT16 usVarID);
UINT16 fnfAdjustRegisterForFlexDebitAscii(void * pStrOrValue, UINT16 usVarID);
static UINT16 fnfBalanceInquiryStmtVars(void * pStrOrValue, UINT16 usVarID);
static UINT16 fnfDailyTotalsInfoFromDailyLogs(void * pStrOrValue,
		UINT16 usVarID, BOOL fWithCurrencySymbol);
static UINT16 fnfDailyTotalsFromDCAPAdjLogs(void * pStrOrValue, UINT16 usVarID,
		BOOL fWithCurrencySymbol);
static UINT16 fnfGetFormattedDailyTotalsInfo(void * pStrOrValue, UINT16 usVarID,
		BOOL fWithCurrencySymbol);
static UINT16 fnfReadDebitCertParms2(void *pStrOrValue, UINT16 usVarID);
static UINT16 fnfReadDebitCertParms2BcRevrsByteAry(void * pStrOrValue,
		UINT16 usVarID);
//TODO - determine if static or not
UINT16 fnfReadDebitCertParmsBcByteArray(void * pStrOrValue, UINT16 usVarID);

//-------------------------------------------------------------
// Constant Tables:
//-------------------------------------------------------------

// This table correlates:
// variable ID's from a report graphic register descriptor (the index)
// to a bob variable ID (the value), for use in report variable formating
// ala the BOBMainDriver function
const BobsShortTableType BobsReportVariableLookup[] = {
		{ BOB_TABLE_DUMMY_VAR_ID },                   // 0
		{ BOB_TABLE_DUMMY_VAR_ID },                   // 1
		{ BOB_TABLE_DUMMY_VAR_ID },                   // 2
		{ BOB_TABLE_DUMMY_VAR_ID },                   // 3
		{ GET_VLT_ASCENDING_REG },                    // 4
		{ GET_VLT_DESCENDING_REG },                   // 5
		{ GET_VLT_PIECE_COUNTER },                    // 6
		{ BOB_TABLE_DUMMY_VAR_ID },                   // 7
		{ BOB_TABLE_DUMMY_VAR_ID },                   // 8
		{ BOBID_IPSD_MODELNUMBER },                   // 9
		{ BOB_TABLE_DUMMY_VAR_ID },                   // 10
		{ BOB_TABLE_DUMMY_VAR_ID },                   // 11
		{ BOB_TABLE_DUMMY_VAR_ID },                   // 12
		{ BOB_TABLE_DUMMY_VAR_ID },                   // 13
		{ BOB_TABLE_DUMMY_VAR_ID },                   // 14
		{ BOB_TABLE_DUMMY_VAR_ID },                   // 15
		{ BOBID_IPSD_MANUFACTURERID },                // 16
		{ VLT_INDI_FILE_INDICIA_SERIAL },             // 17
		{ BOB_TABLE_DUMMY_VAR_ID },                   // 18
		{ BOB_TABLE_DUMMY_VAR_ID },                   // 19
		{ BOB_TABLE_DUMMY_VAR_ID },                   // 20
		{ BOB_TABLE_DUMMY_VAR_ID },                   // 21
		{ BOB_TABLE_DUMMY_VAR_ID },                   // 22
		{ BOB_TABLE_DUMMY_VAR_ID },                   // 23
		{ BOB_TABLE_DUMMY_VAR_ID },                   // 24
		{ BOB_TABLE_DUMMY_VAR_ID },                   // 25
		{ BOB_TABLE_DUMMY_VAR_ID },                   // 26
		{ BOB_TABLE_DUMMY_VAR_ID },                   // 27
		{ BOB_TABLE_DUMMY_VAR_ID },                   // 28
		{ BOB_TABLE_DUMMY_VAR_ID },                   // 29
		{ GET_VLT_CONTROL_SUM },                      // 30
		{ BOB_TABLE_DUMMY_VAR_ID },                   // 31
		{ BOB_TABLE_DUMMY_VAR_ID },                   // 32
		{ BOB_TABLE_DUMMY_VAR_ID },                   // 33
		{ BOB_TABLE_DUMMY_VAR_ID },                   // 34
		{ BOB_TABLE_DUMMY_VAR_ID },                   // 35
		{ BOB_TABLE_DUMMY_VAR_ID },                   // 36
		{ BOB_TABLE_DUMMY_VAR_ID },                   // 37
		{ BOB_TABLE_DUMMY_VAR_ID },                   // 38
		{ BOB_TABLE_DUMMY_VAR_ID },                   // 39
		{ BOB_TABLE_DUMMY_VAR_ID },                   // 40
		{ BOB_TABLE_DUMMY_VAR_ID },                   // 41
		{ BOB_TABLE_DUMMY_VAR_ID },                   // 42
		{ BOB_TABLE_DUMMY_VAR_ID },                   // 43
		{ BOB_TABLE_DUMMY_VAR_ID },                   // 44
		{ BOB_TABLE_DUMMY_VAR_ID },                   // 45
		{ BOB_TABLE_DUMMY_VAR_ID },                   // 46
		{ VLT_INDI_FILE_PBP_SERIAL },                 // 47
		};

// This table correlates:
// Report ID's with a report script ID
const BobsByteTableType BobsReportScriptLookup[] = {
		{ SCRIPT_REFILL_RECEIPT_RPT },                // REFILL_RECEIPT_RPT,
		{ DUMMY_SCRIPT },                             // REFILL_INSTR_RPT,
		{ SCRIPT_REFILL_LOG_RPT },                    // REFILL_STMT_RPT,
		{ SCRIPT_REG_RPT },                           // REFUND_RECEIPT_RPT,
		{ SCRIPT_MULTI_ACCOUNT_RPT },                 // MULTI_ACCT_SUM_RPT,
		{ SCRIPT_SINGLE_ACCOUNT_RPT },                // SINGLE_ACCT_RPT,
		{ SCRIPT_SIMPLE_RPT },                        // SHORT_SVC_RPT,
		{ DUMMY_SCRIPT },                             // LONG_SVC_RPT,
		{ SCRIPT_REG_RPT },                           // REG_RPT,
		{ SCRIPT_CONFIG_RPT },                        // CONFIG_RPT,
		{ SCRIPT_ERROR_RPT },                         // ERR_RPT,
		{ SCRIPT_SIMPLE_RPT },                        // PERMIT_RPT,
		{ DUMMY_SCRIPT },                             // SETUP_RPT,
		{ SCRIPT_SINGLE_ACCOUNT_RPT },                // DELCON_RPT,
		{ DUMMY_SCRIPT },                             // DATA_CAPTURE_RPT,
		{ SCRIPT_SIMPLE_RPT },                     // SW_VER_RPT / CONFIG_RPT_2,
		{ DUMMY_SCRIPT },                             // SW_UPDATE_RPT,
		{ SCRIPT_SIMPLE_RPT },                        // DATETIME_STAMP_RPT,
		{ SCRIPT_SIMPLE_RPT },                        // RATES_DOWNLOAD_RPT,
		{ SCRIPT_SIMPLE_RPT },                        // RATES_SUMMARY_RPT,
		{ SCRIPT_REG_RPT },                          // BALANCE_INQ_RECEIPT_RPT,
		{ SCRIPT_REG_RPT },                           // BALANCE_INQ_STMT_RPT,
		{ SCRIPT_SIMPLE_RPT },                        // USPS_SERVICE_SUM_RPT,
		{ SCRIPT_SIMPLE_RPT },                        // LP_PWR_FAIL_RPT,
		{ SCRIPT_REG_RPT },                           // LP_DAILY_TOTALS_RPT,
		{ SCRIPT_REG_RPT },                           // LP_USAGE_SUMMARY_RPT,
		{ SCRIPT_REG_RPT },                           // LP_FRANK_SEQS_RPT,
		{ SCRIPT_REG_RPT },                           // LP_OTHER_OPS_RPT,
		{ SCRIPT_REG_RPT },                           // LP_PBP_COMM_RPT,
		{ SCRIPT_REG_RPT },                           // LP_COMM_INCIDENTS_RPT,
		{ SCRIPT_REG_RPT },                           // LP_FRAUD_ATTEMPTS_RPT,
		{ SCRIPT_REG_RPT },                           // LP_OP_INCIDENTS_RPT,
		{ SCRIPT_SINGLE_ACCOUNT_RPT },                // LP_POSTDATING_RPT,
		{ SCRIPT_SIMPLE_RPT },                        // GERM_DOM_MANIFEST_RPT,
		{ SCRIPT_SIMPLE_RPT },                        // GERM_INTL_MANIFEST_RPT,
		{ DUMMY_SCRIPT },               // WITHDR_INST__RPT,
		{ SCRIPT_SIMPLE_RPT },            // ASR_RPT,
		{ DUMMY_SCRIPT /*SCRIPT_SOM_RPT*/},                    // INDIA_SOM_RPT,
		{ SCRIPT_REG_RPT },                           // UNIPOSTA_TRACKING_RPT,
		{ SCRIPT_REG_RPT },                           // TEST_PRINT_RPT,
		{ SCRIPT_REG_RPT },                           // NETSET2_CUSTOMER_RPT
		{ DUMMY_SCRIPT },                             // REPORT_IDS,
		};

/******************************************************************************
 This is the table of functions referenced by the register descriptors of
 various JANUS graphic component types.
 (indicia, town circle, ad, inscr, report, etc)
 ******************************************************************************/
const ReportTableType ReportTable[] = { { BOBMainDriver }, /* 0 */
{ fnfDummyRptFcn }, /* 1 */ // reserved for possible future use in its former incarnation
		{ fnfDummyRptFcn }, /* 2 */ // reserved for possible future use in its former incarnation
		{ fnfGetCMOSRptLongParam }, /* 3 */     // Used by K700
		{ fnfDummyRptFcn }, /* 4 */ // reserved for possible future use in its former incarnation
		{ fnfDummyRptFcn }, /* 5 */ // reserved for possible future use in its former incarnation
		{ fnfDummyRptFcn }, /* 6 */ // reserved for possible future use in its former incarnation
		{ fnfDummyRptFcn }, /* 7 */ // reserved for possible future use in its former incarnation
		{ fnfDummyRptFcn }, /* 8 */ // reserved for possible future use in its former incarnation
		{ fnfDummyRptFcn }, /* 9 */ // reserved for possible future use in its former incarnation
		{ fnfDummyRptFcn }, /* 10 */ // reserved for possible future use in its former incarnation
		{ fnfDummyRptFcn }, /* 11 */ // reserved for possible future use in its former incarnation
		{ fnfDummyRptFcn }, /* 12 */ // reserved for possible future use in its former incarnation
		{ fnfDummyRptFcn }, /* 13 */ // reserved for possible future use in its former incarnation
		{ fnfDummyRptFcn }, /* 14 */ // reserved for possible future use in its former incarnation
		{ fnfGetLastRefillAmount }, /* 15 */
		{ fnfGetDate }, /* 16 */    //current date, report format
		{ fnfGetTime }, /* 17 */    //current time, report format
		{ fnfDummyRptFcn }, /* 18 */ // reserved for possible future use in its former incarnation
		{ fnfDummyRptFcn }, /* 19 */ // reserved for possible future use in its former incarnation
		{ fnfGetCreditLine }, /* 20 */
		{ fnfGetAccountBalance }, /* 21 */
		{ fnfAccountRptFunds }, /* 22 */
		{ fnfAccountRptPieces }, /* 23 */
		{ fnfAccountName }, /* 24 */
		{ fnfDummyRptFcn }, /* 25 */ // reserved for possible future use in its former incarnation
		{ fnfAccountRptID }, /* 26 */
		{ fnfGetTotalReportPages }, /* 27 */
		{ fnfRatesRptVars }, /* 28 */ // reserved for possible future use in its former incarnation
		{ fnfDummyRptFcn }, /* 29 */    // Was fnfDelConSumStmtVars
		{ fnfDummyRptFcn }, /* 30 */ // reserved for possible future use in its former incarnation
		{ fnfLastRefillDate }, /* 31 */
		{ fnfLastRefillTime }, /* 32 */
		{ fnfGetPBPActNumber }, /* 33 */
		{ fnfGetBatchAmount }, /* 34 */
		{ fnfGetBatchCount }, /* 35 */
		{ fnfHRAbrvServiceTypeAscii }, /* 36 */
		{ fnfHRProductCodeAscii }, /* 37 */
		{ fnfHRServiceType }, /* 38 */
		{ fnfGetPsdDeviceID }, /* 39 */
		{ fnfGetCurrentReportPageNbr }, /* 40 */
		{ fnfMeterPCN }, /* 41 */
		{ fnfMeterSMR }, /* 42 */
		{ fnfRefillStmtVars }, /* 43 */
		{ fnfErrorLogStmtVars }, /* 44 */
		{ fnfDummyRptFcn }, /* 45 */
		{ fnfGetPrintHeadSerialNumber }, /* 46 */
		{ fnfVersionCity }, /* 47 */
		{ fnfAccountSubName }, /* 48 */
		{ fnfAccountSubSubName }, /* 49 */
		{ fnfDummyRptFcn }, /* 50 */ // reserved for possible future use in its former incarnation
		{ fnfAccountingStmtVars }, /* 51 */
		{ fnfReadPSDParms }, /* 52 */
		{ fnfReadDebitCertParms }, /* 53 */
		{ fnfReadDebitCertParmsBcAscii }, /* 54 */
		{ fnfReadPSDParmsBcAscii }, /* 55 */
		{ fnfOCRVerificationCode }, /* 56 */
		{ fnfTextEntryVars }, /* 57 */
		{ fnfCurrentPermitBatchCount }, /* 58 */
		{ fnfPermitRptVars }, /* 59 */
		{ fnfSuppleServiceVars }, /* 60 */
		{ fnfReadDebitCertParmsSplitFont }, /* 61 */
		{ fnfZipCodeFieldData }, /* 62 */
		{ fnfGetPrintFailLogInfo }, /* 63 */
		{ fnfGetMiscCounts }, /* 64 */    // reserved for use by K700
		{ fnfDummyRptFcn }, /* 65 */
		{ fnfDummyRptFcn }, /* 66 */
		{ fnfDummyRptFcn }, /* 67 */
		{ fnfDummyRptFcn }, /* 68 */
		{ fnfDummyRptFcn }, /* 69 */
		{ fnfDummyRptFcn }, /* 70 */
		{ fnfDummyRptFcn }, /* 71 */
		{ fnfDummyRptFcn }, /* 72 */
		{ fnfGetPostdatingLogInfo }, /* 73 */
		{ fnfGetFrenchCurrentAR }, /* 74 */
		{ fnfGetHRpostalLogo }, /* 75 */
		{ fnfGetIndiciaType }, /* 76 */    // Used by K700
		{ fnfDummyRptFcn }, /* 77 */
		{ fnfGetGermManifestInfo }, /* 78 */
		{ fnfGetSwissBarcodeInfo }, /* 79 */
		{ fnfGetBarcodeVCRInfo }, /* 80 */
		{ fnfGetWeight }, /* 81 */
		{ fnfGetCurrentRefillAmount }, /* 82 */
		{ fnfGetKeypadPhoneNumber }, /* 83 */
		{ fnfGetWithdrawalPassword }, /* 84 */
		{ fnfGetAjoutInfo }, /* 85 */
		{ fnfGetASRLogInfo }, /* 86 */
		{ fnfGetPreDebitItems }, /* 87 */
		{ fnfGetCachedPieceCount }, /* 88 */
		{ fnfGetCachedPieceCountAscii }, /* 89 */
		{ fnfDCAPStringsAscii }, /* 90 */
		{ fnfEMDFormatDebitCertParms }, /* 91 */
		{ fnfGetBobVariableByteArray }, /* 92 */        //don't use this fn
		{ fnfGetUnipostaInfo }, /* 93 */ // For getting Slovakian license number
		{ fnfGetSOMInfo }, /* 94 */
		{ fnfReadModDebitCertParms }, /* 95 */
		{ fnfReadPSDParmsAscii }, /* 96 */      //for flex debit building
		{ fnfFlexDebitIndiaPostAscii }, /* 97 */      //for flex debit building
		{ fnfDummyRptFcn }, /* 98 */      //for SOM
		{ fnfGetGlob2BobVariableByteArray }, /* 99 */ // For building flex debit barcode.
		{ fnfDummyRptFcn }, /* 100 */ // Keep this entry, need one bona fide entry for fnfDummyRptFcn
		{ fnfGetTownName }, /* 101 */ // For getting Slovakian town name according to variable ID.
		{ fnfMailingDateFlex }, /* 102 */ // For getting the mailing date saved by bob in the LoadIndicia
										  // function, and formatting it according to the variable ID.
		{ fnfGetBrazilInfo }, /* 103 */ // For getting Human Readable Indicia Data specific to Brazil.
		{ fnfFlexDebitBrazilPostAscii }, /* 104 */ // For building flex debit input for Brazil.
		{ fnfAdjustRegisterForFlexDebitAscii }, /* 105 */ // For adjusting the register data for flex debit input.
		{ fnfDummyRptFcn }, /* 106 */
		{ fnfReadDebitCertParmsBcByteArray }, /* 107 */
		{ fnfGetNetSet2ReceiptInfo }, /* 108 */
		{ fnfGetSwedenInfo }, /* 109 */ // Get items specific to Sweden.  Used for both the Flex Debit input data and the 2D barcode data.
		{ fnfFlexDebitPhilippinesPostAscii }, /* 110 */ // Get items specific to Philippines.  Used for both the Flex Debit input data and Human Readable data.
		{ fnfReadFlexDebitParms }, /* 111 */
		{ fnfReadDebitCertParms2 }, /* 112 */ // Same as fnfReadDebitCertParms, but uses IPB_GET_INDICIA_DEF_REC_2
		{ fnfReadDebitCertParms2BcRevrsByteAry }, /* 113 */ // Same as fnfReadDebitCertParmsBcByteArray, but it uses
															//  IPB_GET_INDICIA_DEF_REC_2 and reverses the order of the bytes.
		{ fnfBalanceInquiryStmtVars }, /* 114 */ // Balance Inquiry Log Item - Unichar.
		{ fnfDummyRptFcn }, /* 115 */
		{ fnfDummyRptFcn }, /* 116 */
		{ fnfDummyRptFcn }, /* 117 */
		{ fnfDummyRptFcn }, /* 118 */
		{ fnfDummyRptFcn }, /* 119 */

		// functions 120-129 reserved for DM100i Express Meter
		{ fnfGetExpressMeterBarcodeInfo }, /* 120 */

		{ fnfDummyRptFcn }, /* 121 */
		{ fnfDummyRptFcn }, /* 122 */
		{ fnfDummyRptFcn }, /* 123 */

		{ fnfGetRefundReceiptInfo }, /*124 */
    { fnfDummyRptFcn }, /* 125 */
    { fnfDummyRptFcn }, /* 126 */
    { fnfDummyRptFcn }, /* 127 */
    { fnfDummyRptFcn }, /* 128 */
    { fnfDummyRptFcn }, /* 129 */
		{ fnfGetCSDIndiciaString }, /*130 */
};

const size_t RptFunctionIDMax = ARRAY_LENGTH(ReportTable);

//-------------------------------------------------------------
//  Local / Global Variable Definitions:
//-------------------------------------------------------------

// For Brazil, we store the system time when creating the flex debit input data,
//  then use that time when doing the DH calculation for the HR portion of the indicia.
static DATETIME stRunTime;

// page number globals 
unsigned short wCurrentPageNumber = 1;      // current page number
unsigned short wTotalPages = 1;             // total pages for multipage reports

// dcap report globals
unsigned dcapindex = 0; //index has name conflict

// Used by fnfDatCapRptVars()
// char StrOrValue[100];

// Account report globals
unsigned short usRptNumActs;
unsigned short aRptActList[ACT_MAX_NUM_ACCOUNT + 1];
unsigned short usRptActID;

// French Data Log report globals
unsigned short usFrenchNumRecs;
unsigned char ucFrenchIndex;
unsigned char ucFrenchStartIndex;

#if !defined (K700) && !defined (G900)
// PCL ASCII struct populated from the Data Capture Engine
// Used in fnfGetIndiciaType to get the HRT string.
static struct pcl_ascii sPclAscii;
#endif

// Rates report globals
extern int newRatesDownloadedIndex;     //reflects the total # of rates files
extern NEW_RATES_INFO newRatesInfo[20];
extern DATETIME newRatesLoadTime;
extern CMOSRATECOMPSMAP CMOSRateCompsMap; // The Active and Pending Rate Data Elements for the system.

RATES_RPT_DATA sRatesRptData;

// currency symbol globals
char pCurrencySym[MAX_CURRENCY_LEN + 1] = { '\0', '\0', '\0', '\0' };
unsigned char bPlacement;
unsigned char currency_len;

char* BalanceInquiry[MAX_BALANCEINQ_STRS]; // Language string array moved from extreports

// common return code for BMP stuff
static unsigned char retcode = SUCCESSFUL;

// local time date utilities
#define NO_SEPARATOR    0
static UINT8 fnFormatDateASCII(void * pStrOrValue,
		const DATETIME * SysDateTimePtr, UINT8 ubFormatOption,
		char cDateSeparation1, char cDateSeparation2);

// local refill report utilities
static unsigned char fnGetRefillRecordIndex(ushort usVarID);
static unsigned char fnGetRefillLogIndex(uchar ucWhich);
static unsigned char fnGetNextMostRecentRefillLogIndex(uchar ucRefIdx);
static unsigned char fnGetMostRecentRefillLogIndex(void);

// local error report utilities
static UINT8 fnGetMostRecentErrorLogIndex(void);
static UINT8 fnGetNextMostRecentErrorLogIndex(UINT8 ucRefIdx);
static UINT8 fnGetErrorPageIndex(UINT16 usVarID);
static UINT8 fnGetErrorLogIndex(UINT8 ucWhich);
static UINT8 fnGetErrorLogEntryCount(void);

// local delcon report utilites
//static UINT8 fnGetDelConSumVarIndex( UINT16 usVarID );
//static UINT8 fnBuildDelConSumReportList( UINT8 * aDelConList );

// local French data log report utilites
static UINT16 fnfFormatFrenchReportDate(char * pStr,
		const DATETIME * DateTimePtr);
static UINT16 fnfFormatFrenchReportTime(char * pStr,
		const DATETIME * DateTimePtr);

// local permit report utilities
static unsigned char fnGetPermitIndex(ushort usVarID);

// local conversion utility
char Latin1ToUpper(register char uChar);

// default values for language dependent "Yes" and "No", they will be overwritten by the
// translated language from the language FFS file.
unichar pUniNativeWord[MAX_NATIVE_WORD][LEN_NATIVE_WORD + 1] = {
// default values of "Yes" and "No"
		{ (unichar) 'Y', (unichar) 'e', (unichar) 's', NULL_VAL }, {
				(unichar) 'N', (unichar) 'o', (unichar) NULL_VAL },
// default values for the months
		{ (unichar) 'J', (unichar) 'A', (unichar) 'N', NULL_VAL }, {
				(unichar) 'F', (unichar) 'E', (unichar) 'B', NULL_VAL }, {
				(unichar) 'M', (unichar) 'A', (unichar) 'R', NULL_VAL }, {
				(unichar) 'A', (unichar) 'P', (unichar) 'R', NULL_VAL }, {
				(unichar) 'M', (unichar) 'A', (unichar) 'Y', NULL_VAL }, {
				(unichar) 'J', (unichar) 'U', (unichar) 'N', NULL_VAL }, {
				(unichar) 'J', (unichar) 'U', (unichar) 'L', NULL_VAL }, {
				(unichar) 'A', (unichar) 'U', (unichar) 'G', NULL_VAL }, {
				(unichar) 'S', (unichar) 'E', (unichar) 'P', NULL_VAL }, {
				(unichar) 'O', (unichar) 'C', (unichar) 'T', NULL_VAL }, {
				(unichar) 'N', (unichar) 'O', (unichar) 'V', NULL_VAL }, {
				(unichar) 'D', (unichar) 'E', (unichar) 'C', NULL_VAL } };

// this table maps variable ids to the ordinal sort index on the printed report
const ushort rRefillVarId2RecordIndex[][2] = { { GET_REFILL_LOG_DOS0, 0 }, {
		GET_REFILL_LOG_DOS1, 1 }, { GET_REFILL_LOG_DOS2, 2 }, {
		GET_REFILL_LOG_DOS3, 3 }, { GET_REFILL_LOG_DOS4, 4 }, {
		GET_REFILL_LOG_TOS0, 0 }, { GET_REFILL_LOG_TOS1, 1 }, {
		GET_REFILL_LOG_TOS2, 2 }, { GET_REFILL_LOG_TOS3, 3 }, {
		GET_REFILL_LOG_TOS4, 4 }, { GET_REFILL_LOG_AMT0, 0 }, {
		GET_REFILL_LOG_AMT1, 1 }, { GET_REFILL_LOG_AMT2, 2 }, {
		GET_REFILL_LOG_AMT3, 3 }, { GET_REFILL_LOG_AMT4, 4 }, { 0, 0 } //table end marker
};

// this table maps variable ids to the ordinal sort index on the printed report
static const ushort rBalInqVarId2RecordOrdinal[][2] = {
		{ GET_BALINQ_LOG_DOS0, 0 }, { GET_BALINQ_LOG_DOS1, 1 }, {
				GET_BALINQ_LOG_DOS2, 2 }, { GET_BALINQ_LOG_DOS3, 3 }, {
				GET_BALINQ_LOG_DOS4, 4 }, { GET_BALINQ_LOG_TOS0, 0 }, {
				GET_BALINQ_LOG_TOS1, 1 }, { GET_BALINQ_LOG_TOS2, 2 }, {
				GET_BALINQ_LOG_TOS3, 3 }, { GET_BALINQ_LOG_TOS4, 4 }, {
				GET_BALINQ_LOG_ACTION0, 0 }, { GET_BALINQ_LOG_ACTION1, 1 }, {
				GET_BALINQ_LOG_ACTION2, 2 }, { GET_BALINQ_LOG_ACTION3, 3 }, {
				GET_BALINQ_LOG_ACTION4, 4 }, { 0, 0 }         //table end marker
};

// this table maps variable ids to the ordinal sort index on the printed report
const ushort rErrorVarId2RecordIndex[][2] = { { GET_ERROR_LOG_DOS0, 0 }, {
		GET_ERROR_LOG_DOS1, 1 }, { GET_ERROR_LOG_DOS2, 2 }, {
		GET_ERROR_LOG_DOS3, 3 }, { GET_ERROR_LOG_DOS4, 4 }, {
		GET_ERROR_LOG_TOS0, 0 }, { GET_ERROR_LOG_TOS1, 1 }, {
		GET_ERROR_LOG_TOS2, 2 }, { GET_ERROR_LOG_TOS3, 3 }, {
		GET_ERROR_LOG_TOS4, 4 }, { GET_ERROR_LOG_ID0, 0 }, { GET_ERROR_LOG_ID1,
		1 }, { GET_ERROR_LOG_ID2, 2 }, { GET_ERROR_LOG_ID3, 3 }, {
		GET_ERROR_LOG_ID4, 4 }, { GET_ERROR_LOG_COUNT0, 0 }, {
		GET_ERROR_LOG_COUNT1, 1 }, { GET_ERROR_LOG_COUNT2, 2 }, {
		GET_ERROR_LOG_COUNT3, 3 }, { GET_ERROR_LOG_COUNT4, 4 }, { 0, 0 } //table end marker
};

// this table maps variable ids to the sort index on the printed report
const ushort rAccountVarId2RecordIndex[][2] = { { GET_ACCT_ID0, 0 }, {
		GET_ACCT_ID1, 1 }, { GET_ACCT_ID2, 2 }, { GET_ACCT_ID3, 3 }, {
		GET_ACCT_ID4, 4 }, { GET_ACCT_PIECES0, 0 }, { GET_ACCT_PIECES1, 1 }, {
		GET_ACCT_PIECES2, 2 }, { GET_ACCT_PIECES3, 3 }, { GET_ACCT_PIECES4, 4 },
		{ GET_ACCT_AMOUNT0, 0 }, { GET_ACCT_AMOUNT1, 1 },
		{ GET_ACCT_AMOUNT2, 2 }, { GET_ACCT_AMOUNT3, 3 },
		{ GET_ACCT_AMOUNT4, 4 }, { GET_ACCT_NAME0, 0 }, { GET_ACCT_NAME1, 1 }, {
				GET_ACCT_NAME2, 2 }, { GET_ACCT_NAME3, 3 },
		{ GET_ACCT_NAME4, 4 }, { 0, 0 }                       //table end marker
};

/* No DelCon anymore, now its GTS.     !!!!
 // this table maps variable ids to the sort index on the printed report
 const ushort rUSPSSumVarId2RecordIndex[][2] =
 {
 {GET_DELCON_PACKAGE_ID0, 0},
 {GET_DELCON_PACKAGE_ID1, 1},
 {GET_DELCON_PACKAGE_ID2, 2},
 {GET_DELCON_PACKAGE_ID3, 3},
 {GET_DELCON_PACKAGE_ID4, 4},
 {GET_DELCON_DEST_ZIP0, 0},
 {GET_DELCON_DEST_ZIP1, 1},
 {GET_DELCON_DEST_ZIP2, 2},
 {GET_DELCON_DEST_ZIP3, 3},
 {GET_DELCON_DEST_ZIP4, 4},
 {GET_DELCON_POSTAGE0, 0},
 {GET_DELCON_POSTAGE1, 1},
 {GET_DELCON_POSTAGE2, 2},
 {GET_DELCON_POSTAGE3, 3},
 {GET_DELCON_POSTAGE4, 4},
 {GET_DELCON_UPLOAD_DATE0, 0},
 {GET_DELCON_UPLOAD_DATE1, 1},
 {GET_DELCON_UPLOAD_DATE2, 2},
 {GET_DELCON_UPLOAD_DATE3, 3},
 {GET_DELCON_UPLOAD_DATE4, 4},
 {0,0}                       //table end marker
 };
 */

// this table maps variable ids to the ordinal sort index on the printed report
const ushort rRatesVarId2RecordIndex[][2] = { { GET_RATE_MODULE_NAME0, 0 }, {
		GET_RATE_MODULE_NAME1, 1 }, { GET_RATE_MODULE_NAME2, 2 }, {
		GET_RATE_MODULE_NAME3, 3 }, { GET_RATE_MODULE_NAME4, 4 }, {
		GET_RATE_MODULE_VERSION0, 0 }, { GET_RATE_MODULE_VERSION1, 1 }, {
		GET_RATE_MODULE_VERSION2, 2 }, { GET_RATE_MODULE_VERSION3, 3 }, {
		GET_RATE_MODULE_VERSION4, 4 }, { GET_RATE_MODULE_DATE0, 0 }, {
		GET_RATE_MODULE_DATE1, 1 }, { GET_RATE_MODULE_DATE2, 2 }, {
		GET_RATE_MODULE_DATE3, 3 }, { GET_RATE_MODULE_DATE4, 4 }, {
		GET_RATE_MODULE_ACTIVE0, 0 }, { GET_RATE_MODULE_ACTIVE1, 1 }, {
		GET_RATE_MODULE_ACTIVE2, 2 }, { GET_RATE_MODULE_ACTIVE3, 3 }, {
		GET_RATE_MODULE_ACTIVE4, 4 }, { 0, 0 }                //table end marker
};

// this table maps variable ids to the sort index on the printed report
const ushort rDailySumVarId2RecordIndex[][2] = { { GET_DAILY_LOG_DATE0, 0 }, {
		GET_DAILY_LOG_DATE1, 1 }, { GET_DAILY_LOG_DATE2, 2 }, {
		GET_DAILY_LOG_DATE3, 3 }, { GET_DAILY_LOG_DATE4, 4 }, {
		GET_DAILY_LOG_AR0, 0 }, { GET_DAILY_LOG_AR1, 1 },
		{ GET_DAILY_LOG_AR2, 2 }, { GET_DAILY_LOG_AR3, 3 }, { GET_DAILY_LOG_AR4,
				4 }, { GET_DAILY_LOG_DIFF0, 0 }, { GET_DAILY_LOG_DIFF1, 1 }, {
				GET_DAILY_LOG_DIFF2, 2 }, { GET_DAILY_LOG_DIFF3, 3 }, {
				GET_DAILY_LOG_DIFF4, 4 }, { 0, 0 }            //table end marker
};

// this table maps variable ids to the sort index on the printed report
const ushort rFrankSeqVarId2RecordIndex[][2] = { { GET_FRANK_SEQ_DATE0, 0 }, {
		GET_FRANK_SEQ_DATE1, 1 }, { GET_FRANK_SEQ_DATE2, 2 }, {
		GET_FRANK_SEQ_TIME0, 0 }, { GET_FRANK_SEQ_TIME1, 1 }, {
		GET_FRANK_SEQ_TIME2, 2 }, { GET_FRANK_SEQ_EVT0, 0 }, {
		GET_FRANK_SEQ_EVT1, 1 }, { GET_FRANK_SEQ_EVT2, 2 }, {
		GET_FRANK_SEQ_ITEM0, 0 }, { GET_FRANK_SEQ_ITEM1, 1 }, {
		GET_FRANK_SEQ_ITEM2, 2 }, { GET_FRANK_SEQ_AMT0, 0 }, {
		GET_FRANK_SEQ_AMT1, 1 }, { GET_FRANK_SEQ_AMT2, 2 }, {
		GET_FRANK_SEQ_QTY0, 0 }, { GET_FRANK_SEQ_QTY1, 1 }, {
		GET_FRANK_SEQ_QTY2, 2 }, { GET_FRANK_SEQ_IAR0, 0 }, {
		GET_FRANK_SEQ_IAR1, 1 }, { GET_FRANK_SEQ_IAR2, 2 }, {
		GET_FRANK_SEQ_PDATE0, 0 }, { GET_FRANK_SEQ_PDATE1, 1 }, {
		GET_FRANK_SEQ_PDATE2, 2 }, { 0, 0 }                   //table end marker
};

// this table maps variable ids to the sort index on the printed report
const ushort rOtherOpsVarId2RecordIndex[][2] = { { GET_OTHER_OPS_DATE0, 0 }, {
		GET_OTHER_OPS_DATE1, 1 }, { GET_OTHER_OPS_DATE2, 2 }, {
		GET_OTHER_OPS_DATE3, 3 }, { GET_OTHER_OPS_DATE4, 4 }, {
		GET_OTHER_OPS_TIME0, 0 }, { GET_OTHER_OPS_TIME1, 1 }, {
		GET_OTHER_OPS_TIME2, 2 }, { GET_OTHER_OPS_TIME3, 3 }, {
		GET_OTHER_OPS_TIME4, 4 }, { GET_OTHER_OPS_EVT0, 0 }, {
		GET_OTHER_OPS_EVT1, 1 }, { GET_OTHER_OPS_EVT2, 2 }, {
		GET_OTHER_OPS_EVT3, 3 }, { GET_OTHER_OPS_EVT4, 4 }, {
		GET_OTHER_OPS_IAR0, 0 }, { GET_OTHER_OPS_IAR1, 1 }, {
		GET_OTHER_OPS_IAR2, 2 }, { GET_OTHER_OPS_IAR3, 3 }, {
		GET_OTHER_OPS_IAR4, 4 }, { GET_OTHER_OPS_IPC0, 0 }, {
		GET_OTHER_OPS_IPC1, 1 }, { GET_OTHER_OPS_IPC2, 2 }, {
		GET_OTHER_OPS_IPC3, 3 }, { GET_OTHER_OPS_IPC4, 4 }, { 0, 0 } //table end marker
};

// this table maps variable ids to the sort index on the printed report
const ushort rPbPCommVarId2RecordIndex[][2] = { { GET_PBP_COMM_DATE0, 0 }, {
		GET_PBP_COMM_DATE1, 1 }, { GET_PBP_COMM_DATE2, 2 }, {
		GET_PBP_COMM_DATE3, 3 }, { GET_PBP_COMM_DATE4, 4 }, {
		GET_PBP_COMM_TIME0, 0 }, { GET_PBP_COMM_TIME1, 1 }, {
		GET_PBP_COMM_TIME2, 2 }, { GET_PBP_COMM_TIME3, 3 }, {
		GET_PBP_COMM_TIME4, 4 }, { GET_PBP_COMM_EVT0, 0 }, { GET_PBP_COMM_EVT1,
		1 }, { GET_PBP_COMM_EVT2, 2 }, { GET_PBP_COMM_EVT3, 3 }, {
		GET_PBP_COMM_EVT4, 4 }, { GET_PBP_COMM_IAR0, 0 },
		{ GET_PBP_COMM_IAR1, 1 }, { GET_PBP_COMM_IAR2, 2 }, { GET_PBP_COMM_IAR3,
				3 }, { GET_PBP_COMM_IAR4, 4 }, { GET_PBP_COMM_IPC0, 0 }, {
				GET_PBP_COMM_IPC1, 1 }, { GET_PBP_COMM_IPC2, 2 }, {
				GET_PBP_COMM_IPC3, 3 }, { GET_PBP_COMM_IPC4, 4 }, { 0, 0 } //table end marker
};

// this table maps variable ids to the sort index on the printed report
const ushort rCommIncidentsVarId2RecordIndex[][2] = { { GET_COMM_INCIDENT_DATE0,
		0 }, { GET_COMM_INCIDENT_DATE1, 1 }, { GET_COMM_INCIDENT_DATE2, 2 }, {
		GET_COMM_INCIDENT_DATE3, 3 }, { GET_COMM_INCIDENT_DATE4, 4 }, {
		GET_COMM_INCIDENT_TIME0, 0 }, { GET_COMM_INCIDENT_TIME1, 1 }, {
		GET_COMM_INCIDENT_TIME2, 2 }, { GET_COMM_INCIDENT_TIME3, 3 }, {
		GET_COMM_INCIDENT_TIME4, 4 }, { GET_COMM_INCIDENT_EVT0, 0 }, {
		GET_COMM_INCIDENT_EVT1, 1 }, { GET_COMM_INCIDENT_EVT2, 2 }, {
		GET_COMM_INCIDENT_EVT3, 3 }, { GET_COMM_INCIDENT_EVT4, 4 }, {
		GET_COMM_INCIDENT_IAR0, 0 }, { GET_COMM_INCIDENT_IAR1, 1 }, {
		GET_COMM_INCIDENT_IAR2, 2 }, { GET_COMM_INCIDENT_IAR3, 3 }, {
		GET_COMM_INCIDENT_IAR4, 4 }, { GET_COMM_INCIDENT_IPC0, 0 }, {
		GET_COMM_INCIDENT_IPC1, 1 }, { GET_COMM_INCIDENT_IPC2, 2 }, {
		GET_COMM_INCIDENT_IPC3, 3 }, { GET_COMM_INCIDENT_IPC4, 4 }, { 0, 0 } //table end marker
};

// this table maps variable ids to the sort index on the printed report
const ushort rFraudAttemptsVarId2RecordIndex[][2] = { { GET_FRAUD_ATTEMPT_DATE0,
		0 }, { GET_FRAUD_ATTEMPT_DATE1, 1 }, { GET_FRAUD_ATTEMPT_DATE2, 2 }, {
		GET_FRAUD_ATTEMPT_DATE3, 3 }, { GET_FRAUD_ATTEMPT_DATE4, 4 }, {
		GET_FRAUD_ATTEMPT_TIME0, 0 }, { GET_FRAUD_ATTEMPT_TIME1, 1 }, {
		GET_FRAUD_ATTEMPT_TIME2, 2 }, { GET_FRAUD_ATTEMPT_TIME3, 3 }, {
		GET_FRAUD_ATTEMPT_TIME4, 4 }, { GET_FRAUD_ATTEMPT_EVT0, 0 }, {
		GET_FRAUD_ATTEMPT_EVT1, 1 }, { GET_FRAUD_ATTEMPT_EVT2, 2 }, {
		GET_FRAUD_ATTEMPT_EVT3, 3 }, { GET_FRAUD_ATTEMPT_EVT4, 4 }, {
		GET_FRAUD_ATTEMPT_IAR0, 0 }, { GET_FRAUD_ATTEMPT_IAR1, 1 }, {
		GET_FRAUD_ATTEMPT_IAR2, 2 }, { GET_FRAUD_ATTEMPT_IAR3, 3 }, {
		GET_FRAUD_ATTEMPT_IAR4, 4 }, { GET_FRAUD_ATTEMPT_IPC0, 0 }, {
		GET_FRAUD_ATTEMPT_IPC1, 1 }, { GET_FRAUD_ATTEMPT_IPC2, 2 }, {
		GET_FRAUD_ATTEMPT_IPC3, 3 }, { GET_FRAUD_ATTEMPT_IPC4, 4 }, { 0, 0 } //table end marker
};

// this table maps variable ids to the sort index on the printed report
const ushort rOpIncidentsVarId2RecordIndex[][2] = {
		{ GET_OP_INCIDENT_DATE0, 0 }, { GET_OP_INCIDENT_DATE1, 1 }, {
				GET_OP_INCIDENT_DATE2, 2 }, { GET_OP_INCIDENT_DATE3, 3 }, {
				GET_OP_INCIDENT_DATE4, 4 }, { GET_OP_INCIDENT_TIME0, 0 }, {
				GET_OP_INCIDENT_TIME1, 1 }, { GET_OP_INCIDENT_TIME2, 2 }, {
				GET_OP_INCIDENT_TIME3, 3 }, { GET_OP_INCIDENT_TIME4, 4 }, {
				GET_OP_INCIDENT_EVT0, 0 }, { GET_OP_INCIDENT_EVT1, 1 }, {
				GET_OP_INCIDENT_EVT2, 2 }, { GET_OP_INCIDENT_EVT3, 3 }, {
				GET_OP_INCIDENT_EVT4, 4 }, { GET_OP_INCIDENT_IAR0, 0 }, {
				GET_OP_INCIDENT_IAR1, 1 }, { GET_OP_INCIDENT_IAR2, 2 }, {
				GET_OP_INCIDENT_IAR3, 3 }, { GET_OP_INCIDENT_IAR4, 4 }, {
				GET_OP_INCIDENT_IPC0, 0 }, { GET_OP_INCIDENT_IPC1, 1 }, {
				GET_OP_INCIDENT_IPC2, 2 }, { GET_OP_INCIDENT_IPC3, 3 }, {
				GET_OP_INCIDENT_IPC4, 4 }, { 0, 0 }           //table end marker
};

// this table maps variable ids to the sort index on the printed report
const ushort rPostdatingVarId2RecordIndex[][2] = { { GET_POSTDATING_DATE0, 0 },
		{ GET_POSTDATING_DATE1, 1 }, { GET_POSTDATING_DATE2, 2 }, {
				GET_POSTDATING_DATE3, 3 }, { GET_POSTDATING_DATE4, 4 }, {
				GET_POSTDATING_AR0, 0 }, { GET_POSTDATING_AR1, 1 }, {
				GET_POSTDATING_AR2, 2 }, { GET_POSTDATING_AR3, 3 }, {
				GET_POSTDATING_AR4, 4 }, { GET_POSTDATING_PC0, 0 }, {
				GET_POSTDATING_PC1, 1 }, { GET_POSTDATING_PC2, 2 }, {
				GET_POSTDATING_PC3, 3 }, { GET_POSTDATING_PC4, 4 }, { 0, 0 } //table end marker
};

// this table maps variable ids to the sort index on the printed report
const ushort rASRVarId2RecordIndex[][2] = { { GET_ASR_ID0, 0 },
		{ GET_ASR_ID1, 1 }, { GET_ASR_ID2, 2 }, { GET_ASR_ID3, 3 }, {
				GET_ASR_ID4, 4 }, { GET_ASR_PC0, 0 }, { GET_ASR_PC1, 1 }, {
				GET_ASR_PC2, 2 }, { GET_ASR_PC3, 3 }, { GET_ASR_PC4, 4 },
		{ 0, 0 }           //table end marker
};

// this table maps variable ids to the sort index on the printed report
const ushort rPermitVarId2RecordIndex[][2] = { { PERMIT_1_BATCH_COUNT, 1 }, {
		PERMIT_2_BATCH_COUNT, 2 }, { PERMIT_3_BATCH_COUNT, 3 }, {
		PERMIT_1_BATCH_NAME, 1 }, { PERMIT_2_BATCH_NAME, 2 }, {
		PERMIT_3_BATCH_NAME, 3 }, { PERMIT_PIECE_COUNT, 1 }, { 0, 0 } //table end marker
};

//--------------------------------------------------------------------
//  Constant, Field Op to Concversion Type Cross Reference Table:
//
// When the length is 0 in this table, it means that no length match
// should be required in fnFindConvType().
//
const FIELD_OP_X_REF tFieldOpXRef[] = {
// Interpret            Render
// input format,        output format,
// fieldOp1             fieldOp2       length,     convType,
// ------------         -------------  ----------  ------------------

// These are the new names:
		{ DRFOP_ASKII, DRFOP_ASKII, 0, NO_FORMAT_CHANGE }, {
				DRFOP_ASKII_MON_WCS, DRFOP_ASKII, 5, ASKII_MON_WCS_TO_STRING },
		{ DRFOP_ASKII_MON_WCS, DRFOP_ASKII, 6, ASKII_MON_WCS_TO_STRING }, {
				DRFOP_ASKII_MON_WCS_NO_FIXED, DRFOP_ASKII, 5,
				ASKII_MON_WCS_NO_FIXED_TO_STRING }, {
				DRFOP_ASKII_MON_WCS_NO_FIXED, DRFOP_ASKII, 6,
				ASKII_MON_WCS_NO_FIXED_TO_STRING }, { DRFOP_BE_BIGHEX,
				DRFOP_ASKII, 0, BIGHEX_TO_STRING }, { DRFOP_BE_BIN_UINT,
				DRFOP_ASKII, 4, ULONG_TO_STRING }, { DRFOP_BE_BITE_ARY,
				DRFOP_ASKII, 0, NO_FORMAT_CHANGE }, { DRFOP_BE_BITE_ARY,
				DRFOP_BE_BITE_ARY, 0, NO_FORMAT_CHANGE }, { DRFOP_BE_HEX_1,
				DRFOP_ASKII, 1, HEX_TO_STRING }, { DRFOP_BE_HEX_2, DRFOP_ASKII,
				2, BIGHEX_TO_STRING }, { DRFOP_BE_HEX_4, DRFOP_ASKII, 4,
				BIGHEX_TO_STRING }, { DRFOP_LE_HEX_2, DRFOP_ASKII, 2,
				HEX2_TO_STRING }, { DRFOP_LE_HEX_4, DRFOP_ASKII, 4,
				HEX4_TO_STRING }, { DRFOP_BE_INT, DRFOP_ASKII, 3,
				THREE_B_TO_STRING }, { DRFOP_BE_INT, DRFOP_ASKII, 4,
				ULONG_TO_STRING }, { DRFOP_BE_SINT, DRFOP_ASKII, 4,
				SLONG_TO_STRING }, { DRFOP_BE_MON_2DEC, DRFOP_ASKII, 2,
				BE_INT_MONEY2_DEC2_TO_STRING }, { DRFOP_BE_MON_3DEC,
				DRFOP_ASKII, 2, BE_INT_MONEY2_TO_STRING }, { DRFOP_BE_MON_3DEC,
				DRFOP_ASKII, 3, BE_INT_MONEY3_TO_STRING }, { DRFOP_BE_MON_3DEC,
				DRFOP_ASKII, 4, BE_INT_MONEY4_TO_STRING }, { DRFOP_BE_MON_3DEC,
				DRFOP_ASKII, 5, BE_INT_MONEY5_TO_STRING }, {
				DRFOP_BIN_BE_UINT_10CH_LZP, DRFOP_ASKII, 4,
				ULONG_TO_STRING_10CH_LZP }, { DRFOP_DATETIME_IBUT, DRFOP_ASKII,
				4, IB_CANADA_CREATETIME_TO_STRING }, { DRFOP_GEN_BITE_ARY,
				DRFOP_ASKII, 0, NO_FORMAT_CHANGE }, { DRFOP_LE_BITE_ARY,
				DRFOP_LE_BITE_ARY, 0, NO_FORMAT_CHANGE }, { DRFOP_LE_INT,
				DRFOP_ASKII, 4, LE_ULONG_TO_STRING }, { DRFOP_LE_SINT,
				DRFOP_ASKII, 4, LE_SLONG_TO_STRING }, { DRFOP_LE_MON_3DEC,
				DRFOP_ASKII, 2, LE_INT_MONEY2_TO_STRING }, { DRFOP_LE_MON_3DEC,
				DRFOP_ASKII, 3, LE_INT_MONEY3_TO_STRING }, { DRFOP_LE_MON_3DEC,
				DRFOP_ASKII, 4, LE_INT_MONEY4_TO_STRING }, { DRFOP_LE_MON_3DEC,
				DRFOP_ASKII, 5, LE_INT_MONEY5_TO_STRING }, { DRFOP_LE_MON_3DEC,
				DRFOP_ASKII_MON, 3, LE_INT_MONEY3_DEC_EMD_TO_STRING },
		{ DRFOP_LE_MON_3DEC, DRFOP_ASKII_MON_WDP, 3, LE_MONEY3_TO_STRING }, {
				DRFOP_MAILDATE_FMT1, DRFOP_ASKII, 4, IB_MAILDATE_TO_STRING }, {
				DRFOP_MAILDATE_FMT2, DRFOP_ASKII, 6, IB_LP_MAILDATE_TO_STRING },
		{ DRFOP_MAILDATE_FMT3, DRFOP_ASKII, 2, IB_CANADA_CREATEDATE_TO_STRING },
		{ DRFOP_MAILDATE_FMT4, DRFOP_ASKII, 1, IB_CANADA_MAILDATE_TO_STRING }, {
				DRFOP_MAILDATE_FMT5, DRFOP_ASKII, 2,
				IB_GERMAN_MAILDATE_TO_STRING }, { DRFOP_MAILDATE_FMT6,
				DRFOP_ASKII, 6, IB_NETSET2_PRODUCTIONDATE_TO_STRING }, {
				DRFOP_MAILDATE_FMT7, DRFOP_ASKII, 2,
				IB_NETSET2_MAILDATE_TO_STRING }, { DRFOP_MAILDATE_FMT8,
				DRFOP_ASKII, 4, IB_IP_MAILDATE_TO_STRING },
		{ DRFOP_MAILDATE_FMT9, DRFOP_ASKII, 2, IB_IBIL_MAILDATE_TO_STRING }, {
				DRFOP_MAILDATE_FMT10, DRFOP_ASKII, 6,
				IB_ASCII_MAILDATE_TO_STRING }, { DRFOP_MAILDATE_FMT10,
				DRFOP_ASKII, 8, IB_ASCII_MAILDATE_TO_STRING }, {
				DRFOP_MON_BE_2DEC_DP_MS, DRFOP_ASKII, 2,
				BE_INT_MONEY2_DEC2_TO_STRING }, { DRFOP_SECURITY_CODE,
				DRFOP_ASKII, 4, IB_CANADA_SECURITYCODE_TO_STRING }, {
				DRFOP_SQUIG_ASKII, DRFOP_ASKII, 5, SQUIG_ASCII_POSTAGE }, {
				DRFOP_SQUIG_ASKII, DRFOP_ASKII, 6, SQUIG_ASCII_POSTAGE }, {
				DRFOP_SQUIG_BE_MON_3DEC, DRFOP_ASKII, 3, SQUIG_BE_INT_MONEY3 },
		{ 0, 0, 0, 0 },                                 //table terminator
		};

//******************************************************************************
//   FUNCTION NAME: fnfDummyRptFcn
//   AUTHOR       : Clarisa Bellamy
//
//   DESCRIPTION  : Not all function IDs are supported.  This is the function
//                  that is called if an unsupported function ID is encountered.
//   PARAMETERS   : ACII string destination pointer, variable ID
//   RETURN       : 0 - indicating that no characters were copied.
//   NOTES        : None
// ----------------------------------------------------------------------------
UINT16 fnfDummyRptFcn(void * pStrOrValue, UINT16 usVarID) {
	return (0);
}

/******************************************************************************
 FUNCTION NAME: fnCallReportTableFunction
 AUTHOR       : Craig DeFilippo
 DESCRIPTION  : Wrapper to call a report table function.  Calls the report function
 : indicated if the function ID it is within bounds. Otherwise calls
 : fnfDummyRptFcn.
 PARAMETERS   : function id, pointer to result string, variable id
 RETURN       : number of unicode chars, written to result string
 NOTES        : none
 ******************************************************************************/
unsigned short fnCallReportTableFunction(ushort usFnID, void * pStrOrValue,
		unsigned short usVarID) {
	ushort usLen;

	if (usFnID < RptFunctionIDMax)
		usLen = ReportTable[usFnID].ReportFcn(pStrOrValue, usVarID);
	else
		usLen = fnfDummyRptFcn(pStrOrValue, usVarID);

	return (usLen);
}

/************************************************************************************/
/* TITLE: fnFormatRptDoubleMoney                                                    */
/* AUTHOR: defilcj                                                                  */
/* INPUT: empty pStrOrValue, double money (assumes whole numbers only), bFieldTypes,*/
/*        fAddSymbol (indicates if monetary symbol should be added)                 */
/* DESCRIPTION: Format double money quantities for printing w/currency symbol       */
/* OUTPUT: Return number of unicode chars in the Money String                       */
/*                                                                                  */
/* REVISIONS:   Added bFieldTypes as a passed argument.  The new argument allows    */
/*              this function to differentiate between money formatted for a report */
/*              and money formatted for a mail piece.                               */
/*              - blu                                                               */
/************************************************************************************/
unsigned short fnFormatRptDoubleMoney(void * pStrOrValue, double dMoney,
		uchar bFieldTypes, BOOL fAddSymbol) {
	uchar bMinWhole, bMinDec, bCurPlace;
	unichar sCurSym[MAX_CURRENCY_LEN + 1] = { 0, 0, 0, 0, }, *pDest;
	unsigned short usMoneyLen = 0;
	ushort usSymbalID;
	uchar *pCurrSym;
	size_t symLen = 0;

	pDest = (unichar*) pStrOrValue;
	// determine whether the currency symbal preceeds or appends to the value
	bCurPlace = fnFlashGetByteParm(BP_CURRENCY_SYM_PLACEMENT);

	// determine if the currency symbal will be printed in a report or mail piece
	switch (bFieldTypes) {

	case REPORT_FIELDS:
	default:
		usSymbalID = ASP_REPORTS_CURRENCY_SYM;
		break;

	case MAILPIECE_FIELDS:
		usSymbalID = ASP_INDICIA_CURRENCY_STRING;
		break;
	}
	// retrieve the appropriate currency symbal
	pCurrSym = fnFlashGetAsciiStringParm(usSymbalID);
	if (pCurrSym) {
		symLen = strlen((const char *) pCurrSym);
		if ((symLen > 0) && (symLen <= MAX_CURRENCY_LEN)) {
			(void) unistrcpyFromCharStr(sCurSym, (char *) pCurrSym);
		}
	}

	if ((bCurPlace == BEFORE) && (fAddSymbol == TRUE)) {
		(void) unistrcpy(pDest, sCurSym);
		pDest += unistrlen(sCurSym);
	}
	bMinWhole = fnFlashGetByteParm(BP_MIN_PRINTED_INTEGERS);
	bMinDec = fnFlashGetByteParm(BP_MIN_PRINTED_DECIMALS);
	usMoneyLen = fnMoney2Unicode(pDest, dMoney, TRUE, bMinWhole, bMinDec);
	pDest += usMoneyLen;

	if ((bCurPlace == AFTER) && (fAddSymbol == TRUE)) {
		(void) unistrcpy(pDest, sCurSym);
		pDest += unistrlen(sCurSym);
	}

	return ((UINT16) (pDest - (unichar*) pStrOrValue));
}

/******************************************************************************
 FUNCTION NAME: fnFindConvType
 AUTHOR       : Craig DeFilippo
 DESCRIPTION  : search a table to find a conversion type based on the fieldOp
 : and length of the data
 PARAMETERS   : usFieldLen, ucFieldOp1, and ucFieldOp2 are from def rec, see notes
 : specReq is an extra qualifying key, maybe another fieldOp
 : pass back convType
 RETURN       : FALSE if no match is found, true otherwise
 NOTES        : length and fieldOp's are from IPB_GET_INDICIA_DEF_RECORD or
 : IPB_IPSD_PARM_LIST_DEF_REC, and IPB_BUILD_BC_DEF_REC
 MODIFICATION HISTORY:
 08/27/2008  Joe Qu  Merged changes from Janus for NetSet2 Netherlands,
 remove length constraint for byte arrays
 04/25/2003  Craig DeFilippo  Initial version.
 ******************************************************************************/

BOOL fnFindConvType(ushort usFieldLen, uchar ucFieldOp1, uchar ucFieldOp2,
		uchar* pConvType) {
	const FIELD_OP_X_REF* pXref = &tFieldOpXRef[0];
	BOOL fRetVal = FALSE;

	while (pXref->ucFieldOp1) {
		if ((pXref->ucFieldOp1 == ucFieldOp1)
				&& (pXref->ucFieldOp2 == ucFieldOp2)) {
			// no length match constraint for ASCII, or byte arrays
			if ((pXref->ucFieldOp1 == DRFOP_ASKII)
					|| (pXref->ucFieldOp1 == DRFOP_LE_BITE_ARY)
					|| (pXref->ucFieldOp1 == DRFOP_BE_BITE_ARY)
					|| (pXref->ucFieldOp1 == DRFOP_BE_BIGHEX)
					|| (pXref->usFieldLen == usFieldLen)) {
				*pConvType = pXref->ucConvType;
				fRetVal = TRUE;
				break;
			}
		}

		pXref++;
	}

	return (fRetVal);
}

/******************************************************************************
 FUNCTION NAME: fnfEMDFormatDebitCertParms
 AUTHOR       : Craig DeFilippo
 DESCRIPTION  : Read a parameter from the IPB_GET_INDICIA_DEF_REC and convert
 : it to unicode. Use EMD parameters to format min decimals.
 : This is the same as fnfReadDebitCertParms except uses
 : DRFOP_ASKII_MON instead of ASKII as output render type.
 PARAMETERS  : Unicode string destination pointer, variable ID
 RETURN      : length of unicode string, number of unicode chars
 NOTES        : Originally for Spain, useful because we need to right justify
 : postage with proper minimum printed integers and decimals.
 : Usually we left justify postage if we need to limit decimals
 : to less than three.
 ******************************************************************************/
unsigned short fnfEMDFormatDebitCertParms(void * pStrOrValue,
		unsigned short usVarID) {
	IPSD_DEF_REC rFieldRec;
	char* pData;
	ushort usLen = 0;
	uchar ucConvType;

	//get the IBI record from flash
	if (fnFlashGetIBPRecVar(&rFieldRec, IPB_GET_INDICIA_DEF_REC, usVarID)) {
		pData = (char*) pIPSD_barcodeData.pGlob;
		if (fnFindConvType(rFieldRec.usFieldLen, rFieldRec.ucFieldOp,
				DRFOP_ASKII_MON, &ucConvType)) {
			usLen = fnPleaseConvertThis2Unicode(pData + rFieldRec.usFieldOff,
					rFieldRec.usFieldLen, ucConvType, pStrOrValue);
		}
	}

	return (usLen);
}

/******************************************************************************
 FUNCTION NAME: fnfReadDebitCertParms
 AUTHOR       : Craig DeFilippo
 DESCRIPTION  : Read a parameter from the IPB_GET_INDICIA_DEF_REC and convert
 : it to unicode. Use the pIPSD_barcodeData.pGlob as data source.
 PARAMETERS   : Unicode string destination pointer, variable ID
 RETURN       : length of unicode string, number of unicode chars
 NOTES        : None
 ******************************************************************************/
unsigned short fnfReadDebitCertParms(void * pStrOrValue, unsigned short usVarID) {
	IPSD_DEF_REC rFieldRec;
	char* pData;
	uchar ucConvType;
	ushort usLen = 0;

	//get the IBI record from flash
	if (fnFlashGetIBPRecVar(&rFieldRec, IPB_GET_INDICIA_DEF_REC, usVarID)) {
		pData = (char*) pIPSD_barcodeData.pGlob;
		if (fnFindConvType(rFieldRec.usFieldLen, rFieldRec.ucFieldOp,
				DRFOP_ASKII, &ucConvType)) {
			usLen = fnPleaseConvertThis2Unicode(pData + rFieldRec.usFieldOff,
					rFieldRec.usFieldLen, ucConvType, pStrOrValue);
		}
	}

	return (usLen);
}

/******************************************************************************
 FUNCTION NAME: fnfReadModDebitCertParms
 AUTHOR       : Craig DeFilippo
 DESCRIPTION  : Read a parameter from the IPB_GET_INDICIA_DEF_REC and convert
 : it to unicode. Use the pIPSD_ModifiedBarcodeData.pGlob as data source.
 PARAMETERS   : Unicode string destination pointer, variable ID
 RETURN       : length of unicode string, number of unicode chars
 NOTES        :
 1. Same as fnfReadDebitCertParms, except the data source is different.
 ******************************************************************************/
UINT16 fnfReadModDebitCertParms(void *pStrOrValue, UINT16 usVarID) {
	IPSD_DEF_REC rFieldRec;
	char *pData;
	UINT16 usLen = 0;
	UINT8 ucConvType;
	char pLogBuf[50];

	//get the IBI record from flash
	if (fnFlashGetIBPRecVar(&rFieldRec, IPB_GET_INDICIA_DEF_REC, usVarID)) {
		pData = (char *) pIPSD_ModifiedBarcodeData.pGlob;
		if (fnFindConvType(rFieldRec.usFieldLen, rFieldRec.ucFieldOp,
				DRFOP_ASKII, &ucConvType)) {
			usLen = fnPleaseConvertThis2Unicode(pData + rFieldRec.usFieldOff,
					rFieldRec.usFieldLen, ucConvType, pStrOrValue);
		}
	} else {
		(void) sprintf(pLogBuf,
				"fnfReadModDebitCertParms: Couldn't find DefRec data for VCR %u",
				usVarID);
		fnDumpStringToSystemLog(pLogBuf);
	}

	return (usLen);
}

/******************************************************************************
 FUNCTION NAME: fnfReadDebitCertParms2
 AUTHOR       : Clarisa Bellamy
 DESCRIPTION  : Read a parameter from the Barcode, using IPB_GET_INDICIA_DEF_REC_2
 and convert it to unicode.
 : Use the pIPSD_barcodeData.pGlob as data source.
 PARAMETERS   : Unicode string destination pointer, variable ID
 RETURN       : length of unicode string, number of unicode chars
 NOTES        :
 1. Same as fnfReadDebitCertParms, except uses IPB_GET_INDICIA_DEF_REC_2.
 ******************************************************************************/
static UINT16 fnfReadDebitCertParms2(void *pStrOrValue, UINT16 usVarID) {
	IPSD_DEF_REC rFieldRec;
	char *pData;
	UINT16 usLen = 0;
	UINT8 ucConvType;

	//get the IBI record from flash
	if (fnFlashGetIBPRecVar(&rFieldRec, IPB_GET_INDICIA_DEF_REC_2, usVarID)) {
		pData = (char *) pIPSD_barcodeData.pGlob;
		if (fnFindConvType(rFieldRec.usFieldLen, rFieldRec.ucFieldOp,
				DRFOP_ASKII, &ucConvType)) {
			usLen = fnPleaseConvertThis2Unicode(pData + rFieldRec.usFieldOff,
					rFieldRec.usFieldLen, ucConvType, pStrOrValue);
		}
	}

	return (usLen);
}

/******************************************************************************
 FUNCTION NAME: fnfReadDebitCertParmsBcAscii
 AUTHOR       : Craig DeFilippo
 DESCRIPTION  : Read a parameter from the IPB_GET_INDICIA_DEF_REC and convert
 : it to Ascii. Use the fieldOp from the IPB_BUILD_BC_DEF_REC as the
 : second fieldOp key in the call to fnFindConvType.
 : Intended to build a modified barcode data array prior to
 : image generation.
 PARAMETERS   : string destination pointer, variable ID
 RETURN       : length of string, number of chars
 NOTES        : None
 ******************************************************************************/
unsigned short fnfReadDebitCertParmsBcAscii(void * pStrOrValue,
		unsigned short usVarID) {
	IPSD_DEF_REC rFieldRec1;
	IPSD_BC_DEF_REC rFieldRec2;
	char* pData;
	uchar ucConvType;
	ushort usLen = 0;

	//get the IBI record from flash
	if (fnFlashGetIBPRecVar(&rFieldRec1, IPB_GET_INDICIA_DEF_REC, usVarID)
			&& fnFlashGetIBPBcDefRecVar(&rFieldRec2, IPB_BUILD_BC_DEF_REC,
					usVarID)) {
		pData = (char*) pIPSD_barcodeData.pGlob;

		if (fnFindConvType(rFieldRec1.usFieldLen, rFieldRec1.ucFieldOp,
				rFieldRec2.ucFieldOp, &ucConvType)) {
			if (fnPleaseConvertThis2Ascii(pData + rFieldRec1.usFieldOff,
					rFieldRec1.usFieldLen, ucConvType, pStrOrValue)) {
				usLen = (UINT16) strlen(pStrOrValue);
			}

		}
	}

	return (usLen);
}

/******************************************************************************
 FUNCTION NAME: fnfReadDebitCertParmsBcByteArray
 AUTHOR       : Craig DeFilippo
 DESCRIPTION  : Read a parameter from the IPB_GET_INDICIA_DEF_REC and copy the
 : variable from the original debit certificate to the destination
 : using the length in IPB_GET_INDICIA_DEF_REC.
 : Intended to build a modified barcode data array prior to
 : image generation.
 PARAMETERS   : string destination pointer, variable ID
 RETURN       : field length from IPB_GET_INDICIA_DEF_REC
 NOTES        : None
 ******************************************************************************/
UINT16 fnfReadDebitCertParmsBcByteArray(void * pStrOrValue, UINT16 usVarID) {
	IPSD_DEF_REC rFieldRec1;
	char * pData;
	UINT16 usLen = 0;

	//get the IBI record from flash
	if (fnFlashGetIBPRecVar(&rFieldRec1, IPB_GET_INDICIA_DEF_REC, usVarID)) {
		pData = (char *) pIPSD_barcodeData.pGlob;
		(void) memcpy(pStrOrValue, pData + rFieldRec1.usFieldOff,
				rFieldRec1.usFieldLen);
		usLen = rFieldRec1.usFieldLen;
	}

	return (usLen);
}

/******************************************************************************
 FUNCTION NAME: fnfReadDebitCertParms2BcRevrsByteAry
 AUTHOR       : Clarisa Bellamy
 DESCRIPTION  : Read a parameter from the IPB_GET_INDICIA_DEF_REC_2 and copy the
 : variable, in reverse order, from the original debit certificate
 : to the destination using the length in IPB_GET_INDICIA_DEF_REC_2.
 PARAMETERS   : string destination pointer, variable ID
 RETURN       : field length from IPB_GET_INDICIA_DEF_REC_2
 NOTES        :
 1.  Intended to build a modified barcode data array prior to image generation.

 ******************************************************************************/
static UINT16 fnfReadDebitCertParms2BcRevrsByteAry(void * pStrOrValue,
		UINT16 usVarID) {
	IPSD_DEF_REC rFieldRec1;
	UINT8 *pData;
	UINT16 usLen = 0;

	//get the IBI record from flash
	if (fnFlashGetIBPRecVar(&rFieldRec1, IPB_GET_INDICIA_DEF_REC_2, usVarID)) {
		pData = pIPSD_barcodeData.pGlob;
		(void) memcpy(pStrOrValue, pData + rFieldRec1.usFieldOff,
				rFieldRec1.usFieldLen);
		usLen = rFieldRec1.usFieldLen;
		fnReverseIt((UINT8 *) pStrOrValue, usLen);
	}

	return (usLen);
}

/******************************************************************************
 FUNCTION NAME: fnfReadPSDParms
 AUTHOR       : Craig DeFilippo
 DESCRIPTION  : Read a parameter from the IPB_PSD_PARM_DEF_REC and convert
 : it to unicode.
 PARAMETERS   : Unicode string destination pointer, variable ID
 RETURN       : length of unicode string, number of unicode chars
 NOTES        : None
 ******************************************************************************/
unsigned short fnfReadPSDParms(void * pStrOrValue, unsigned short usVarID) {
	IPSD_DEF_REC rFieldRec;
	char* pData;
	uchar ucConvType;
	ushort usLen = 0;

	//get the PSD record from flash
	if (fnFlashGetIBPRecVar(&rFieldRec, IPB_PSD_PARM_DEF_REC, usVarID)) {
		pData = (char*) pIPSD_paramListGlob;     //aPSDParam;
		if (fnFindConvType(rFieldRec.usFieldLen, rFieldRec.ucFieldOp,
				DRFOP_ASKII, &ucConvType)) {
			//Special case for Horizon CSD
			//  - Special case is not needed anymore, since the LONG_TO_STRING conversion has been fixed.
//        	if(usVarID == 6) //Piece count
//        		ucConvType = LE_LONG_TO_STRING;
			usLen = fnPleaseConvertThis2Unicode(pData + rFieldRec.usFieldOff,
					rFieldRec.usFieldLen, ucConvType, pStrOrValue);
		}
	}

	return (usLen);
}

/******************************************************************************
 FUNCTION NAME: fnfReadPSDParmsBcAscii
 AUTHOR       : Craig DeFilippo
 DESCRIPTION  : Read a parameter from the IPB_PSD_PARM_DEF_REC and convert
 : it to Ascii. Use the fieldOp from the IPB_BUILD_BC_DEF_REC as the
 : second fieldOp key in the call to fnFindConvType.
 : Intended to build a modified barcode data array prior to
 : image generation.
 PARAMETERS   : string destination pointer, variable ID
 RETURN       : length of string, number of chars
 NOTES        : None
 ******************************************************************************/
unsigned short fnfReadPSDParmsBcAscii(void * pStrOrValue,
		unsigned short usVarID) {
	IPSD_DEF_REC rFieldRec1;
	IPSD_BC_DEF_REC rFieldRec2;
	char* pData;
	uchar ucConvType;
	ushort usLen = 0;

	//get the PSD record from flash
	if (fnFlashGetIBPRecVar(&rFieldRec1, IPB_PSD_PARM_DEF_REC, usVarID)
			&& fnFlashGetIBPBcDefRecVar(&rFieldRec2, IPB_BUILD_BC_DEF_REC,
					usVarID)) {
		pData = (char*) pIPSD_paramListGlob;     //aPSDParam;

		if (fnFindConvType(rFieldRec1.usFieldLen, rFieldRec1.ucFieldOp,
				rFieldRec2.ucFieldOp, &ucConvType)) {
			if (fnPleaseConvertThis2Ascii(pData + rFieldRec1.usFieldOff,
					rFieldRec1.usFieldLen, ucConvType, pStrOrValue)) {
				usLen = (UINT16) strlen(pStrOrValue);
			}
		}
	}

	return (usLen);
}

/******************************************************************************
 FUNCTION NAME: fnfHRProductCodeAscii
 AUTHOR       : Craig DeFilippo
 DESCRIPTION  : Get the current ascii product code from the dcap module
 PARAMETERS   : string destination pointer, variable ID
 RETURN       : length of string, number of chars
 NOTES        : None
 ******************************************************************************/
unsigned short fnfHRProductCodeAscii(void * pStrOrValue, unsigned short usVarID) {
	ushort usLen = 0;
	return (usLen);
}

/******************************************************************************
 FUNCTION NAME: fnfHRAbrvServiceTypeAscii
 AUTHOR       : Craig DeFilippo
 DESCRIPTION  : Get the current Human readable abbreviated ascii service type
 : from the dcap module.
 PARAMETERS   : string destination pointer, variable ID
 RETURN       : length of string, number of chars
 NOTES        : None
 ******************************************************************************/
unsigned short fnfHRAbrvServiceTypeAscii(void * pStrOrValue,
		unsigned short usVarID) {
	char *pServType;
	ushort usLen = 0;
	uchar ucDcapControl;
	STATUS status = NU_SUCCESS;
	uint16_t length;
	uint32_t type;
	bool is_set;

#if defined (G900) || defined (K700)
	const struct ind_output *pDCInfo;
	SINT16 sRet;
#else
	RMAI_PCL_OUTPUT *pPCLOutput = NULL;
#endif

#if defined (G900) || defined (K700)
	// 3rd generation DCAP
//    sRet = fnRateGetIndiciaInfo(&pDCInfo);
#else
	// 1st Gen w/ PCL data or 2nd Gen
	pPCLOutput = (RMAI_PCL_OUTPUT *)fnRateGetProdConfigListInfo();
#endif

	switch (usVarID) {
	case ITALY_FEECOMBOID :

#if defined (G900) || defined (K700)
		// not needed for G9 for Italy G9C0
#else
		if ((pPCLOutput) && (pPCLOutput->iError == 0) )
		{
			ConvertUnitoAscii((unsigned short *)pPCLOutput->pPCLInfo->FeeComboID, (char *)pStrOrValue, LENCOMBOID);
			usLen = (unsigned short)strlen(pStrOrValue);
		}
#endif
		break;

	default:
		smgr_get_settings_info(SETTING_ITEM_SERVICECODEALPHA, &length, &type,
				&is_set);

		if (is_set) {
			status = smgr_get_setting_data(SETTING_ITEM_SERVICECODEALPHA,
					pStrOrValue, length);
			if (status == NU_SUCCESS) {
				usLen = (unsigned short) strlen(pStrOrValue);
			} else {
				cJSON_AddItemToArray(settingsMgrErrorItem,
						CreateStringItem("Setting Service Code Alpha",
								"Internal Error"));
			}
		}
		break;
	}

	return (usLen);
}

/******************************************************************************
 FUNCTION NAME: fnfHRServiceType
 AUTHOR       : Craig DeFilippo
 DESCRIPTION  : Get the current Human readable service type or mail type string
 : from the dcap module and convert to unicode.
 PARAMETERS   : unichar string destination pointer, variable ID
 RETURN       : length of unichar string, number of unichars
 NOTES        : None
 ******************************************************************************/
unsigned short fnfHRServiceType(void * pStrOrValue, unsigned short usVarID) {
	ushort usLen = 0;
	char *pServType = NULL_PTR;

	if (pServType != NULL_PTR) {
		usLen = (UINT16) strlen(pServType);
		(void) unistrcpyFromCharStr(pStrOrValue, pServType);
	}

	return (usLen);
}

/********************************************************************************/
/* TITLE: fnfLastRefillDate                                                     */
/* AUTHOR: Sam Thillaikumaran                                                   */
/* INPUT: empty pStrOrValue                                                     */
/* DESCRIPTION: Retrieve last refill date and copy it to char string            */
/* OUTPUT: pStrOrValue will hold char string; returns SUCCESSFUL                */
/********************************************************************************/
unsigned short fnfLastRefillDate(void * pStrOrValue, unsigned short usVarID) {
	ushort usLen = 0;

	usLen = fnfRefillStmtVars(pStrOrValue, GET_REFILL_LOG_DOS0);

	return (usLen);
}

/********************************************************************************/
/* TITLE: fnfLastRefillTime                                                     */
/* AUTHOR: Sam Thillaikumaran                                                   */
/* INPUT: empty pStrOrValue                                                     */
/* DESCRIPTION: Retrieve last refill time and copy it to char string            */
/* OUTPUT: pStrOrValue will hold char string; returns SUCCESSFUL                */
/********************************************************************************/
unsigned short fnfLastRefillTime(void * pStrOrValue, unsigned short usVarID) {
	ushort usLen = 0;

	usLen = fnfRefillStmtVars(pStrOrValue, GET_REFILL_LOG_TOS0);

	return (usLen);
}

/********************************************************************************/
/* TITLE:   fnGetMostRecentRefillLogIndex                                      */
/* AUTHOR:  Craig DeFilippo                                                     */
/* INPUT:   none                                                                */
/* DESCRIPTION:  Scan log and return most recent log index                      */
/* Output: index of most recent refill log entry                                */
/********************************************************************************/
unsigned char fnGetMostRecentRefillLogIndex(void) {
	tIPSD_LOG_LMNT_REFILL* pRefillLog = NULL_PTR;
	uchar ucScan = 0, ucMRRIndex = 0;
	ulong ulScanDate = 0, ulMRRDate = 0;

	while (ucScan < IPSD_LOG_MAX_REFILL) {
		pRefillLog = (tIPSD_LOG_LMNT_REFILL*) &pIPSD_RefillLog[ucScan
				* sizeof(tIPSD_LOG_LMNT_REFILL)];
		EndianAwareCopy(&ulScanDate, pRefillLog->pRefillDate,
				sizeof(pRefillLog->pRefillDate));
		if (ulScanDate > ulMRRDate) {
			ulMRRDate = ulScanDate;
			ucMRRIndex = ucScan;
		}
		ucScan++;
	}
	return (ucMRRIndex);
}

#if 0
/********************************************************************************/
/* TITLE:   fnGetNextMostRecentRefillLogIndex                                  */
/* AUTHOR:  Craig DeFilippo                                                     */
/* INPUT:   index value                                                         */
/* DESCRIPTION:  Scan log and return the most recent log index after the date   */
/*               of the passed reference index. If the passed reference index is*/
/*               out of range return the most recent log index.                 */
/* Output: index of next most recent refill log entry                           */
/* Note : This version assumes no logic ordering of the list                    */
/*        But is more complicated than the one below.                           */
/********************************************************************************/
unsigned short fnGetNextMostRecentRefillLogIndex(uchar ucRefIdx)
{
	tIPSD_LOG_LMNT_REFILL* pRefillLog = NULL_PTR;
	ushort ucScan = 0, ucNMRRIndex, ucCount = 0;
	ulong ulScanDate = 0, ulRefDate = 0, ulDateDelta = 0xFFFFFFFF;

	if(ucRefIdx < IPSD_LOG_MAX_REFILL)
	{
		pRefillLog = (tIPSD_LOG_LMNT_REFILL*) &pIPSD_RefillLog[ucRefIdx * sizeof(tIPSD_LOG_LMNT_REFILL)];
		EndianAwareCopy(&ulRefDate, pRefillLog->pRefillDate, sizeof(pRefillLog->pRefillDate));
		ucNMRRIndex = ucRefIdx;

		while(ucScan < IPSD_LOG_MAX_REFILL)
		{
			pRefillLog = (tIPSD_LOG_LMNT_REFILL*) &pIPSD_RefillLog[ucScan * sizeof(tIPSD_LOG_LMNT_REFILL)];
			EndianAwareCopy(&ulScanDate, pRefillLog->pRefillDate, sizeof(pRefillLog->pRefillDate));
			if(ulScanDate < ulRefDate)
			{
				if( (ulRefDate - ulScanDate) < ulDateDelta )
				{
					ulDateDelta = ulRefDate - ulScanDate;
					ucNMRRIndex = ucScan;
				}
			}

			ucCount++;

			if(ucCount < IPSD_LOG_MAX_REFILL)
			{
				ucScan++;
				if(ucScan >= IPSD_LOG_MAX_REFILL)
				ucScan = 0;
			}
			else
			{
				break;
			}
		}
	}
	else
	{
		ucNMRRIndex = fnGetMostRecentRefillLogIndex();
	}

	return(ucNMRRIndex);
}
#else
/********************************************************************************/
/* TITLE:   fnGetNextMostRecentRefillLogIndex                                  */
/* AUTHOR:  Craig DeFilippo                                                     */
/* INPUT:   index value                                                         */
/* DESCRIPTION:  Scan log and return the most recent log index after the date   */
/*               of the passed reference index. If the passed reference index is*/
/*               out of range return the most recent log index.                 */
/* Output: index of next most recent refill log entry                           */
/* Note : Assumes the log is created as a circular list.  I.E. new entries      */
/*        appended to the end, and oldest entry overwritten when wrapping       */
/********************************************************************************/
// simple version
unsigned char fnGetNextMostRecentRefillLogIndex(uchar ucRefIdx) {
	uchar ucNMRRIndex;

	//default
	ucNMRRIndex = fnGetMostRecentRefillLogIndex();

	if (ucRefIdx < IPSD_LOG_MAX_REFILL) {
		if (ucRefIdx > 0) {
			ucNMRRIndex = ucRefIdx - 1;
		} else {
			ucNMRRIndex = IPSD_LOG_MAX_REFILL - 1;
		}
	}

	return (ucNMRRIndex);
}

#endif
/********************************************************************************/
/* TITLE:   fnGetRefillLogIndex                                                 */
/* AUTHOR:  Craig DeFilippo                                                     */
/* INPUT:   index value                                                         */
/* DESCRIPTION:  Scan log and return the index of the entry corresponding to    */
/*               the passed ordinal reference.  e.g. 0 = most recent,           */
/*               1 = second most recent, etc.                                   */
/* Output: index of refill log entry                                            */
/********************************************************************************/
unsigned char fnGetRefillLogIndex(uchar ucWhich) {
	uchar ucIndex = 0, ucCount;

	ucIndex = fnGetMostRecentRefillLogIndex();

	if (ucWhich < IPSD_LOG_MAX_REFILL) {
		for (ucCount = 0; ucCount < ucWhich; ucCount++) {
			ucIndex = fnGetNextMostRecentRefillLogIndex(ucIndex);
		}
	}

	return (ucIndex);
}

/********************************************************************************/
/* TITLE:   fnGetRefillRecordIndex                                              */
/* AUTHOR:  Craig DeFilippo                                                     */
/* INPUT:   index value                                                         */
/* DESCRIPTION:  Scan the variable id to record index table to find the record  */
/*               index ( 0...) for this refill variable                         */
/* Output: index of refill log entry                                            */
/********************************************************************************/
unsigned char fnGetRefillRecordIndex(ushort usVarID) {
	ushort i = 0;

	while ((rRefillVarId2RecordIndex[i][0] > 0)
			&& (rRefillVarId2RecordIndex[i][0] != usVarID)) {
		i++;
	}

	return (rRefillVarId2RecordIndex[i][1]);
}
/********************************************************************************/
/* TITLE: fnfRefillStmtVars                                                     */
/* AUTHOR:  Craig DeFilippo                                                     */
/* INPUT: empty pStrOrValue, Variable ID whose pieces must be retrieved         */
/* DESCRIPTION:  Retrieve refill log variables and translate to unichar string  */
/*  Variable ID's Supported are:                                                */
/*                              GET_REFILL_LOG_DOS0 - 4                         */
/*                              GET_REFILL_LOG_TOS0 - 4                         */
/*                              GET_REFILL_LOG_AMT0 - 4                         */
/* Output: number of characters in output string                                */
/********************************************************************************/
unsigned short fnfRefillStmtVars(void * pStrOrValue, unsigned short usVarID) {

	unsigned short usLen = 0;
	tIPSD_LOG_LMNT_REFILL* pRefillLog = NULL_PTR;
	DATETIME DateTime;
	long IPSDtime;
	char tStr[30] = { 0, 0, 0, 0 };
	uchar MoneyArray[SPARK_MONEY_SIZE], recordIndex;

	recordIndex = fnGetRefillLogIndex(fnGetRefillRecordIndex(usVarID));

	if (recordIndex < IPSD_LOG_MAX_REFILL) {
		pRefillLog = (tIPSD_LOG_LMNT_REFILL*) &pIPSD_RefillLog[recordIndex
				* sizeof(tIPSD_LOG_LMNT_REFILL)];

		// if date is 0 then the log entry is empty
		if (memcmp(pRefillLog->pRefillDate, tStr,
				sizeof(pRefillLog->pRefillDate)) != 0) {
			switch (usVarID) {

			case GET_REFILL_LOG_DOS0 :
			case GET_REFILL_LOG_DOS1 :
			case GET_REFILL_LOG_DOS2 :
			case GET_REFILL_LOG_DOS3 :
			case GET_REFILL_LOG_DOS4 :
				EndianAwareCopy(&IPSDtime, pRefillLog->pRefillDate,
						sizeof(pRefillLog->pRefillDate));
				fnIPSDTimeToSys(&DateTime, IPSDtime);
				ConvertFromGMTToLocal(&DateTime, TRUE);
				if (fnFormatDate(tStr, &DateTime, TRUE) == SUCCESSFUL)
					usLen = (UINT16) strlen(tStr);
				break;

			case GET_REFILL_LOG_TOS0 :
			case GET_REFILL_LOG_TOS1 :
			case GET_REFILL_LOG_TOS2 :
			case GET_REFILL_LOG_TOS3 :
			case GET_REFILL_LOG_TOS4 :
				EndianAwareCopy(&IPSDtime, pRefillLog->pRefillDate,
						sizeof(pRefillLog->pRefillDate));
				fnIPSDTimeToSys(&DateTime, IPSDtime);
				ConvertFromGMTToLocal(&DateTime, TRUE);
				if (fnFormatTime(tStr, &DateTime, usVarID) == SUCCESSFUL)
					usLen = (UINT16) strlen(tStr);
				break;

			case GET_REFILL_LOG_AMT0 :
			case GET_REFILL_LOG_AMT1 :
			case GET_REFILL_LOG_AMT2 :
			case GET_REFILL_LOG_AMT3 :
			case GET_REFILL_LOG_AMT4 :
				GetCurrencySymbol();

				if (bPlacement == BEFORE) {
					strcpy(tStr, pCurrencySym);
					usLen = (UINT16) strlen(tStr);
				}
				memset(MoneyArray, 0, sizeof(MoneyArray));
				memcpy(MoneyArray + 1, pRefillLog->m4RefillAmt, sizeof(long));
				(void) fnMoney2Ascii(&usLen, tStr + usLen, MoneyArray);

				if (bPlacement == AFTER) {
					strcat(tStr + usLen, pCurrencySym);
				}

				usLen = (UINT16) strlen(tStr);
				break;

			default:
				break;
			}
		}
	}

	if (usLen) {
		(void) unistrcpyFromCharStr(pStrOrValue, tStr);
		retcode = SUCCESSFUL;
	}

	return (usLen);
}

//*******************************************************************************
// TITLE:               fnGetMostRecentBalInqLogIndex
// DESCRIPTION:
//      Return the index of the most recent entry in the Balance Inquiry log.
//      This handles wrapping.
// INPUTS:
//      None
// Output:
//      UINT8 - Index of Most Recent Balance Inquiry log entry.
//NOTES:
//  1. If there are NO entries, this will return 0xFF.
//
// HISTORY:
// 2009.05.14 Clarisa Bellamy - Initial version.
//-------------------------------------------------------------------------------
UINT8 fnGetMostRecentBalInqLogIndex(void) {
	UINT8 ucRetVal = 0;

	// Index points to next entry to write to.
	ucRetVal = CMOSBalanceInquiryLog.ucIndex;
	// Adjust to the last entry...
	if (CMOSBalanceInquiryLog.ucIndex) {
		ucRetVal--;
	} else    // Index is zero, do we have ANY entries yet?
	{
		if (CMOSBalanceInquiryLog.fWrapped) {
			ucRetVal = NUM_BAL_INQ_ENTRIES - 1;
		} else {
			ucRetVal = 0xFF;
		}
	}

	return (ucRetVal);
}

//*******************************************************************************
// TITLE:               fnGetNextMostRecentBalInqLogIndex
// DESCRIPTION:
//      Scan log and return the index of the entry previous index passed in.
//      This handles wrapping.
//
// INPUT:
//      UINT8 ucRefIdx - Reference Index.
// Output:
//      UINT8 - Index of Balance Inquiry log entry previous to Reference Index.
//
//NOTES:
//  1. If the passed reference index is out of range return the most recent
//      log index.
//  2. Assumes the log is created as a circular list.  I.E. new entries
//      appended to the end, and oldest entry overwritten when wrapping.
//
// HISTORY:
// 2009.05.14 Clarisa Bellamy - Initial version, adapted from fnGetNextMostRecentRefillLogIndex.
//-------------------------------------------------------------------------------
UINT8 fnGetNextMostRecentBalInqLogIndex(UINT8 ucRefIdx) {
	uchar ucNMRIndex = 0xFF;       // Default to indicate empty log.

	if (ucRefIdx < NUM_BAL_INQ_ENTRIES) {
		if (ucRefIdx > 0) {
			ucNMRIndex = ucRefIdx - 1;
		} else if (CMOSBalanceInquiryLog.fWrapped) {
			ucNMRIndex = NUM_BAL_INQ_ENTRIES - 1;
		}
	} else {
		// Default to THE most recent entry:
		ucNMRIndex = fnGetMostRecentBalInqLogIndex();
	}

	return (ucNMRIndex);
}

//*******************************************************************************
// TITLE:               fnGetOrderedBalanceInquiryLogIndex
// DESCRIPTION:
//      Scan log and return the index of the entry corresponding to the
//       passed in ordinal reference.  e.g.  0 = most recent,
//                                           1 = second most recent, etc.
// INPUT:
//      UINT8 ucWhich - Ordinal value: 0 = most recent.
// Output:
//      UINT8 - Index of Balance Inquiry log entry corresponding to ordinal value.
//
//  NOTES:
//   1. If the ordinal is more than the number of entries the log can hold,
//      return the index to the most recent entry.
//   2. If there are no entries, or there aren't enough entries, return a number
//      >= NUM_BAL_INQ_ENTRIES.
//
// HISTORY:
// 2009.05.14 Clarisa Bellamy - Initial version, adapted from fnGetRefillLogIndex.
//-------------------------------------------------------------------------------
UINT8 fnGetOrderedBalanceInquiryLogIndex(UINT8 ucWhich) {
	UINT8 ucIndex = 0;
	UINT8 ucCount = 0;

	ucIndex = fnGetMostRecentBalInqLogIndex();

	// Range check.  If ucIndex >= NUM_BAL_INQ_ENTRIES, then there isn't an
	//  entry yet.  If ucWhich >= NUM_BAL_INQ_ENTRIES, then we just get the
	//  first entry, if there is one.
	if ((ucIndex < NUM_BAL_INQ_ENTRIES) && (ucWhich < NUM_BAL_INQ_ENTRIES)) {
		// If ucIndex >= NUM_BAL_INQ_ENTRIES before we get to the nth entry,
		//  then there aren't that many entries in the log yet.
		while ((ucIndex < NUM_BAL_INQ_ENTRIES) && (ucCount < ucWhich)) {
			ucIndex = fnGetNextMostRecentBalInqLogIndex(ucIndex);
			ucCount++;
		}
	}

	return (ucIndex);
}

//*******************************************************************************
// TITLE:               fnGetBalInqRecordOrdinal
// DESCRIPTION:
//      Scan the variable id to record ordinal table to find the record ordinal
//       ( 0...) for this balance inquiry variable.
// INPUT:
//      UINT8 uxVarId - Variable ID (from the VCR).
// Output:
//      UINT8 - Ordinal of corresponding Balance Inquiry log entry.
// HISTORY:
// 2009.05.14 Clarisa Bellamy - Initial version, adapted from fnGetRefillRecordIndex.
//-------------------------------------------------------------------------------
UINT8 fnGetBalInqRecordOrdinal(UINT16 usVarID) {
	UINT8 ubOrdinal = 0;   // Not found (return ordinal 0.)
	UINT16 uwIndex = 0;

	while ((rBalInqVarId2RecordOrdinal[uwIndex][0] > 0)
			&& (rBalInqVarId2RecordOrdinal[uwIndex][0] != usVarID)) {
		uwIndex++;
	}

	if (rBalInqVarId2RecordOrdinal[uwIndex][0] > 0) {
		ubOrdinal = rBalInqVarId2RecordOrdinal[uwIndex][1];
	}
	return (ubOrdinal);
}

//*******************************************************************************
// TITLE:               fnfBalanceInquiryStmtVars
// DESCRIPTION:
//      Retrieve Balane Inquiry Log variables and translate to unichar string.
//  Variable ID's Supported are:
//                              GET_BALINQ_LOG_DOS0 - 4
//                              GET_BALINQ_LOG_TOS0 - 4
//                              GET_BALINQ_LOG_ACTION0 - 4
// INPUT:
//      pStrOrValue - Pointer to empty unichar buffer to put result into.
//      UINT16 usVarID -   Variable ID (from the VCR).
// RETURNS:
//      UINT16 - Number of unistring chars written to pStrOrValue.
// Outputs:
//      Writes unichar data to pStrOrValue.
// HISTORY:
// 2009.05.14 Clarisa Bellamy - Initial version, adapted from fnfRefillStmtVars.
//-------------------------------------------------------------------------------
static UINT16 fnfBalanceInquiryStmtVars(void * pStrOrValue, UINT16 usVarID) {
	T_CMOSBalInqLogEntry *pBalInqLog;
	char *pStr = NULL_PTR;
	UINT16 uwLen = 0;
	DATETIME DateTime;
	UINT32 IPSDtime;
	char tStr[30] = { 0, 0, 0, 0, 0, 0, 0, 0 };
	UINT16 uwLogIndex;

	uwLogIndex = fnGetOrderedBalanceInquiryLogIndex(
			fnGetBalInqRecordOrdinal(usVarID));

	if (uwLogIndex < NUM_BAL_INQ_ENTRIES) {
		pBalInqLog = &CMOSBalanceInquiryLog.aLog[uwLogIndex];

		// if date is 0 then the log entry is empty
		if (memcmp(&pBalInqLog->sTimeStamp, tStr,
				sizeof(pBalInqLog->sTimeStamp)) != 0) {
			switch (usVarID) {

			case GET_BALINQ_LOG_DOS0 :
			case GET_BALINQ_LOG_DOS1 :
			case GET_BALINQ_LOG_DOS2 :
			case GET_BALINQ_LOG_DOS3 :
			case GET_BALINQ_LOG_DOS4 :
				DateTime = pBalInqLog->sTimeStamp;
				if (fnFormatDate(tStr, &DateTime, TRUE) == SUCCESSFUL)
					uwLen = (UINT16) strlen(tStr);
				break;

			case GET_BALINQ_LOG_TOS0 :
			case GET_BALINQ_LOG_TOS1 :
			case GET_BALINQ_LOG_TOS2 :
			case GET_BALINQ_LOG_TOS3 :
			case GET_BALINQ_LOG_TOS4 :
				DateTime = pBalInqLog->sTimeStamp;
				if (fnFormatTime(tStr, &DateTime, usVarID) == SUCCESSFUL)
					uwLen = (UINT16) strlen(tStr);
				break;

			case GET_BALINQ_LOG_ACTION0 :
			case GET_BALINQ_LOG_ACTION1 :
			case GET_BALINQ_LOG_ACTION2 :
			case GET_BALINQ_LOG_ACTION3 :
			case GET_BALINQ_LOG_ACTION4 :
				// Get the pointer from the appropriate language string array.
				//  This array is updated on startup and whenever the
				//  language is changed.
				pStr = BalanceInquiry[0];

				// Make a local copy of the string, so it can be converted to
				//  a unicode string later.
				if (pStr != NULL_PTR) {
					uwLen = (UINT16) strlen(pStr);
					if (uwLen >= sizeof(tStr))
						uwLen = sizeof(tStr) - 1;
					strncpy(tStr, pStr, uwLen);
					tStr[uwLen] = 0;
				}
				break;

			default:
				break;
			}
		}
	}

	// If we have a non-blank string, convert it to unicode.
	if (uwLen) {
		unistrcpyFromCharStr(pStrOrValue, tStr);
		retcode = SUCCESSFUL;
	}

	return (uwLen);
}

/********************************************************************************/
/* TITLE: fnfGetAccountBalance                                                  */
/* AUTHOR: BMP                                                                  */
/* INPUT: empty pStrOrValue,usVarID                                             */
/* DESCRIPTION: Retrieve Money Parameter from Modem and convert to char ptr     */
/* OUTPUT: Return number of unicode chars in PBP account balance                */
/********************************************************************************/
unsigned short fnfGetAccountBalance(void * pStrOrValue, unsigned short usVarID) {
	unsigned short Len = 0;
	char chAsciiArray[18] = { 0 };
	unsigned char MoneyArray[SPARK_MONEY_SIZE];
	double dblMoneyVal;
	BOOL fNegBalance = FALSE;   // allow negative balance for swiss requirements

	dblMoneyVal = fnfGetCMOSDoubleParam(CMOS_DB_PBP_DEBIT_BAL);
	if (dblMoneyVal < 0) {
		fNegBalance = TRUE;
		dblMoneyVal *= -1;
	}

	fnDouble2BinFive(MoneyArray, dblMoneyVal, TRUE);
	GetCurrencySymbol();

	if (bPlacement == BEFORE) {
		strcpy(chAsciiArray, pCurrencySym);
		Len = (UINT16) strlen(chAsciiArray);
	}

	if (fNegBalance) {
		strcat(chAsciiArray + Len, "-");
		Len = (UINT16) strlen(chAsciiArray);
	}

	(void) fnMoney2Ascii(&Len, chAsciiArray + Len, MoneyArray);

	if (bPlacement == AFTER) {
		strcat(chAsciiArray + Len, pCurrencySym);
	}

	(void) unistrcpyFromCharStr(pStrOrValue, chAsciiArray);
	Len = (UINT16) strlen(chAsciiArray);

	return (Len);
}

/********************************************************************************/
/* TITLE: fnfGetCreditLine                                                      */
/* AUTHOR: BMP                                                                  */
/* INPUT: empty pStrOrValue,                                                    */
/* DESCRIPTION: Retrieve Money Parameter from Modem and convert to char ptr     */
/* OUTPUT: Return number of unicode chars in credit line                        */
/********************************************************************************/
unsigned short fnfGetCreditLine(void * pStrOrValue, unsigned short usVarID) {
	unsigned short Len = 0;
	char chAsciiArray[18] = { 0 };
	unsigned char MoneyArray[SPARK_MONEY_SIZE];
	double dblMoneyVal;

	dblMoneyVal = fnfGetCMOSDoubleParam(CMOS_DB_PBP_CREDIT_BAL);
	fnDouble2BinFive(MoneyArray, dblMoneyVal, TRUE);
	GetCurrencySymbol();

	if (bPlacement == BEFORE) {
		strcpy(chAsciiArray, pCurrencySym);
		Len = (UINT16) strlen(chAsciiArray);
	}

	(void) fnMoney2Ascii(&Len, chAsciiArray + Len, MoneyArray);

	if (bPlacement == AFTER) {
		strcat(chAsciiArray, pCurrencySym);
	}

	(void) unistrcpyFromCharStr(pStrOrValue, chAsciiArray);
	Len = (UINT16) strlen(chAsciiArray);

	return Len;
}

/********************************************************************************/
/* TITLE: fnfGetPBPActNumber                                                    */
/* AUTHOR: defilcj                                                              */
/* INPUT: empty pStrOrValue,                                                    */
/* DESCRIPTION: Retrieve PBP account Number                                     */
/* OUTPUT: Return number of unicode chars in PBP account number                 */
/********************************************************************************/
unsigned short fnfGetPBPActNumber(void * pStrOrValue, unsigned short usVarID) {
	return (fnfGetCMOSRptPackedByteParam(pStrOrValue, CMOS_PB_PBP_ACCT_NBR));
}

/********************************************************************************/
/* TITLE: fnfGetBatchCount                                                      */
/* AUTHOR: defilcj                                                              */
/* INPUT: empty pStrOrValue,                                                    */
/* DESCRIPTION: Retrieve batch counter                                          */
/* OUTPUT: Return number of unicode chars in batch counter                      */
/********************************************************************************/
unsigned short fnfGetBatchCount(void * pStrOrValue, unsigned short usVarID) {
	unsigned short Len = 0, *pUniString;
	unsigned long ulBatchCount = 0;

	ulBatchCount = (unsigned long) fnGetCurReportBatchCount();
	Len = fnBin2Unicode(pStrOrValue, ulBatchCount);
	pUniString = pStrOrValue;
	*(pUniString + Len) = 0;  //null terminate the unistring
	return Len;
}

/********************************************************************************/
/* TITLE: fnfGetBatchAmount                                                     */
/* AUTHOR: defilcj                                                              */
/* INPUT: empty pStrOrValue,                                                    */
/* DESCRIPTION: Retrieve batch amount                                           */
/* OUTPUT: Return number of unicode chars in batch amount                       */
/********************************************************************************/
unsigned short fnfGetBatchAmount(void * pStrOrValue, unsigned short usVarID) {
	unsigned short usLen = 0;
	double dBatch = 0.0;

	dBatch = (double) fnGetCurReportBatchValue();
	usLen = fnFormatRptDoubleMoney(pStrOrValue, dBatch, REPORT_FIELDS, TRUE);

	return (usLen);
}

/*****************************************************************************/
/* TITLE: fnfGetDate                                                         */
/* AUTHOR: BMP, WFB                                                          */
/* INPUT: empty pStrOrValue, bField                                          */
/* DESCRIPTION: Gets the report date format and uses that parameter to get   */
/*              the System Date Time in a date format                        */
/* OUTPUT: Returns number of unicode chars in date                           */
/* modified 7/1/03, defilcj                                                  */
/*****************************************************************************/
unsigned short fnfGetDate(void * pStrOrValue, unsigned short usVarID) {
	DATETIME DateTime;
	ushort usLen = 0;
	char aAsciiDate[15] = { 0, 0 };
	unichar unicodeDate[12] = { 0, 0 };
	BOOL bAsciiToUnicodeConversionNeeded = TRUE;

	uchar ucStatus = 0;

	switch (usVarID) {
	case INDICIA_DATE_WITHOUT_DATE_SEP :
		// adjusted by advanced date
		// not including date separator
		if (GetPrintedDate(&DateTime, true))
			ucStatus = fnFormatDate(aAsciiDate, &DateTime, FALSE);
		break;

	case INDICIA_DATE_WITH_DATE_SEP :
		// adjusted by advanced date
		// including date separator
		if (GetPrintedDate(&DateTime, true))
			ucStatus = fnFormatDate(aAsciiDate, &DateTime, TRUE);
		break;

	case CURRENT_DATE_JAPANESE_FORMAT :
		// not adjusted by advanced date
		// Japan Format
		if (GetSysDateTime(&DateTime, true, true))
			ucStatus = fnFormatJapaneseDate(unicodeDate, &DateTime);
		bAsciiToUnicodeConversionNeeded = FALSE;
		break;


	case CURRENT_DATE_WITH_DATE_SEP :
	default:
		// not adjusted by advanced date
		// including date separator
		if (GetSysDateTime(&DateTime, true, true))
			ucStatus = fnFormatDate(aAsciiDate, &DateTime, TRUE);
		break;
	}

	if (ucStatus == SUCCESSFUL) {
		if(bAsciiToUnicodeConversionNeeded) {
			(void) unistrcpyFromCharStr(pStrOrValue, aAsciiDate);
			usLen = (UINT16) strlen(aAsciiDate);
		}
		else{
			(void) unistrcpy(pStrOrValue,  unicodeDate);
			usLen = unistrlen( pStrOrValue );
		}
	}

	return (usLen);
}

/*****************************************************************************/
/* TITLE: fnfFormatDate                                                      */
/* AUTHOR: WFB                                                               */
/* INPUT: empty pStrOrValue, datetime pointer                                */
/* DESCRIPTION: Take a pointer to a DATETIME structure and returns a formatted
 date string for reports
 This was split out of fnfGetDate to make it more generic        */
/* OUTPUT: Returns successful or ERROR                                  */
/* modified 8/25/04, Victor                     */
/*****************************************************************************/
unsigned char fnfFormatDate(void * pStrOrValue, const DATETIME * SysDateTimePtr) {
	return (fnFormatDate(pStrOrValue, SysDateTimePtr, TRUE));
}

/*****************************************************************************/
/* TITLE: fnFormatDate                                                      */
/* AUTHOR: WFB                                                               */
/* INPUT: empty pStrOrValue, datetime pointer, flag to include date separator char */
/* DESCRIPTION: Take a pointer to a DATETIME structure and returns a formatted
 date string for reports
 This was split out of fnfGetDate to make it more generic                */
/* OUTPUT: Returns successful or ERROR                                      */
/*****************************************************************************/
unsigned char fnFormatDate(void * pStrOrValue, const DATETIME * SysDateTimePtr,
		BOOL fIncSepChar) {
	UINT8 option;
	char DateSeparation = 0;

	option = fnFlashGetByteParm(BP_REPORT_DATE_FORMAT);
	if (fIncSepChar) {
		DateSeparation = (char) fnFlashGetByteParm(BP_PRINTABLE_DATE_SEP_CHAR);
	}

	retcode = fnFormatDateASCII(pStrOrValue, SysDateTimePtr, option,
			DateSeparation, DateSeparation);

	return retcode;
}

/*****************************************************************************/
/* TITLE: fnFormatJapaneseDate                                                      */
/* AUTHOR: WFB                                                               */
/* INPUT: empty pStrOrValue, datetime pointer,  */
/* DESCRIPTION: Take a pointer to a DATETIME structure and returns a formatted
 date unicode string for reports
/* OUTPUT: Returns successful or ERROR                                      */
/*****************************************************************************/
unsigned char fnFormatJapaneseDate(unichar * pBuff, const DATETIME * SysDateTimePtr) {
	unichar ucYearSeparator = 0x5E74;
	unichar ucMonthSeparator = 0x6708;
	unichar ucDaySeparator = 0x65E5;
	char aAsciiDate[12];

	snprintf(aAsciiDate, sizeof(aAsciiDate), "%02d%02dY%dM%dD", SysDateTimePtr->bCentury, SysDateTimePtr->bYear, SysDateTimePtr->bMonth,
			SysDateTimePtr->bDay);
	(void) unistrcpyFromCharStr(pBuff, aAsciiDate);

	for(int i = 0; i < 12; i++)
	{
		if(pBuff[i] == (unichar) 'Y')
			pBuff[i] = ucYearSeparator;
		if(pBuff[i] == (unichar) 'M')
			pBuff[i] = ucMonthSeparator;
		if(pBuff[i] == (unichar) 'D')
			pBuff[i] = ucDaySeparator;
	}

	return SUCCESSFUL;
}

/*****************************************************************************/
// TITLE:           fnFormatDateASCII
// AUTHOR: WFB
// DESCRIPTION:
//  Formats the time in the date/time structure into a string, of the indicated
//  format.
//   This was split out of fnFormatDate to make it more generic
// INPUTS:
//      pStrOrValue - Pointer to destination buffer where string is to be written.
//                  Must be big enough to hold the date and the null-terminator.
//                  The date may be from 6 - 11 chars long.
//      SysDateTimePtr - Pointer to structure which holds the date/time to be
//                  printed.  The DATETIME structure is defined in global.h,
//                  And is parsed into easily recognizable parts.
//      ubFormatOption - Date Format control byte, Defined in the datDict.h
//                  DATE_FORMAT_*
//      DateSeparation - Date separation character, 0 = no date separation char.
//
// OUTPUT:
//  A formatted string written into the destination buffer.  If the buffer is
//  too short data will be overwritten.
// RETURNS:
//  SUCCESSFUL                      if formatted date was copied to buffer.
//  ERR_GET_DATE_SWITCH (53, 0x35)  if the format is unknown.  Nothing written
//                                  to buffer.
// NOTES:
//  This is static for now.  But it could be made global.
/*****************************************************************************/
static UINT8 fnFormatDateASCII(void * pStrOrValue,
		const DATETIME * SysDateTimePtr, UINT8 ubFormatOption,
		char cDateSeparation1, char cDateSeparation2) {
	char chMonth[4];
	BOOL DateSeparation = FALSE;

	retcode = SUCCESSFUL;

	// To include date separators, they both must be non-zero. It's all or nothing baby!
	DateSeparation = (cDateSeparation1 != 0) && (cDateSeparation2 != 0);

	if (DateSeparation) {
		switch (ubFormatOption & DATE_FORMAT_BASE) {
		case DATE_FORMAT_MMMDDYY:
			GetCurrentMonth(SysDateTimePtr->bMonth, chMonth);
			sprintf(pStrOrValue, "%s%c%02d%c%02d", chMonth, cDateSeparation1,
					SysDateTimePtr->bDay, cDateSeparation2,
					SysDateTimePtr->bYear);
			break;
		case DATE_FORMAT_MMDDYY:
			sprintf(pStrOrValue, "%02d%c%02d%c%02d", SysDateTimePtr->bMonth,
					cDateSeparation1, SysDateTimePtr->bDay, cDateSeparation2,
					SysDateTimePtr->bYear);
			break;
		case DATE_FORMAT_DDMMYY:
			sprintf(pStrOrValue, "%02d%c%02d%c%02d", SysDateTimePtr->bDay,
					cDateSeparation1, SysDateTimePtr->bMonth, cDateSeparation2,
					SysDateTimePtr->bYear);
			break;
		case DATE_FORMAT_YYMMDD:
			sprintf(pStrOrValue, "%02d%c%02d%c%02d", SysDateTimePtr->bYear,
					cDateSeparation1, SysDateTimePtr->bMonth, cDateSeparation2,
					SysDateTimePtr->bDay);
			break;
		case DATE_FORMAT_DDMMMYY:
			GetCurrentMonth(SysDateTimePtr->bMonth, chMonth);
			sprintf(pStrOrValue, "%02d%c%s%c%02d", SysDateTimePtr->bDay,
					cDateSeparation1, chMonth, cDateSeparation2,
					SysDateTimePtr->bYear);
			break;
		case DATE_FORMAT_DDMMMYYYY:
			GetCurrentMonth(SysDateTimePtr->bMonth, chMonth);
			sprintf(pStrOrValue, "%02d%c%s%c%d%02d", SysDateTimePtr->bDay,
					cDateSeparation1, chMonth, cDateSeparation2,
					SysDateTimePtr->bCentury, SysDateTimePtr->bYear);
			break;
		case DATE_FORMAT_MMMDDYYYY:
			GetCurrentMonth(SysDateTimePtr->bMonth, chMonth);
			sprintf(pStrOrValue, "%s%c%02d%c%2d%02d", chMonth, cDateSeparation1,
					SysDateTimePtr->bDay, cDateSeparation2,
					SysDateTimePtr->bCentury, SysDateTimePtr->bYear);
			break;
		case DATE_FORMAT_DDMMYYYY:
			sprintf(pStrOrValue, "%02d%c%02d%c%d%02d", SysDateTimePtr->bDay,
					cDateSeparation1, SysDateTimePtr->bMonth, cDateSeparation2,
					SysDateTimePtr->bCentury, SysDateTimePtr->bYear);
			break;
		case DATE_FORMAT_MMDDYYYY:
			sprintf(pStrOrValue, "%02d%c%02d%c%d%02d", SysDateTimePtr->bMonth,
					cDateSeparation1, SysDateTimePtr->bDay, cDateSeparation2,
					SysDateTimePtr->bCentury, SysDateTimePtr->bYear);
			break;
		case DATE_FORMAT_YYYYMMDD:
			sprintf(pStrOrValue, "%d%02d%c%02d%c%02d", SysDateTimePtr->bCentury,
					SysDateTimePtr->bYear, cDateSeparation1,
					SysDateTimePtr->bMonth, cDateSeparation2,
					SysDateTimePtr->bDay);
			break;

		default:
			retcode = ERR_GET_DATE_SWITCH_LT10;
			break;
		}
	} else {
		switch (ubFormatOption & DATE_FORMAT_BASE) {
		case DATE_FORMAT_MMMDDYY:
			GetCurrentMonth(SysDateTimePtr->bMonth, chMonth);
			sprintf(pStrOrValue, "%s%02d%02d", chMonth, SysDateTimePtr->bDay,
					SysDateTimePtr->bYear);
			break;
		case DATE_FORMAT_MMDDYY:
			sprintf(pStrOrValue, "%02d%02d%02d", SysDateTimePtr->bMonth,
					SysDateTimePtr->bDay, SysDateTimePtr->bYear);
			break;
		case DATE_FORMAT_DDMMYY:
			sprintf(pStrOrValue, "%02d%02d%02d", SysDateTimePtr->bDay,
					SysDateTimePtr->bMonth, SysDateTimePtr->bYear);
			break;
		case DATE_FORMAT_YYMMDD:
			sprintf(pStrOrValue, "%02d%02d%02d", SysDateTimePtr->bYear,
					SysDateTimePtr->bMonth, SysDateTimePtr->bDay);
			break;
		case DATE_FORMAT_DDMMMYY:
			GetCurrentMonth(SysDateTimePtr->bMonth, chMonth);
			sprintf(pStrOrValue, "%02d%s%02d", SysDateTimePtr->bDay, chMonth,
					SysDateTimePtr->bYear);
			break;
		case DATE_FORMAT_DDMMMYYYY:
			GetCurrentMonth(SysDateTimePtr->bMonth, chMonth);
			sprintf(pStrOrValue, "%02d%s%d%02d", SysDateTimePtr->bDay, chMonth,
					SysDateTimePtr->bCentury, SysDateTimePtr->bYear);
			break;
		case DATE_FORMAT_MMMDDYYYY:
			GetCurrentMonth(SysDateTimePtr->bMonth, chMonth);
			sprintf(pStrOrValue, "%s%02d%2d%02d", chMonth, SysDateTimePtr->bDay,
					SysDateTimePtr->bCentury, SysDateTimePtr->bYear);
			break;
		case DATE_FORMAT_DDMMYYYY:
			sprintf(pStrOrValue, "%02d%02d%d%02d", SysDateTimePtr->bDay,
					SysDateTimePtr->bMonth, SysDateTimePtr->bCentury,
					SysDateTimePtr->bYear);
			break;
		case DATE_FORMAT_MMDDYYYY:
			sprintf(pStrOrValue, "%02d%02d%d%02d", SysDateTimePtr->bMonth,
					SysDateTimePtr->bDay, SysDateTimePtr->bCentury,
					SysDateTimePtr->bYear);
			break;
		case DATE_FORMAT_YYYYMMDD:
			sprintf(pStrOrValue, "%d%02d%02d%02d", SysDateTimePtr->bCentury,
					SysDateTimePtr->bYear, SysDateTimePtr->bMonth,
					SysDateTimePtr->bDay);
			break;

		default:
			retcode = ERR_GET_DATE_SWITCH_LT10;
			break;
		}
	}

	return (retcode);
}

/*****************************************************************************/
/* TITLE: fnFormatIndiciaDate                                               */
/* AUTHOR: CJD                                                               */
/* INPUT: empty pStrOrValue, datetime pointer                                */
/* DESCRIPTION: Take a pointer to a DATETIME structure and returns a         */
/*              formatted date string for indicias                          */
/* OUTPUT: Returns successful or ERROR                                      */
/* Note: handles date duck, does not handle date separator                  */
/*****************************************************************************/
UINT8 fnFormatIndiciaDate(void * pStrOrValue, const DATETIME * SysDateTimePtr) {
	unsigned short usPrintFlags;
	UINT8 option;
	char cLen = 0, chMonth[4], QuackChar = ' ';
	char QuackDay[3] = "  ", Day[3];
	UINT8 retVal = SUCCESSFUL;

	option = fnFlashGetByteParm(BP_INDICIA_DATE_FORMAT);

#ifdef G900
	switch (fnOITGetPrintMode())
#else
	switch (fnOITGetJanusPrintMode())
#endif
	{
		case PMODE_PERMIT:
			usPrintFlags = usPermitPrintFlags;
			break;

		case PMODE_TIME_STAMP:
		case PMODE_AD_TIME_STAMP:
			usPrintFlags = usDateTimePrintFlags;
			break;

		default:
			usPrintFlags = usIndiciaPrintFlags;
			break;
		}

		if (usPrintFlags & BOB_DUCK_ENTIRE) {
			//if ducking the whole thing,
			// create a string with the proper number of spaces
			switch (option) {
			case DATE_FORMAT_MMMDDYY:
			case DATE_FORMAT_DDMMMYY:
				cLen = 7;
				break;

			case DATE_FORMAT_MMDDYY:
			case DATE_FORMAT_DDMMYY:
			case DATE_FORMAT_YYMMDD:
				cLen = 6;
				break;

			case DATE_FORMAT_DDMMYYYY:
			case DATE_FORMAT_MMDDYYYY:
			case DATE_FORMAT_YYYYMMDD:
				cLen = 8;
				break;

			case DATE_FORMAT_DDMMMYYYY:
			case DATE_FORMAT_MMMDDYYYY:
				cLen = 9;
				break;

			default:
				retVal = (ERR_GET_DATE_SWITCH_LT10 );
				break;
			}

			if (retVal == SUCCESSFUL) {
				memset(pStrOrValue, QuackChar, cLen);
				*((char*) pStrOrValue + cLen) = 0;
			}
		} else {
			if (usPrintFlags & BOB_DUCK_DAY) {
				strcpy(Day, QuackDay);
			} else {
				sprintf(Day, "%02d", SysDateTimePtr->bDay);
			}

			switch (option) {
			case DATE_FORMAT_MMMDDYY:
				GetCurrentMonth(SysDateTimePtr->bMonth, chMonth);
				sprintf(pStrOrValue, "%s%s%02d", chMonth, Day,
						SysDateTimePtr->bYear);
				break;

			case DATE_FORMAT_MMDDYY:
				sprintf(pStrOrValue, "%02d%s%02d", SysDateTimePtr->bMonth, Day,
						SysDateTimePtr->bYear);
				break;

			case DATE_FORMAT_DDMMYY:
				sprintf(pStrOrValue, "%s%02d%02d", Day, SysDateTimePtr->bMonth,
						SysDateTimePtr->bYear);
				break;

			case DATE_FORMAT_YYMMDD:
				sprintf(pStrOrValue, "%02d%02d%s", SysDateTimePtr->bYear,
						SysDateTimePtr->bMonth, Day);
				break;

			case DATE_FORMAT_DDMMMYY:
				GetCurrentMonth(SysDateTimePtr->bMonth, chMonth);
				sprintf(pStrOrValue, "%s%s%02d", Day, chMonth,
						SysDateTimePtr->bYear);
				break;

			case DATE_FORMAT_DDMMMYYYY:
				GetCurrentMonth(SysDateTimePtr->bMonth, chMonth);
				sprintf(pStrOrValue, "%s%s%d%02d", Day, chMonth,
						SysDateTimePtr->bCentury, SysDateTimePtr->bYear);
				break;

			case DATE_FORMAT_MMMDDYYYY:
				GetCurrentMonth(SysDateTimePtr->bMonth, chMonth);
				sprintf(pStrOrValue, "%s%s%d%02d", chMonth, Day,
						SysDateTimePtr->bCentury, SysDateTimePtr->bYear);
				break;

			case DATE_FORMAT_DDMMYYYY:
				sprintf(pStrOrValue, "%s%02d%d%02d", Day,
						SysDateTimePtr->bMonth, SysDateTimePtr->bCentury,
						SysDateTimePtr->bYear);
				break;

			case DATE_FORMAT_MMDDYYYY:
				sprintf(pStrOrValue, "%02d%s%d%02d", SysDateTimePtr->bMonth,
						Day, SysDateTimePtr->bCentury, SysDateTimePtr->bYear);
				break;

			case DATE_FORMAT_YYYYMMDD:
				sprintf(pStrOrValue, "%d%02d%02d%s", SysDateTimePtr->bCentury,
						SysDateTimePtr->bYear, SysDateTimePtr->bMonth, Day);
				break;

			default:
				retVal = (ERR_GET_DATE_SWITCH_LT10 );
				break;
			}
		}

		return (retVal);
	}

	/*****************************************************************************/
	/* TITLE: fnfGetTime                                                        */
	/* AUTHOR: BMP                                                               */
	/* INPUT: empty pStrOrValue, bField                                          */
	/* DESCRIPTION: Gets the report time format and uses that parameter to format*/
	/*              the System Date Time.                                        */
	/* OUTPUT: Returns number of unicode chars in time                          */
	/*****************************************************************************/
	unsigned short fnfGetTime(void * pStrOrValue, unsigned short usVarID) {
		ushort usLen = 0;
		DATETIME DateTime;
		char aAsciiTime[20];

		if (GetSysDateTime(&DateTime, true, true) == SUCCESSFUL) {
			if (fnFormatTime(aAsciiTime, &DateTime, usVarID) == SUCCESSFUL) {
				(void) unistrcpyFromCharStr(pStrOrValue, aAsciiTime);
				usLen = (UINT16) strlen(aAsciiTime);
			}
		}

		return (usLen);
	}

	/*****************************************************************************/
	/* TITLE: fnfFormatTime                                                      */
	/* AUTHOR: WFB                                                               */
	/* INPUT: empty pStrOrValue, datetime pointer                                */
	/* DESCRIPTION: Take a pointer to a DATETIME structure and returns a formatted
	 time string for reports
	 This was split out of fnfGetTime to make it more generic                */
	/* OUTPUT: Returns successful or ERROR                                      */
	/* modified 8/25/04, Victor                     */
	/*****************************************************************************/
	unsigned char fnfFormatTime(void * pStrOrValue,
			const DATETIME * SysDateTimePtr) {
		return (fnFormatTime(pStrOrValue, SysDateTimePtr,
				CURRENT_TIME_WITH_TIME_SEP));
	}

	/*****************************************************************************/
	/* TITLE: fnFormatTime                                                      */
	/* AUTHOR: WFB                                                               */
	/* INPUT: empty pStrOrValue, datetime pointer                                */
	/* DESCRIPTION: Take a pointer to a DATETIME structure and returns a formatted
	 time string for reports
	 This was split out of fnfGetTime to make it more generic                */
	/* OUTPUT: Returns successful or ERROR                                      */
	/*****************************************************************************/
	unsigned char fnFormatTime(void * pStrOrValue,
			const DATETIME * SysDateTimePtr, ushort usVarID) {
		DATETIME LocalDateTime;
		unsigned short APFollower;
		unsigned short TimeSeparation;
		unsigned char TimeFormatOption;

		char chTimeSep;

		char chFollower = 0;

		//Make Local copy of datetime structure to prevent it form being modified
		(void) memcpy(&LocalDateTime, SysDateTimePtr, sizeof(LocalDateTime));

		TimeFormatOption = fnFlashGetByteParm(BP_TIME_FORMAT);
		TimeSeparation = fnFlashGetByteParm(BP_PRINTABLE_TIME_SEP_CHAR);
		retcode = fnUnicode2Ascii(&TimeSeparation, &chTimeSep, 1);

		if ((TimeFormatOption == TIME_FORMAT_12)
				&& (usVarID != INDICIA_TIME_WITHOUT_TIME_SEP )) {
			if ((LocalDateTime.bHour > 12) && (LocalDateTime.bHour <= 24)) {
				LocalDateTime.bHour -= 12;
				APFollower = fnFlashGetWordParm(WP_DISP_PM_FOLLOWER_CHAR);
				retcode = fnUnicode2Ascii(&APFollower, &chFollower, 1);
#ifndef K700
				chFollower = (char) toupper(chFollower);
#endif
			} else if (LocalDateTime.bHour == 12) {
				APFollower = fnFlashGetWordParm(WP_DISP_PM_FOLLOWER_CHAR);
				retcode = fnUnicode2Ascii(&APFollower, &chFollower, 1);
#ifndef K700
				chFollower = (char) toupper(chFollower);
#endif
			} else if (LocalDateTime.bHour < 12) {
				if (LocalDateTime.bHour == 0) {
					LocalDateTime.bHour = 12;
				}
				APFollower = fnFlashGetWordParm(WP_DISP_AM_FOLLOWER_CHAR);
				retcode = fnUnicode2Ascii(&APFollower, &chFollower, 1);
#ifndef K700
				chFollower = (char) toupper(chFollower);
#endif
			}

			(void) sprintf(pStrOrValue, "%d%c%02d%c", LocalDateTime.bHour,
					chTimeSep, LocalDateTime.bMinutes, chFollower);
		} else {
			/* FORMAT_24 */
			switch (usVarID) {
			case INDICIA_TIME_WITHOUT_TIME_SEP :
				(void) sprintf(pStrOrValue, "%02d%02d", LocalDateTime.bHour,
						LocalDateTime.bMinutes);
				break;

			default:
				(void) sprintf(pStrOrValue, "%d%c%02d", LocalDateTime.bHour,
						chTimeSep, LocalDateTime.bMinutes);
				break;
			}
		}
		retcode = SUCCESSFUL;

		return retcode;

	}
#if 0
	/********************************************************************************/
	/* TITLE: fnfGetCurrentMonth                                                    */
	/* AUTHOR: BMP                                                                  */
	/* INPUT:                                                                       */
	/* DESCRIPTION:                                                                 */
	/* OUTPUT:                                                                      */
	/********************************************************************************/
	unsigned char fnGetCurrentMonth(void * pStrOrValue,unsigned short usVarID)
	{
		DATETIME DateTime;
		DATETIME * SysDateTimePtr = &DateTime;

		if (GetSysDateTime(SysDateTimePtr, true, true) == SUCCESSFUL)
		{
			GetCurrentMonth(SysDateTimePtr->bMonth,pStrOrValue);
			retcode = SUCCESSFUL;
		}
		else
		retcode = ERR_GET_MONTH;

		return retcode;

	}
#endif

	/********************************************************************************/
	/* TITLE: GetCurrentMonth                                                       */
	/* AUTHOR: Craig DeFilippo                                                      */
	/* INPUT:                                                                       */
	/* DESCRIPTION:                                                                 */
	/* OUTPUT:                                                                      */
	/********************************************************************************/
	void GetCurrentMonth(UINT8 bMonth, char * chMonth) {
		unichar pUniMonth[LEN_NATIVE_WORD + 1] = { NULL_VAL };
		UINT8 bID = NATIVE_JAN + bMonth - 1; // bMonth starts from 1

		GetNativeWord(pUniMonth, bID);
		fnUnicode2AsciiWithNul(pUniMonth, chMonth, 3);
	}

	/*
	 void GetCurrentMonth(unsigned short bMonth,char * chMonth)
	 {
	 strcpy(chMonth,(char *) GetRptStr(idx_Feature10 + bMonth));
	 }
	 */

	/********************************************************************************/
	/* TITLE: GetNativeWord                                                         */
	/* AUTHOR: Victor Li                                                            */
	/* INPUT:   Unicode destination pointer and native word ID          */
	/* DESCRIPTION: This function is used to retrieve the language dependent word   */
	/*      that is used for report.                    */
	/* OUTPUT:  No return value                                                 */
	/********************************************************************************/
	void GetNativeWord(unichar *pUniDest, unsigned char bID) {
		if (bID < MAX_NATIVE_WORD) {
			(void) unistrcpy(pUniDest, pUniNativeWord[bID]);
		}
	}

//TODO - AB - Re-enable once language support is put in; this used to be called from systask
#if 0
	/********************************************************************************/
	/* TITLE: fnLoadNativeWords                                                         */
	/* AUTHOR:                                                             */
	/* INPUT:   None            */
	/* DESCRIPTION: This function is */
	/* */
	/* OUTPUT:  No return value                                                 */
	/********************************************************************************/
	void fnLoadNativeWords(void)
	{
		short i;
		char *pAsciiLangString = NULL_PTR;
		UINT16 wLen;

		(void)fnInitCurrentLanguageStrings();

		// load pUniNativeWord table with string read from FFS language file
		for (i=0; i<MAX_NATIVE_WORD; i++)
		{
			if ( i <= NATIVE_NO )
			{
				pAsciiLangString = fnGetYesNoString(i);
				if( pAsciiLangString != NULL_PTR )
				{
					wLen = (UINT16)strlen( pAsciiLangString );
					fnAscii2Unicode( pAsciiLangString, pUniNativeWord[i], wLen );
					pUniNativeWord[i][unistrlen(pUniNativeWord[i])] = NULL_VAL; // null terminated
				}
			}
			else // Native word is a month
			{
				// Substract 1 because we're getting the string from a enum: JAN is value 1
				pAsciiLangString = fnGetMonthTextString(i - 1);
				if( pAsciiLangString != NULL_PTR )
				{
					wLen = (UINT16)strlen( pAsciiLangString );
					fnAscii2Unicode( pAsciiLangString, pUniNativeWord[i], wLen );
					pUniNativeWord[i][unistrlen(pUniNativeWord[i])] = NULL_VAL; // null terminated
				}
			}
		}
	}
#endif

	/********************************************************************************/
	/* TITLE: fnfGetPsdDeviceID                                                     */
	/* AUTHOR: Craig DeFilippo                                                      */
	/* INPUT:  Standard report field function                                       */
	/* DESCRIPTION: Unicode version of psd device ID (manuf seq number)             */
	/* OUTPUT: Returns number of unicode characters                                 */
	/********************************************************************************/
	unsigned short fnfGetPsdDeviceID(void * pStrOrValue, unsigned short usVarID) {
		uchar ucTemp = 0;
		char strTemp1[3] = { 0 };
		unichar ustrCRC[3] = { 0 }, ustrSN[IPSD_MFGSNASCII_LEN + 1] = { 0 },
				ustrFC[3] = { 0 };
		ushort usLen = 0;
		BOOL fBob;

		fBob = fnValidData(BOBAMAT0, BOBID_IPSD_MFGSNCRC, &ucTemp);
		if (fBob == BOB_OK) {
			sprintf(strTemp1, "%02x", ucTemp);
			strTemp1[0] = (char) toupper(strTemp1[0]);
			strTemp1[1] = (char) toupper(strTemp1[1]);
			(void) unistrcpyFromCharStr(ustrCRC, strTemp1);
		}

		if (fBob == BOB_OK) {
			fBob = fnValidData(BOBAMAT0, BOBID_IPSD_FAMILYCODE, &ucTemp);
			sprintf(strTemp1, "%02x", ucTemp);
			strTemp1[0] = (char) toupper(strTemp1[0]);
			strTemp1[1] = (char) toupper(strTemp1[1]);
			(void) unistrcpyFromCharStr(ustrFC, strTemp1);
		}

		if (fBob == BOB_OK) {
			fBob = fnValidData(BOBAMAT0, BOBID_IPSD_MFGSNASCII, (void*) ustrSN);
		}

		if (fBob == BOB_OK) {
			(void) unistrcat(pStrOrValue, ustrCRC);
			(void) unistrcat(pStrOrValue, ustrFC);
			(void) unistrcat(pStrOrValue, ustrSN);
			usLen = unistrlen(pStrOrValue);
		} else {
			fnProcessSCMError();
		}

		return (usLen);
	}

	/********************************************************************************/
	/* TITLE: fnfGetPrintHeadSerialNumber                                           */
	/* AUTHOR: Craig DeFilippo                                                      */
	/* INPUT:  Standard report field function                                       */
	/* DESCRIPTION: Unicode version of print head serial number                     */
	/* OUTPUT: Returns number of unicode characters                                 */
	/********************************************************************************/
	unsigned short fnfGetPrintHeadSerialNumber(void * pStrOrValue,
			unsigned short usVarID) {
		char strPHSerialNumber[16] = { 0 };
		ushort usLen = 0;
		long lPhSn;
#ifdef K700
		fnGetPrintHeadSerialNumber((uchar *)strPHSerialNumber);
#else
		lPhSn = fnGetPrintHeadSerialNumber();
		sprintf(strPHSerialNumber, "%08x", lPhSn);
#endif
		(void) unistrcpyFromCharStr(pStrOrValue, strPHSerialNumber);
		usLen = unistrlen(pStrOrValue);

		return (usLen);
	}

	/********************************************************************************/
	/* TITLE: fnfVersionCity                                                        */
	/* AUTHOR: Craig DeFilippo                                                      */
	/* INPUT:  Standard report field function                                       */
	/* DESCRIPTION: Unicode version of print head serial number                     */
	/* OUTPUT: Returns number of unicode characters                                 */
	/********************************************************************************/
#define MAX_VER_STR_SZ 11
// From version.c:
	extern uchar fnGetPrettyUicVersionString(char* dest);
// From hw7720.c:
	extern char * ReturnLowVersion(void);

	unsigned short fnfVersionCity(void * pStrOrValue, unsigned short usVarID) {
		DATETIME tempDateTime;
		char *pTemp;
		const char *pSrc;
		UINT32 dwVersion;
		UINT16 wVerSeg;
		ushort usLen = 0;

		// This was made longer than just MAX_VER_STR_SZ because some cases
		//  write PNLENGTH bytes into it, then truncate with a null-terminator.
		char strVerStr[MAX_VER_STR_SZ + PNLENGTH + 1] = { 0 };

		char cRMEffDates[LENDATE * (MAXCARRIERS * 2)]; // function output argument
		char cRMPartNbrs[PNLENGTH * (MAXCARRIERS * 2)]; // function output argument
		char cRMSvcLevels[VLENGTH * (MAXCARRIERS * 2)]; // function output argument
		uchar cNbrRatesModules;                  // function return value
		char tmpEffDateString[MAX_VER_STR_SZ + 1] = { 0 };
		char tmpPartNum[PNLENGTH] = { 0 };
		char tmpSvcLev[VLENGTH] = { 0 };
		uchar ucModNum = 0;
		UINT8 bVerSeg[2];

		memset(cRMEffDates, 0, sizeof(cRMEffDates));
		memset(cRMPartNbrs, 0, sizeof(cRMPartNbrs));
		memset(cRMSvcLevels, 0, sizeof(cRMSvcLevels));

		switch (usVarID) {
		case VER_STR_PSD_FIRMWARE :
			if (fnValidData(BOBAMAT0, BOBID_IPSD_FIRMWAREVERSUBSTR, strVerStr)
					!= BOB_OK) {
				fnProcessSCMError();
			}
			break;

		case VER_STR_UIC_SOFTWARE :
			(void) fnGetPrettyUicVersionString(strVerStr);
			break;

		case VER_STR_WEIGH_PLATFORM :
			if (fnPlatGetSWVersion(strVerStr) == FALSE) {
				strVerStr[0] = 0;
			}
			break;

		case VER_STR_RATE_MANAGER :
			/*
			 if( fnIsRateManagerAvailable() == TRUE )
			 {
			 pSrc = fnRateGetRMSWVersion();
			 if ( pSrc != NULL_PTR )
			 {
			 strncpy( strVerStr, pSrc, MAX_VER_STR_SZ );
			 strVerStr[MAX_VER_STR_SZ] = 0; //NULL terminate
			 }
			 }
			 */
			break;

		case VER_STR_RATE_DATA :
		case VER_STR_RATE_DATA2 :
			/*
			 #if defined (K700) || defined (G900)
			 if( fnIsRateManagerAvailable() == TRUE )
			 {
			 if ( usVarID == VER_STR_RATE_DATA2 )
			 {
			 if we are looking for the second module, set to 2nd set returned by call
			 else we are interested in the first module (ucModNum = 0)
			 ucModNum = 1;
			 }
			 //cNbrRatesModules = fnRateGetModuleInfo( cRMPartNbrs, cRMSvcLevels, cRMEffDates ) ;
			 (void)fnRateGetModuleInfo( &cNbrRatesModules,cRMPartNbrs, cRMSvcLevels, cRMEffDates );
			 if ( cNbrRatesModules > 0 )
			 {
			 memcpy(tmpPartNum, cRMPartNbrs + (PNLENGTH * ucModNum), PNLENGTH -1 );
			 memcpy(tmpSvcLev, cRMSvcLevels + (VLENGTH * ucModNum), VLENGTH -1 );
			 strncpy(strVerStr, tmpPartNum, PNLENGTH-1);
			 strncat(strVerStr, tmpSvcLev, VLENGTH-1);
			 strVerStr[MAX_VER_STR_SZ] = 0; //NULL terminate
			 }
			 }
			 #else
			 strcpy(strVerStr, "");
			 #endif
			 */
			break;

		case VER_STR_RATE_EFFECTIVE :
		case VER_STR_RATE_EFFECTIVE2 :
			/*            if( fnIsRateManagerAvailable() == TRUE )
			 {
			 if ( usVarID == VER_STR_RATE_EFFECTIVE2 )
			 {
			 if we are looking for the second module, set to 2nd set returned by call
			 else we are interested in the first module (ucModNum = 0)
			 ucModNum = 1;
			 }

			 (void)fnRateGetModuleInfo( &cNbrRatesModules,cRMPartNbrs, cRMSvcLevels, cRMEffDates );
			 if ( cNbrRatesModules > 0 )
			 {
			 memset(tmpEffDateString, 0, sizeof(tmpEffDateString));
			 memcpy(tmpEffDateString, cRMEffDates + (LENDATE * ucModNum), LENDATE -1 );

			 //format of Rates effective (tmpEffDateString) date is always YYYY/MM/DD
			 memcpy( strVerStr, &tmpEffDateString[0], 2 );
			 tempDateTime.bCentury = (unsigned char)atoi(strVerStr );
			 if(tempDateTime.bCentury != 0)
			 {
			 memcpy( strVerStr, &tmpEffDateString[2],  2 );
			 tempDateTime.bYear = (unsigned char)atoi(strVerStr );

			 memcpy( strVerStr, &tmpEffDateString[5], 2 );
			 tempDateTime.bMonth = (unsigned char)atoi(strVerStr );

			 memcpy( strVerStr, &tmpEffDateString[8], 2 );
			 tempDateTime.bDay = (unsigned char)atoi(strVerStr );

			 tempDateTime.bDayOfWeek = 1;
			 tempDateTime.bHour = 0;
			 tempDateTime.bMinutes = 0;
			 tempDateTime.bSeconds = 0;
			 (void)fnFormatDate(strVerStr, &tempDateTime, TRUE);
			 }
			 else
			 {
			 strVerStr[0] = 0;
			 }
			 }
			 }*/
			break;

		case VER_STR_PSOC_SOFTWARE :
#ifndef K700
			pTemp = fnReadPSOCVersion(MCB_PSOC);
#endif
			strcpy(strVerStr, pTemp);
			break;

		case VER_STR_PM_SOFTWARE :
#if defined (K700)
			fnGetPMSoftwareVersion(strVerStr);
#else if defined (G900)
			dwVersion = fnReadPMVersion();
			bVerSeg[0] = (UINT8) ((dwVersion >> 24) & 0x00ff);
			bVerSeg[1] = (UINT8) ((dwVersion >> 16) & 0x00ff);
			wVerSeg = (UINT16) (dwVersion & 0x0000ffff);

			(void) sprintf(strVerStr, "%02X%02X.%04X", bVerSeg[0], bVerSeg[1],
					wVerSeg);

#endif
			break;

		case VER_STR_PRT_SER_NUM :
#if defined (K700) || defined (G900)
			(void) fnGetPrintSerialNumber((UINT8 *) strVerStr);
#endif
			break;

		case VER_STR_LOW_SECT :
#ifdef G900
			pTemp = (char *) ReturnLowVersion();
			if (pTemp != NULL_PTR) {
				strncpy(strVerStr, pTemp, sizeof(strVerStr));
			}
#endif
			break;

		case VER_STR_PROT_SECT :
#ifdef G900
			pTemp = (char *) ReturnProtVersion();
			if (pTemp != NULL_PTR) {
				strncpy(strVerStr, pTemp, sizeof(strVerStr));
			}
#endif
			break;

		case VER_STR_FDR_FW :
#ifdef G900
			pTemp = fnFeederGetMcpVersionString();
			if (pTemp != NULL_PTR) {
				strncpy(strVerStr, pTemp, sizeof(strVerStr));
			}
#endif
			break;

		case VER_STR_FDR_PROFILE :
#ifdef G900
			pTemp = fnFeederGetProfileVersionString();
			if (pTemp != NULL_PTR) {
				//
				// The profile string returned is composed of two parts separated
				// by a space character.  The first part contains various information
				// including the Model Number.  The second part is the actual
				// version.  Since Future Phoenix doesn't have room on its screen
				// to display the whole string, this function skips past the first
				// part and displays the version only.
				//
				while (*pTemp && *pTemp != ' ')
					pTemp++;
				if (*pTemp == ' ')
					pTemp++;

				strncpy(strVerStr, pTemp, sizeof(strVerStr));
			}
#endif
			break;

		default:
			break;
		}

		(void) unistrcpyFromCharStr(pStrOrValue, strVerStr);
		usLen = unistrlen(pStrOrValue);

		return (usLen);
	}

	/********************************************************************************/
	/* TITLE: fnfMeterPCN                                                           */
	/* AUTHOR: Craig DeFilippo                                                      */
	/* INPUT:  Standard report field function                                       */
	/* DESCRIPTION: Unicode version of meter PCN                                    */
	/* OUTPUT: Returns number of unicode characters                                 */
	/********************************************************************************/
	unsigned short fnfMeterPCN(void * pStrOrValue, unsigned short usVarID) {
		ushort usLen = 0;

		(void) unistrcpyFromCharStr(pStrOrValue, CMOSSignature.bUicPcn);
		usLen = unistrlen(pStrOrValue);

		return (usLen);
	}

	/********************************************************************************/
	/* TITLE: fnfMeterSMR                                                           */
	/* AUTHOR: Craig DeFilippo                                                      */
	/* INPUT:  Standard report field function                                       */
	/* DESCRIPTION: Unicode version of meter PCN                                    */
	/* OUTPUT: Returns number of unicode characters                                 */
	/********************************************************************************/
	unsigned short fnfMeterSMR(void * pStrOrValue, unsigned short usVarID) {
		ushort usLen = 0;
#ifdef K700
		switch (usVarID)
		{
			case SERV_RPT_CURR_SMR:
			(void)unistrcpyFromCharStr( pStrOrValue, CMOSPCNParams.CurrSMR );
			break;

			case SERV_RPT_MFG_SMR:
			(void)unistrcpyFromCharStr( pStrOrValue, CMOSSignature.bUicSMR );
			break;

			default:
			break;
		}
#else
		//unistrcpyFromCharStr(pStrOrValue, CMOSPCNParams.CurrSMR);
		(void) unistrcpyFromCharStr(pStrOrValue, CMOSSignature.bUicSMR);
#endif
		usLen = unistrlen(pStrOrValue);

		return (usLen);
	}

//#if 0 

	/********************************************************************************/
	/* TITLE: GetCurrencySymbol                                                     */
	/* AUTHOR: BMP                                                                  */
	/* INPUT: None. bPlacement and pCurrencySym are global                          */
	/* DESCRIPTION: Get bPlacement and pCurrencySym from the flash.This makes it    */
	/* flexible for all countries to use.                                           */
	/* OUTPUT: None.                                                                */
	/********************************************************************************/
	void GetCurrencySymbol(void) {
		uchar *pCurrSym;
		size_t symLen = 0;

		bPlacement = fnFlashGetByteParm(BP_CURRENCY_SYM_PLACEMENT);

		currency_len = 0;
		pCurrSym = fnFlashGetAsciiStringParm(ASP_REPORTS_CURRENCY_SYM);
		if (pCurrSym) {
			symLen = strlen((const char *) pCurrSym);
			if ((symLen > 0) && (symLen < sizeof(pCurrencySym))) {
				/* copy the Currency Symbol only */
				strcpy(pCurrencySym, (const char *) pCurrSym);
				currency_len = symLen;
			}
		}

	}

	/********************************************************************************/
	/* TITLE: fnfGetCurrencyAndMoneyLens                                            */
	/* AUTHOR: BMP                                                                  */
	/* INPUT: holds money amount                                                    */
	/* DESCRIPTION:Determine if length of money amount and Currency length exceeds 14*/
	/* OUTPUT: Return SUCCESSFUL if lengths < = 14, else error                      */
	/********************************************************************************/
	unsigned char fnGetCurrencyAndMoneyLens(const char * pchAsciiArray) {
		if ((currency_len + strlen(pchAsciiArray)) > 14)
			retcode = EXCEEDS_MAX_LENS;
		else
			retcode = SUCCESSFUL;

		return retcode;
	}

	/********************************************************************************/
	/* TITLE: fnfGetLastRefillAmount                                                */
	/* AUTHOR: Craig DeFilippo                                                      */
	/* INPUT: empty pStrOrValue,                                                    */
	/* DESCRIPTION: Retrieve Money Parameter from Modem and convert to char ptr     */
	/* OUTPUT: Return SUCCESSFUL and pStrOrValue or error                           */
	/********************************************************************************/
	unsigned short fnfGetLastRefillAmount(void * pStrOrValue,
			unsigned short usVarID) {
		ushort usLen = 0;

		usLen = fnfRefillStmtVars(pStrOrValue, GET_REFILL_LOG_AMT0);

		return (usLen);

	}

#if 0
	/**********************************************************************************/
	/*                       CMOS/FLASH WRAPPER FUNCTIONS                             */
	/* unsigned char fnCMOSGetByteParm(unsigned short wParmID);                       */
	/* unsigned short fnCMOSGetWordParm(unsigned short wParmID);                      */
	/* unsigned long fnCMOSGetLongParm(unsigned short wParmID);                       */
	/* unsigned char * fnCMOSGetAsciiStringParm(unsigned short wParmID);              */
	/* double * fnCMOSGetDoubleParm(unsigned short wParmID);                          */
	/* unsigned short * fnCMOSGetUnicodeStringParm(unsigned short wParmID);           */
	/* (translate from unsigned short to char string                                  */
	/* unsigned char * fnCMOSGetPackedByteParm(unsigned short   wParmID);             */
	/* (select and ID number and return a string for Packed byte procedure )          */
	/* AS IN THE FLASH, THE CMOS WRAPPERS DO NOT DO ERROR CHECKING. THEY DO NOT CHECK */
	/* IF THE wVarID FALLS WITHIN THE VALID RANGE. THEY DO NOT CHECK IF THERE IS A     */
	/* A DUMMY VARIABLE IN THE TABLE THAT HAS BEEN PASSED IN ACCIDENTALLY.  THE ERROR */
	/* CHECKING IS LEFT TO THE USER.                                                  */
	/*                                                                                */
	/* unsigned char * fnFlashGetMoneyParm(unsigned short wParmID);                   */
	/*                                                                                */
	/* Remember to do bounds checking on wVarID                                        */
	/*                                                                                */
	/* Remember copy the unsigned char * or unsigned short *  of the flash address    */
	/* into a local parameter so that you modify the local parameter, not the flash   */
	/* address.This applies to CMOS addresses as well.  The contents of the CMOS      */
	/* Params should NOT be modified directly.  They should only be copied into local */
	/* variables is modification is necessary                                         */
	/*                                                                                */
	/**********************************************************************************/

	/********************************************************************************/
	/* TITLE: fnfGetCMOSRptByteParam                                                */
	/* AUTHOR: BMP                                                                  */
	/* INPUT: empty pStrOrValue, wVarID is passed as a parameter into the CMOS Table */
	/* DESCRIPTION: Retrieve Byte Parameter from CMOS and convert to char ptr       */
	/* OUTPUT: Return SUCCESSFUL and pStrOrValue or error                           */
	/********************************************************************************/
	unsigned char fnfGetCMOSRptByteParam(void * pStrOrValue, unsigned short wVarID)
	{	unsigned char bByte;

		/* Do Range check on wVarID */
		if ( wVarID < MAX_CMOS_BYTE_PARM)
		{
			/* Copy CMOS byte */
			bByte = fnfGetCMOSByteParam(wVarID);
			sprintf(pStrOrValue,"%u",bByte);
			retcode = SUCCESSFUL;
		}
		else
		retcode = ERR_CMOS_BYTE_VARID_RANGE;

		return retcode;

	}

	/********************************************************************************/
	/* TITLE: fnfGetCMOSRptWordParam                                                */
	/* AUTHOR: BMP                                                                  */
	/* INPUT: empty pStrOrValue, wVarID is passed as a parameter into the CMOS Table */
	/* DESCRIPTION: Retrieve word Parameter from CMOS table and convert to char *   */
	/* OUTPUT: Return SUCCESSFUL and pStrOrValue or error                           */
	/********************************************************************************/
	unsigned char fnfGetCMOSRptWordParam(void * pStrOrValue, unsigned short wVarID)
	{	unsigned short sWord = 0;

		/* Do range check first */
		if ( wVarID < MAX_CMOS_WORD_PARM)
		{
			sWord = fnfGetCMOSWordParam(wVarID);
			sprintf(pStrOrValue, "%u",sWord);
			retcode = SUCCESSFUL;
		}
		else

		retcode = ERR_CMOS_WORD_VARID_RANGE;

		return retcode;

	}
#endif
	/********************************************************************************/
	/* TITLE: fnfGetCMOSRptLongParam                                                */
	/* AUTHOR: BMP                                                                  */
	/* INPUT:  empty pStrOrValue,wVarID is passed as a parameter into the CMOS Table */
	/* DESCRIPTION: Retrieve long Parameter from CMOS table and convert to char *   */
	/* OUTPUT: Return SUCCESSFUL and pStrOrValue or error                           */
	/********************************************************************************/
	unsigned short fnfGetCMOSRptLongParam(void * pStrOrValue,
			unsigned short wVarID) {
		unsigned long flLong = 0;
		unsigned short usLen = 0, *pUniString;

		switch (wVarID) {
		case CMOS_LONG_JAM_COUNT :
			wVarID = CMOS_LP_JAM_CYCLE_COUNT;
			break;

		case CMOS_LONG_SKEW_COUNT :
			wVarID = CMOS_LP_SKEW_CYCLE_COUNT;
			break;

		default:
			break;

		}
		if (wVarID < MAX_CMOS_LONG_PARM) {
			flLong = fnfGetCMOSLongParam(wVarID);
			usLen = fnBin2Unicode(pStrOrValue, flLong);
			pUniString = pStrOrValue;
			*(pUniString + usLen) = 0;  //null terminate the unistring
		}

		return (usLen);
	}
#if 0
	/********************************************************************************/
	/* TITLE: fnfGetCMOSRptDoubleParam                                              */
	/* AUTHOR: BMP                                                                  */
	/* INPUT: empty pStrOrValue, wVarID is passed as a parameter into the CMOS Table */
	/* DESCRIPTION: Retrieve Double Parameter from CMOS table and convert to char * */
	/* OUTPUT: Return SUCCESSFUL and pStrOrValue or error                           */
	/********************************************************************************/
	unsigned char fnfGetCMOSRptDoubleParam(void * pStrOrValue, unsigned short wVarID)
	{
		double dblMoney;
		unsigned char bMinWhole,bMinDec,bLen;
		unsigned short wUnicode[35];
		char chAscii[18];
		char chAsciiArray[16];

		memset(chAscii,0,18);
		memset(wUnicode,0,35);

		if ( wVarID < MAX_CMOS_DB_PARM)
		{
			dblMoney = fnfGetCMOSDoubleParam(wVarID);

			if ((wVarID == CMOS_DB_BATCH_AMT_MAX) || (wVarID == CMOS_DB_BATCH_AMT_TOTAL))
			{

				/* Get min # of integers and decimals from FLASH */
				bMinWhole = fnFlashGetByteParm(BP_MIN_PRINTED_INTEGERS);
				bMinDec = fnFlashGetByteParm(BP_MIN_PRINTED_DECIMALS);

				/* Translate MONEY from DOUBLE format to UNICODE  */
				bLen = fnMoney2Unicode( wUnicode, dblMoney, true,bMinWhole, bMinDec);

				/* Now translate UNICODE to ASCII. */
				(void) fnUnicode2Ascii(wUnicode, chAscii, bLen+1);

				chAscii[bLen+1] = 0;

				/* Copy Currency Symbol to pCurrencySymbol*/
				GetCurrencySymbol();

				/* if there is enough space on the report for CurrencySymbol and Money Amt */
				if ((fnGetCurrencyAndMoneyLens(&chAsciiArray[0]) == SUCCESSFUL) && (bPlacement == BEFORE))
				{
					/*Then copy both */
					strncpy((char *)pStrOrValue,pCurrencySym,strlen(pCurrencySym));
					strcpy((char * ) pStrOrValue+currency_len, chAscii);
				}
				else if ((fnGetCurrencyAndMoneyLens(&chAsciiArray[0]) == SUCCESSFUL) && (bPlacement == AFTER))
				/* bPlacement == AFTER */
				{
					strncpy((char *) pStrOrValue,chAscii,strlen(chAscii));
					strcpy((char *)pStrOrValue+strlen((char *) pStrOrValue),pCurrencySym);
				} /* else just copy the Money Amt- - no room for currency and amount, print just the amount */
				else
				strcpy((char * ) pStrOrValue, chAscii);

				retcode = SUCCESSFUL;

			}
			else /* No currency symbol needed print just the amount*/
			{
				fnTranslateAmount2Ascii(dblMoney,(char *) pStrOrValue);
				retcode = SUCCESSFUL;
			}
		}
		else
		retcode = ERR_CMOS_DBL_VARID_RANGE;

		return retcode;
	}
	/********************************************************************************/
	/* TITLE: fnfGetCMOSRptASCIIParam                                               */
	/* AUTHOR: BMP                                                                  */
	/* INPUT: empty pStrOrValue, wVarID is passed as a parameter into the CMOS Table */
	/* DESCRIPTION: Retrieve address of structure of string Parameter from CMOS     */
	/* and copy it to pStrOrValue.                                                  */
	/* OUTPUT: Return SUCCESSFUL and pStrOrValue or error                           */
	/********************************************************************************/
	unsigned char fnfGetCMOSRptStringParam(void * pStrOrValue, unsigned short wVarID)
	{
		char *pchASCII;

		/* Do range check on wVarID */
		if ( wVarID < MAX_CMOS_STR_PARM)
		{
			pchASCII = fnfGetCMOSStringParam(wVarID);

			/* Do check if dummy variable was returned */
			if( pchASCII == NULL_PTR )
			{
				retcode = ERR_CMOS_ASCII_DUMMY_VAR;
			}
			else
			{
				strcpy((char * ) pStrOrValue,pchASCII);
				retcode = SUCCESSFUL;
			}
		}
		else
		retcode = ERR_CMOS_ASCII_VARID_RANGE;

		return retcode;
	}

	/********************************************************************************/
	/* TITLE: fnfCMOSGetRptUnicodeParam                                             */
	/* AUTHOR: BMP                                                                  */
	/* INPUT: empty pStrOrValue, wVarID is passed as a parameter into the CMOS      */
	/* DESCRIPTION: Retrieve Unicode Parameter from CMOS wrapper and translate to   */
	/* ascii string                                                                 */
	/* OUTPUT: SUCCESSFUL and pStrOrValue                                           */
	/********************************************************************************/
	unsigned char fnfGetCMOSRptUnicodeParam(void * pStrOrValue, unsigned short wVarID)
	{
		unsigned short wUnicodeLen;
		unsigned short * pwParam;

		if ( wVarID < MAX_CMOS_UC_PARM )
		{
			pwParam = fnfGetCMOSUCStringParam(wVarID);

			if( pwParam == NULL_PTR )
			{
				retcode = ERR_CMOS_UC_DUMMY_VAR;
			}
			else
			{
				/* full length = len + null terminator */
				wUnicodeLen = (strlen((char *) pwParam) / (unsigned short) 2) + (unsigned short) 1;
				fnUnicode2AsciiWithNul(pwParam, (char*) pStrOrValue,(unsigned short) wUnicodeLen);
				retcode = SUCCESSFUL;

			}
		}
		else
		retcode = ERR_CMOS_UC_VARID_RANGE;

		return retcode;
	}

#endif

	/********************************************************************************/
	/* TITLE: fnGetLengthOfPB                                                       */
	/* AUTHOR: BMP                                                                  */
	/* INPUT: wVarID is passed as a parameter into the if-then-else statement       */
	/* DESCRIPTION: Based on the length of Setup, Inscription Table, FeatureTable,Ad*/
	/* Slogan Table, PCN SW Version, Error Table, Inspection Tables, Audit Table    */
	/* the correct length of the BCD is assigned. If wVarID is not correct, zero    */
	/* is returned                                                                  */
	/* OUTPUT: wLen                                                                 */
	/********************************************************************************/
	UINT8 fnGetLengthOfPB(UINT16 wVarID) {
		UINT8 wLen;

		if (wVarID == 0) /*Setup.lwPBPAcctNumber */
		{
			wLen = 8;
		} else if (wVarID == 1) /*InscrTable */
		{
			wLen = 4;
		} else if (wVarID == 2) /*FeatureTable */
		{
			wLen = 8;
		} else if (wVarID == 3) /*Ad Slogan */
		{
			wLen = 4;
		} else if (wVarID == 4) /* PCN SW VERSION */
		{
			wLen = 4;
		} else if ((wVarID >= 5) && (wVarID <= 14)) /* Error Table */
		{
			wLen = 12;
		} else if ((wVarID >= 15) && (wVarID <= 18)) /* Inspection Tables */
		{
			wLen = 13;
		} else if ((wVarID >= 19) && (wVarID <= 28)) /* Audit Tables */
		{
			wLen = 42;
		} else {
			wLen = 0;
		}

		return wLen;
	}

	/* *************************************************************************
	 // FUNCTION NAME: fnPB2HexUnicode
	 // DESCRIPTION: Converts a BCD string to Unicode.
	 // AUTHOR: Joe Mozdzer
	 //
	 // INPUTS:  pBCD - pointer to the BCD string.  this should be in packed format,
	 //                  with one digit per nibble, or 2 digits per byte.  the output
	 //                  of the function is undefined if the input string contains
	 //                  any nibbles greater than 9.
	 //          pDest - pointer to the unicode destination.  (must be pre-malloc'ed)
	 //          bLen - length, in digits, of the BCD string to convert.  note
	 //                  that if this is an odd number, the high nibble of the
	 //                  first input byte is ignored.
	 //
	 // *************************************************************************/
	UINT16 fnPB2HexUnicode(const UINT8 *pBCD, UINT16 *pDest, UINT8 bLen) {
		UINT8 bDigit;
		char String;

		while (bLen) {
			/* pull a digit out of the string */
			if (bLen % 2) {
				/* process odd nibble */
				bDigit = *pBCD++;
				bDigit &= 0x0F;
			} else {
				/* process even nibble */
				bDigit = *pBCD;
				bDigit = (bDigit & 0xF0) >> 4;
			}

			sprintf(&String, "%x", bDigit);

			//*pDest++ = (unsigned short) String;
			*pDest++ = String;

			bLen--;
		}

		*pDest = 0; //null terminator

		return (0);
	}

	/********************************************************************************/
	/* TITLE: fnfGetCMOSRptPackedByteParam                                          */
	/* AUTHOR: BMP                                                                  */
	/* INPUT: empty pStrOrValue, wVarID is passed as a parameter into the CMOS Table */
	/* DESCRIPTION: Retrieve Packed Byte Parameter from CMOS wrapper                */
	/* OUTPUT: len of unicode string returned                                       */
	/********************************************************************************/
	unsigned short fnfGetCMOSRptPackedByteParam(void * pStrOrValue,
			unsigned short wVarID)

	{
		unsigned char * pPB;
		ushort usLen = 0;

		/* Do Range check on wVarID */
		if (wVarID < MAX_CMOS_PB_PARM) {
			pPB = fnfGetCMOSPackedByteParam(wVarID);

			/* Do check if pbIndex is valid */
			if (pPB != NULL_PTR) {
				usLen = fnGetLengthOfPB(wVarID);
				(void) fnPB2HexUnicode(pPB, pStrOrValue, usLen & 0xFF);
			}
		}

		return (usLen);
	}

#if 0

	/********************************************************************************/
	/* TITLE: fnfGetCMOSRptMoneyParam                                               */
	/* AUTHOR: BMP                                                                  */
	/* INPUT: empty pStrOrValue, wVarID is passed as a parameter into the CMOS Table */
	/* DESCRIPTION: Retrieve Money Parameter from CMOS wrapper and convert to ASCII */
	/* OUTPUT: SUCCESSFUL and pStrOrValue                                           */
	/********************************************************************************/
	unsigned char fnfGetCMOSRptMoneyParam(void * pStrOrValue, unsigned short wVarID)
	{
		unsigned char * pMoney;
		//unsigned short * pLen;
		char chAsciiArray[16];

		unsigned char bMinWhole,bMinDec,bLen;
		unsigned short wUnicode[35];
		char chAscii[18];

		memset(chAscii,0,18);
		memset(wUnicode,0,35);

		if ( wVarID < MAX_CMOS_MONEY_PARM)
		{
			pMoney = fnfGetCMOSMoneyParam(wVarID);

			/* Get min # of integers and decimals from FLASH */
			bMinWhole = fnFlashGetByteParm(BP_MIN_PRINTED_INTEGERS);
			bMinDec = fnFlashGetByteParm(BP_MIN_PRINTED_DECIMALS);

			/* Translate MONEY from DOUBLE format to UNICODE  */
			bLen = fnMoney2Unicode( wUnicode, *pMoney, true,bMinWhole, bMinDec);

			/* Now translate UNICODE to ASCII. */
			(void) fnUnicode2Ascii(wUnicode, chAscii, bLen+1);

			chAscii[bLen+1] = 0;

			/* Copy Currency Symbol to pCurrencySymbol*/
			GetCurrencySymbol();

			/* if there is enough space on the report for CurrencySymbol and Money Amt */
			if ((fnGetCurrencyAndMoneyLens(&chAsciiArray[0]) == SUCCESSFUL) && (bPlacement == BEFORE))
			{
				strncpy(pStrOrValue,pCurrencySym,strlen(pCurrencySym));
				strcpy((char * )pStrOrValue+currency_len,chAscii);
			}
			else if ((fnGetCurrencyAndMoneyLens(&chAsciiArray[0]) == SUCCESSFUL) && (bPlacement == AFTER))
			{
				strncpy((char *) pStrOrValue, chAscii,strlen(chAscii));
				strcpy((char *) pStrOrValue+(strlen(pStrOrValue)),pCurrencySym);
			}
			/* else just copy the Money Amt */
			else
			strcpy((char * ) pStrOrValue, chAscii);

			retcode = SUCCESSFUL;
		}
		else
		retcode = ERR_CMOS_MONEY_VAR_RANGE;

		return retcode;

	}

	/********************************************************************************/
	/* TITLE: fnfGetFlashByteParam                                                  */
	/* AUTHOR: BMP                                                                  */
	/* INPUT: empty pStrOrValue, wVarID is passed as a parameter into the FLASH      */
	/* DESCRIPTION: Retrieve Byte Parameter from Flash wrapper                      */
	/* OUTPUT: Return SUCCESSFUL and pStrOrValue                                    */
	/********************************************************************************/
	unsigned char fnfGetFlashRptByteParam(void * pStrOrValue, unsigned short wVarID)
	{	unsigned char cParam;

		if ( wVarID < MAX_BYTE_PARM )
		{
			cParam = fnFlashGetByteParm(wVarID);

			sprintf(pStrOrValue,"%u",cParam);

			retcode = SUCCESSFUL;

		}
		else

		retcode = ERR_FLASH_BYTE_VARID_RANGE_ERR;

		return retcode;

	}
	/********************************************************************************/
	/* TITLE: fnfGetFlashWordParam                                                  */
	/* AUTHOR: BMP                                                                  */
	/* INPUT: empty pStrOrValue, wVarID is passed as a parameter into the FLASH      */
	/* DESCRIPTION: Retrieve word Parameter from Flash wrapper                      */
	/* OUTPUT: return SUCCESSFUL and pStrOrValue                                    */
	/********************************************************************************/
	unsigned char fnfGetFlashRptWordParam(void * pStrOrValue, unsigned short wVarID)
	{	unsigned short wParam;

		if ( wVarID < MAX_WORD_PARM )
		{
			wParam = fnFlashGetWordParm(wVarID);

			sprintf(pStrOrValue,"%u",wParam);

			retcode = SUCCESSFUL;

		}

		else

		retcode = ERR_FLASH_WORD_VARID_RANGE_ERR;

		return retcode;
	}
	/********************************************************************************/
	/* TITLE: fnfGetFlashLongParam                                                  */
	/* AUTHOR: BMP                                                                  */
	/* INPUT: empty pStrOrValue, wVarID is passed as a parameter into the FLASH      */
	/* DESCRIPTION: Retrieve Long Parameter from Flash Parameter                    */
	/* OUTPUT: return SUCCESSFUL and pStrOrValue                                    */
	/********************************************************************************/
	unsigned char fnfGetFlashRptLongParam(void * pStrOrValue, unsigned short wVarID)
	{	unsigned long lParam;

		if (wVarID < MAX_LONG_PARM )
		{
			lParam = fnFlashGetLongParm(wVarID);

			sprintf(pStrOrValue, "%ul",lParam);

			retcode = SUCCESSFUL;

		}

		else

		retcode = ERR_FLASH_LONG_VARID_RANGE_ERR;

		return retcode;
	}

	/********************************************************************************/
	/* TITLE: fnfGetFlashMoneyParam                                                 */
	/* AUTHOR: BMP                                                                  */
	/* INPUT: empty pStrOrValue,wVarID is passed as a parameter into the FLASH       */
	/* DESCRIPTION: Retrieve Money Parameter from Flash wrapper                     */
	/* OUTPUT: return SUCCESSFUL and pStrOrValue                                    */
	/********************************************************************************/
	unsigned char fnfGetFlashRptMoneyParam(void * pStrOrValue, unsigned short wVarID)
	{
		unsigned char * pMoney;
		//unsigned short * pLen;
		char chAsciiArray[16];
		//double dblMoney;
		unsigned char bMinWhole,bMinDec,bLen;
		unsigned short wUnicode[35];
		char chAscii[18];

		memset(chAscii,0,18);
		memset(wUnicode,0,35);

		if ( wVarID < MAX_MONETARY_PARM )
		{
			pMoney = fnFlashGetMoneyParm(wVarID);

			if( pMoney != NULL_PTR )
			{
				/* Get min # of integers and decimals from FLASH */
				bMinWhole = fnFlashGetByteParm(BP_MIN_PRINTED_INTEGERS);
				bMinDec = fnFlashGetByteParm(BP_MIN_PRINTED_DECIMALS);

				/* Translate MONEY from DOUBLE format to UNICODE  */
				bLen = fnMoney2Unicode( wUnicode, *pMoney, true,bMinWhole, bMinDec);

				/* Now translate UNICODE to ASCII. */
				(void) fnUnicode2Ascii(wUnicode, chAscii, bLen+1);

				chAscii[bLen+1] = 0;

				/* Copy Currency Symbol to pCurrencySymbol*/
				GetCurrencySymbol();

				/* if there is enough space on the report for CurrencySymbol and Money Amt */
				if ((fnGetCurrencyAndMoneyLens(&chAsciiArray[0]) == SUCCESSFUL) && (bPlacement == BEFORE))
				{
					strncpy(pStrOrValue,pCurrencySym,strlen(pCurrencySym));
					strcpy((char * )pStrOrValue+currency_len,chAscii);
				}
				else if ((fnGetCurrencyAndMoneyLens(&chAsciiArray[0]) == SUCCESSFUL) && (bPlacement == AFTER))
				{
					strncpy((char *) pStrOrValue, chAscii,strlen(chAscii));
					strcpy((char *) pStrOrValue+(strlen(pStrOrValue)),pCurrencySym);
				}
				/* else just copy the Money Amt */
				else
				strcpy((char * ) pStrOrValue, chAscii);

				retcode = SUCCESSFUL;

			}
			else
			retcode = ERR_FLASH_GET_MONEY_PARAM;

		}
		else
		retcode = ERR_FLASH_MONEY_VAR_RANGE_ERR;

		return retcode;

	}

	/********************************************************************************/
	/* TITLE: fnfGetFlashPackedByteParam                                            */
	/* AUTHOR: BMP                                                                  */
	/* INPUT: empty pStrOrValue, wVarID is passed as a parameter into the FLASH      */
	/* DESCRIPTION: Retrieve Unicode Parameter from Flash wrapper                   */
	/* OUTPUT: SUCCESSFUL and pStrOrValue                                           */
	/********************************************************************************/
	/*
	 unsigned char fnfGetFlashRptPackedByteParm(void * pStrOrValue, unsigned short wVarID)
	 {unsigned char * pbIndex;
	 unsigned short pDest[10];
	 unsigned char wLen;
	 char chArray[20];
	 wLen = 10;
	 memset(pDest,0,10);
	 memset(chArray,0,21);


	 if (wVarID < MAX_PACKED_BYTE_PARM )
	 {
	 pbIndex  = fnFlashGetPackedByteParm(wVarID);

	 // Do check if pbIndex is valid
	 if( pbIndex == NULL_PTR )
	 {
	 retcode = ERR_FLASH_PB_DUMMY_VAR;
	 }
	 else
	 {
	 fnBCD2Unicode(pbIndex,pDest,wLen);

	 fnUnicode2AsciiWithNul(pDest,chArray,(strlen(chArray)+1));

	 strcpy((char * ) pStrOrValue,chArray);

	 retcode = SUCCESSFUL;
	 }
	 }

	 else
	 retcode = ERR_FLASH_PB_VARID_RANGE;

	 return retcode;
	 }

	 */

	/*****************************************************************************/
	/* TITLE: fnfGetRedateDate                                                   */
	/* AUTHOR: Sam                                                               */
	/* INPUT: empty pStrOrValue, bField                                          */
	/* DESCRIPTION: Gets the Redate report date format and uses that parameter   */
	/*              to get the System Date Time in a date format                 */
	/* OUTPUT: Returns successful or ERROR from fnfGetRedateDate                 */
	/*****************************************************************************/
	unsigned char fnfGetRedateDate(void * pStrOrValue, unsigned short usVarID) {
		DATETIME DateTime;
		DATETIME * SysDateTimePtr = &DateTime;

		retcode = ERR_GET_DATE;

		if (GetSysDateTime(SysDateTimePtr, true, true) == SUCCESSFUL) {
			retcode = fnFormatRedateDate(pStrOrValue, SysDateTimePtr);
		}

		return retcode;
	}
//*****************************************************************************
// TITLE:       fnfFormatDateClassicUS
// AUTHOR: Sam T
//
// INPUTS:  
//      pStrOrValue - Pointer to destination buffer where string is to be written. 
//                  Must be big enough to hold the date and the null-terminator.
//                  The date may be from 8 - 11 chars long.
//      SysDateTimePtr - Pointer to structure which holds the date/time to be 
//                  printed.  The DATETIME structure is defined in global.h,
//                  And is parsed into easily recognizable parts.
//
// DESCRIPTION: 
//  Formats the date in the date/time structure into a string.
//  The format of the string is "Jan 1,2007" regardless of the values of the
//   EMD parameters BP_REPORT_DATE_FORMAT and BP_PRINTABLE_DATE_SEP_CHAR. 
// 
// OUTPUT:  
//  A formatted string written into the destination buffer.  If the buffer is 
//  too short data will be overwritten.
//
// RETURNS:
//  SUCCESSFUL                      if formatted date was copied to buffer.
//  ERR_GET_DATE_SWITCH (53, 0x35)  if the format is unknown.  Nothing written 
//                                  to buffer.
// MODIFICATONS:
//  2007.08.06 - Clarisa Bellamy 
//    Still does the same thing, just...
//    - Changed the name to be more descriptive, and generic. 
//    - Removed the guts, and just call an existing routine, to save code space.
//-----------------------------------------------------------------------------
	UINT8 fnfFormatDateClassicUS( void * pStrOrValue, DATETIME * SysDateTimePtr )
	{
		// Formats the date as "Jan 1,2007"  regardless of the values of
		//  BP_REPORT_DATE_FORMAT and BP_PRINTABLE_DATE_SEP_CHAR.
		UINT8 ubFormatOption = DATE_FORMAT_MMMDDYYYY | DATE_FORMAT_FLAG_NIP_LEADING_ZEROS;
		char cDateSeparator1 = ' ';// Space
		char cDateSeparator2 = ',';// Comma
		UINT8 retval;

		retval = fnFormatDateAscii( pStrOrValue, SysDateTimePtr, ubFormatOption,
				cDateSeparator1, cDateSeparator2 );
		return( retval );
	}

#endif

	/***************************************************************************************************************************/
	/* TITLE: fnDetermineTotalPages                                                */
	/* AUTHOR: Craig DeFilippo                                                      */
	/* INPUT: Report ID defined in fwrapper.h                                       */
	/* DESCRIPTION: Determine the Total Pages for multipage reports                 */
	/* OUTPUT: return total Pages for passed poert type                             */
	/* MODIFICATION HISTORY:                                                                                                   */
	/*  2012.08.14 Liu Jupeng   Merged the fixing issue from the branch fphx02.08_c1_secap.                                    */
	/*     2011.08.21 Deborah Kohl FPHX - 02.08                                                                                */
	/*     - Changed function fnfGetPreDebitItems() to print the batch count corresponding                                     */
	/*       to the selected permit when we are in permit mode (before we were always printing the indicia                     */
	/*       batch count)                                                                                                      */
	/*     - Changed functions fnDetermineTotalPages(), fnfPermitRptVars(), added fnGetPermitIndex(), PermitVarId2RecordIndex[]*/
	/*       to support printing of permit report on multiple pages                                                            */
	/***************************************************************************************************************************/
	unsigned short fnDetermineTotalPages(unsigned char ucReportID) {
		div_t result;
		unsigned short recCount = 0;
		UINT16 usTotalPages = 1;
		UINT8 ucNumRecsPerPage = 1;
		UINT8 ucEndIndex;
		BOOL fWrapped = FALSE;

		switch (ucReportID) {
		case CONFIG_RPT:
		case DATETIME_STAMP_RPT:
		case LP_DAILY_TOTALS_RPT:
		case SW_VER_RPT:
		case SHORT_SVC_RPT:
		case BALANCE_INQ_RECEIPT_RPT:
		case REFILL_RECEIPT_RPT:
		case REG_RPT:
		case SINGLE_ACCT_RPT:
		case REFUND_RECEIPT_RPT:
			recCount = 1;
			ucNumRecsPerPage = 1;
			break;

		case NETSET2_CUSTOMER_RPT:
//            recCount = fnGetCurrentReportTotalPages();
			ucNumRecsPerPage = 1;
			break;

		case PERMIT_RPT:
			recCount = GetNumberOfPermits();
			ucNumRecsPerPage = ThreeEntriesPerPage;
			break;

		case DELCON_RPT:
//          recCount = fnDelConRecordsUploaded();   // David

//            recCount = fnGetCurrentReportTotalPages();
			ucNumRecsPerPage = 1;
			break;

		case ERR_RPT:
			recCount = fnGetErrorLogEntryCount();
			ucNumRecsPerPage = FiveEntriesPerPage;
			break;

		case LP_COMM_INCIDENTS_RPT:
			// Get whether the log wrapped & the index of the next record to be written
			//TODO JAH remove dcperiod.c fWrapped = fnDCAP_PM_GetTechSuperData_Info( DCAP_COMM_INCID, &ucFrenchStartIndex );
			if (fWrapped) {
				// Log wrapped, use max records for the record count
				recCount = NUMCOMMINCID;

				// if start index > max, reset it back to the first record
				if (ucFrenchStartIndex == NUMCOMMINCID)
					ucFrenchStartIndex = 0;
			} else {
				// Log didn't wrap, use the given index as the record count because the index is pointing
				// to next record to be used, but the index starts at 0.
				// Reset the index of the oldest record to the first record, which is at index 0.
				recCount = ucFrenchStartIndex;
				ucFrenchStartIndex = 0;
			}

			ucNumRecsPerPage = FiveEntriesPerPage;
			usFrenchNumRecs = recCount;
			break;

		case LP_FRANK_SEQS_RPT:
			// Get whether the log wrapped & the index of the next record to be written
			//TODO JAH remove dcperiod.c fWrapped = fnDCAP_PM_GetTechSuperData_Info( DCAP_UPFRANK_SEQ, &ucFrenchStartIndex );
			if (fWrapped) {
				// Log wrapped, use max records for the record count
				recCount = NUMUPFRANKSEQ;

				// if start index > max, reset it back to the first record
				if (ucFrenchStartIndex == NUMUPFRANKSEQ)
					ucFrenchStartIndex = 0;
			} else {
				// Log didn't wrap, use the given index as the record count because the index is pointing
				// to next record to be used, but the index starts at 0.
				// Reset the index of the oldest record to the first record, which is at index 0.
				recCount = ucFrenchStartIndex;
				ucFrenchStartIndex = 0;
			}

			ucNumRecsPerPage = ThreeEntriesPerPage;
			usFrenchNumRecs = recCount;
			break;

		case LP_FRAUD_ATTEMPTS_RPT:
			// Get whether the log wrapped & the index of the next record to be written
			//TODO JAH remove dcperiod.c fWrapped = fnDCAP_PM_GetTechSuperData_Info( DCAP_FRAUD_EVENTS, &ucFrenchStartIndex );
			if (fWrapped) {
				// Log wrapped, use max records for the record count
				recCount = NUMFRAUDEVENTS;

				// if start index > max, reset it back to the first record
				if (ucFrenchStartIndex == NUMFRAUDEVENTS)
					ucFrenchStartIndex = 0;
			} else {
				// Log didn't wrap, use the given index as the record count because the index is pointing
				// to next record to be used, but the index starts at 0.
				// Reset the index of the oldest record to the first record, which is at index 0.
				recCount = ucFrenchStartIndex;
				ucFrenchStartIndex = 0;
			}

			ucNumRecsPerPage = FiveEntriesPerPage;
			usFrenchNumRecs = recCount;
			break;

		case LP_OP_INCIDENTS_RPT:
			// Get whether the log wrapped & the index of the next record to be written
			//TODO JAH remove dcperiod.c fWrapped = fnDCAP_PM_GetTechSuperData_Info( DCAP_FMOP_INCID, &ucFrenchStartIndex );
			if (fWrapped) {
				// Log wrapped, use max records for the record count
				recCount = NUMFMOPINCID;

				// if start index > max, reset it back to the first record
				if (ucFrenchStartIndex == NUMFMOPINCID)
					ucFrenchStartIndex = 0;
			} else {
				// Log didn't wrap, use the given index as the record count because the index is pointing
				// to next record to be used, but the index starts at 0.
				// Reset the index of the oldest record to the first record, which is at index 0.
				recCount = ucFrenchStartIndex;
				ucFrenchStartIndex = 0;
			}

			ucNumRecsPerPage = FiveEntriesPerPage;
			usFrenchNumRecs = recCount;
			break;

		case LP_OTHER_OPS_RPT:
			// Get whether the log wrapped & the index of the next record to be written
			//TODO JAH remove dcperiod.c fWrapped = fnDCAP_PM_GetTechSuperData_Info( DCAP_UPOTHER_OP, &ucFrenchStartIndex );
			if (fWrapped) {
				// Log wrapped, use max records for the record count
				recCount = NUMUPOTHEROP;

				// if start index > max, reset it back to the first record
				if (ucFrenchStartIndex == NUMUPOTHEROP)
					ucFrenchStartIndex = 0;
			} else {
				// Log didn't wrap, use the given index as the record count because the index is pointing
				// to next record to be used, but the index starts at 0.
				// Reset the index of the oldest record to the first record, which is at index 0.
				recCount = ucFrenchStartIndex;
				ucFrenchStartIndex = 0;
			}

			ucNumRecsPerPage = FiveEntriesPerPage;
			usFrenchNumRecs = recCount;
			break;

		case LP_PBP_COMM_RPT:
			// Get whether the log wrapped & the index of the next record to be written
			//TODO JAH remove dcperiod.c fWrapped = fnDCAP_PM_GetTechSuperData_Info( DCAP_FMCOMM_WDC, &ucFrenchStartIndex );
			if (fWrapped) {
				// Log wrapped, use max records for the record count
				recCount = NUMFMCOMMWDC;

				// if start index > max, reset it back to the first record
				if (ucFrenchStartIndex == NUMFMCOMMWDC)
					ucFrenchStartIndex = 0;
			} else {
				// Log didn't wrap, use the given index as the record count because the index is pointing
				// to next record to be used, but the index starts at 0.
				// Reset the index of the oldest record to the first record, which is at index 0.
				recCount = ucFrenchStartIndex;
				ucFrenchStartIndex = 0;
			}

			ucNumRecsPerPage = FiveEntriesPerPage;
			usFrenchNumRecs = recCount;
			break;

		case LP_POSTDATING_RPT:
			recCount = POST_LOG_SZ;
			ucNumRecsPerPage = FiveEntriesPerPage;
			ucFrenchStartIndex = 0;
			usFrenchNumRecs = recCount;
			break;

		case LP_USAGE_SUMMARY_RPT:
			//TODO JAH remove dcperiod.c recCount = fnDCAP_GetAdjustable_Log_Limits(&ucFrenchStartIndex, &ucEndIndex, &fWrapped);
			// compensate for an anomaly that happens if the Dcap rules haven't been downloaded
			if (recCount > ADJUSTABLE_LOG_ENTRIES)
				recCount = 0;
			ucNumRecsPerPage = FiveEntriesPerPage;
			usFrenchNumRecs = recCount;
			break;

		case MULTI_ACCT_SUM_RPT:
//            recCount = fnGetTotalAccountsInUse();
			ucNumRecsPerPage = FiveEntriesPerPage;
			break;

		case BALANCE_INQ_STMT_RPT:
		case REFILL_STMT_RPT:
			recCount = 5;
			ucNumRecsPerPage = FiveEntriesPerPage;
			break;

		case RATES_DOWNLOAD_RPT:
		case RATES_SUMMARY_RPT:
			recCount = sRatesRptData.bNumRecords;
			ucNumRecsPerPage = FiveEntriesPerPage;
			break;

		case ASR_RPT:
			//TODO JAH remove asrapi.c recCount = (unsigned short)fnASRNbCmosASRLogEntryUsed(0);
			ucNumRecsPerPage = FiveEntriesPerPage;
			break;

		case DATA_CAPTURE_RPT:
		case SETUP_RPT:
		default:
			break;
		}

		result = div(recCount, ucNumRecsPerPage);
		if (result.rem > 0)
			usTotalPages = result.quot + 1;
		else if (result.quot == 0)
			usTotalPages = 1;
		else
			usTotalPages = result.quot;

		return (usTotalPages);

	}

	unsigned short fnfGetTotalReportPages(void * pStrOrValue,
			unsigned short usVarID) {
		char strPages[3] = { 0 };
		ushort usLen = 0;

		sprintf(strPages, "%d", wTotalPages);
		(void) unistrcpyFromCharStr(pStrOrValue, strPages);
		usLen = unistrlen(pStrOrValue);

		return (usLen);
	}
	/********************************************************************************/
	/* TITLE: fnSetCurrentReportPageNbr                                             */
	/* AUTHOR: Craig DeFilippo                                                      */
	/* INPUT:                                                                       */
	/* DESCRIPTION: Set the Current Page Nbr for a multipage report                 */
	/* OUTPUT: returns void                                                         */
	/********************************************************************************/
	void fnSetCurrentReportPageNbr(unsigned short wCurrentPgNbr) {
		wCurrentPageNumber = wCurrentPgNbr;
		return;
	}

	/********************************************************************************/
	/* TITLE: fnfGetCurrentReportPageNbr                                            */
	/* AUTHOR: Craig DeFilippo                                                      */
	/* INPUT:                                                                       */
	/* DESCRIPTION: Get the Current Page Number for MultiAccount Report             */
	/* OUTPUT: number of unicode chars                                              */
	/* pStrOrValue will hold unichar string of wCurrentPageNumber                   */
	/********************************************************************************/
	unsigned short fnfGetCurrentReportPageNbr(void * pStrOrValue,
			unsigned short usVarID) {
		char strPage[3] = { 0 };
		ushort usLen = 0;

		sprintf(strPage, "%d", wCurrentPageNumber);
		(void) unistrcpyFromCharStr(pStrOrValue, strPage);
		usLen = unistrlen(pStrOrValue);

		return (usLen);
	}

	/********************************************************************************/
	/* TITLE: fnSetTotalReportPages                                                 */
	/* AUTHOR: Craig DeFilippo                                                      */
	/* INPUT:                                                                       */
	/* DESCRIPTION: Set the total pages count                                       */
	/* OUTPUT: returns void                                                         */
	/********************************************************************************/
	void fnSetTotalReportPages(unsigned short usTotalPages) {
		wTotalPages = usTotalPages;
		return;
	}

	/********************************************************************************/
	/* TITLE: fnfAccountingStmtVars                                                 */
	/* AUTHOR:  Craig DeFilippo                                                     */
	/*                                                                              */
	/* INPUT: empty pStrOrValue, and Variable ID                                    */
	/* DESCRIPTION:                                                                 */
	/*    Retrieve variable ID's data and translate it to char string               */
	/*    Variable ID's Supported are:                                              */
	/*                                GET_ACCT_ID0 - 4                              */
	/*                                GET_ACCT_PIECES0 - 4                          */
	/*                                GET_ACCT_AMOUNT0 - 4                          */
	/*                                GET_ACCT_NAME0 - 4                            */
	/*                                                                              */
	/* OUTPUT: number of unicode characters written to pStrOrValue                   */
	/********************************************************************************/

	unsigned short fnfAccountingStmtVars(void * pStrOrValue,
			unsigned short usVarID) {
		ushort usLen = 0, listIndex;
		uchar pageIndex;

		// determine what slot we are filling on the current page
		pageIndex = fnGetAccountVarIndex(usVarID);
		// determine the corresponding list index for the page we are on
		listIndex = (unsigned short) (((wCurrentPageNumber - 1)
				* FiveEntriesPerPage) + pageIndex);
		// set the global account ID accordingly
		usRptActID = aRptActList[listIndex];

		if ((usRptActID > 0) && (usRptActID <= ACT_MAX_NUM_ACCOUNT)) {
			switch (usVarID) {
			case GET_ACCT_ID0 :
			case GET_ACCT_ID1 :
			case GET_ACCT_ID2 :
			case GET_ACCT_ID3 :
			case GET_ACCT_ID4 :
				usLen = fnfAccountRptID(pStrOrValue, usVarID);
				break;

			case GET_ACCT_PIECES0 :
			case GET_ACCT_PIECES1 :
			case GET_ACCT_PIECES2 :
			case GET_ACCT_PIECES3 :
			case GET_ACCT_PIECES4 :
				usLen = fnfAccountRptPieces(pStrOrValue, usVarID);
				break;

			case GET_ACCT_AMOUNT0 :
			case GET_ACCT_AMOUNT1 :
			case GET_ACCT_AMOUNT2 :
			case GET_ACCT_AMOUNT3 :
			case GET_ACCT_AMOUNT4 :
				usLen = fnfAccountRptFunds(pStrOrValue, usVarID);
				break;

			case GET_ACCT_NAME0 :
			case GET_ACCT_NAME1 :
			case GET_ACCT_NAME2 :
			case GET_ACCT_NAME3 :
			case GET_ACCT_NAME4 :
				usLen = fnfAccountName(pStrOrValue, usVarID);

				break;

			default:
				(void) memset(pStrOrValue, 0, sizeof(ushort)); //null
				break;
			}
		} else {
			(void) memset(pStrOrValue, 0, sizeof(ushort));   //null
		}

		return (usLen);
	}

	/********************************************************************************/
	/* TITLE:   fnGetAccountVarIndex                                                */
	/* AUTHOR:  Craig DeFilippo                                                     */
	/* INPUT:   variable ID                                                         */
	/* DESCRIPTION:  Scan the variable id to record index table to find the record  */
	/*               index ( 0...) for this Account variable                        */
	/* Output: index of error log entry                                             */
	/********************************************************************************/
	unsigned char fnGetAccountVarIndex(ushort usVarID) {
		ushort i = 0;

		while ((rAccountVarId2RecordIndex[i][0] > 0)
				&& (rAccountVarId2RecordIndex[i][0] != usVarID)) {
			i++;
		}

		return (rAccountVarId2RecordIndex[i][1]);
	}

	/********************************************************************************/
// TITLE:   fnPreMultiAccountreport
// AUTHOR:  Craig DeFilippo                                                     
// INPUT:   none                                                                
// DESCRIPTION:  Initialize globals that will be referenced in 
//               fnfAccountingStmtVars
// Output: none
	/********************************************************************************/
	void fnPreMultiAccountReport(void) {
		memset(aRptActList, 0, sizeof(aRptActList));
		usRptNumActs = 0;

//    (void)fnGetAccountIDList( ACT_QT_BYID, &usRptNumActs, aRptActList, ACT_MAX_NUM_ACCOUNT+1 );
	}

	/********************************************************************************/
// TITLE:   fnRptSingleAccountID
// AUTHOR:  Craig DeFilippo                                                     
// INPUT:   none                                                                
// DESCRIPTION:  Set the account ID for the single account report
// Output: none
	/********************************************************************************/
	void fnRptSingleAccountID(ushort usAccountID) {
		usRptActID = usAccountID;
	}

	/******************************************************************************
	 FUNCTION NAME: fnfAccountName
	 AUTHOR       : Craig DeFilippo
	 DESCRIPTION  : Format main account name, use global usRptActID to identify which one
	 PARAMETERS   : Standard report field function
	 RETURN       : number of unicode chars
	 NOTES        : none
	 ******************************************************************************/
	unsigned short fnfAccountName(void * pStrOrValue, unsigned short usVarID) {
		sMainActName sAccountName;
		ushort usStatus;

		memset(sAccountName, 0, sizeof(sAccountName));
		/*
		 usStatus = fnGetAccountName(ACT_QT_BYT1,usRptActID,sAccountName);
		 if(usStatus == ACT_SUCCESS)
		 {
		 (void)unistrcpy( pStrOrValue, sAccountName );
		 }
		 */

		return (unistrlen(sAccountName));
	}

	/******************************************************************************
	 FUNCTION NAME: fnfAccountSubName
	 AUTHOR       : Craig DeFilippo
	 DESCRIPTION  : Format sub account name, use global usRptActID to identify which one
	 PARAMETERS   : Standard report field function
	 RETURN       : number of unicode chars
	 NOTES        : none
	 ******************************************************************************/
	unsigned short fnfAccountSubName(void * pStrOrValue, unsigned short usVarID) {
		sSubActName sAccountName;
		ushort usStatus;

		memset(sAccountName, 0, sizeof(sAccountName));
		/*
		 usStatus = fnGetAccountName(ACT_QT_BYT2,usRptActID,sAccountName);
		 if(usStatus == ACT_SUCCESS)
		 {
		 (void)unistrcpy( pStrOrValue, sAccountName );
		 }
		 */

		return (unistrlen(sAccountName));
	}

	/******************************************************************************
	 FUNCTION NAME: fnfAccountSubSubName
	 AUTHOR       : Craig DeFilippo
	 DESCRIPTION  : Format sub sub account name, use global usRptActID to identify which one
	 PARAMETERS   : Standard report field function
	 RETURN       : number of unicode chars
	 NOTES        : none
	 ******************************************************************************/
	unsigned short fnfAccountSubSubName(void * pStrOrValue,
			unsigned short usVarID) {
		sSSubActName sAccountName;
		ushort usStatus;

		memset(sAccountName, 0, sizeof(sAccountName));
		/*
		 usStatus = fnGetAccountName(ACT_QT_BYT3,usRptActID,sAccountName);
		 if(usStatus == ACT_SUCCESS)
		 {
		 (void)unistrcpy( pStrOrValue, sAccountName );
		 }
		 */

		return (unistrlen(sAccountName));
	}

	/******************************************************************************
	 FUNCTION NAME: fnfAccountRptPieces
	 AUTHOR       : Craig DeFilippo
	 DESCRIPTION  : Format account pieces, use global usRptActID to identify which one
	 PARAMETERS   : Standard report field function
	 RETURN       : number of unicode chars
	 NOTES        : none
	 ******************************************************************************/
	unsigned short fnfAccountRptPieces(void * pStrOrValue,
			unsigned short usVarID) {
		char sPieces[ACCOUNT_REPORT_PIECE_SZ + 1];
		long lPieces = 0;
		double dFunds;
		ushort usStatus;

		memset(sPieces, 0, sizeof(sPieces));

		/*
		 usStatus = fnGetAccountTotals( ACT_QT_BYID, usRptActID, NULL_PTR, &dFunds, &lPieces );
		 if(usStatus == ACT_SUCCESS)
		 {
		 sprintf(sPieces,"%d",lPieces);
		 (void)unistrcpyFromCharStr( pStrOrValue, sPieces );
		 }
		 */

		return ((UINT16) strlen(sPieces));
	}

	/******************************************************************************
	 FUNCTION NAME: fnfAccountRptFunds
	 AUTHOR       : Craig DeFilippo
	 DESCRIPTION  : Format account funds, use global usRptActID to identify which one
	 PARAMETERS   : Standard report field function
	 RETURN       : number of unicode chars
	 NOTES        : none
	 ******************************************************************************/
	unsigned short fnfAccountRptFunds(void * pStrOrValue,
			unsigned short usVarID) {
		long lPieces;
		double dFunds = 0.0;
		ushort usStatus, usLen = 0;

		/*
		 usStatus = fnGetAccountTotals( ACT_QT_BYID, usRptActID, NULL_PTR, &dFunds, &lPieces );
		 if(usStatus == ACT_SUCCESS)
		 {
		 usLen = fnFormatRptDoubleMoney(pStrOrValue, dFunds, REPORT_FIELDS, TRUE);
		 }
		 */

		return (usLen);
	}

	/******************************************************************************
	 FUNCTION NAME: fnfAccountRptID
	 AUTHOR       : Craig DeFilippo
	 DESCRIPTION  : Format account ID, use global usRptActID to identify which one
	 PARAMETERS   : Standard report field function
	 RETURN       : number of unicode chars
	 NOTES        : none
	 ******************************************************************************/
	unsigned short fnfAccountRptID(void * pStrOrValue, unsigned short usVarID) {
		char sID[ACCOUNT_REPORT_ID_SZ + 1];

		memset(sID, 0, sizeof(sID));
		sprintf(sID, "%d", usRptActID);
		(void) unistrcpyFromCharStr(pStrOrValue, sID);

		return ((UINT16) strlen(sID));
	}

	/********************************************************************************/
	/* TITLE: fnfErrorLogStmtVars                                                   */
	/* AUTHOR:  Craig DeFilippo                                                     */
	/*                                                                              */
	/* INPUT: empty pStrOrValue, and Variable ID whose pieces must be retrieved     */
	/* DESCRIPTION:
	 Retrieve variable ID's data and translate it to char string
	 Variable ID's Supported are:
	 GET_ERROR_LOG_DOS0 - 4
	 GET_ERROR_LOG_TOS0 - 4
	 GET_ERROR_LOG_ID0 - 4
	 GET_ERROR_LOG_COUNT0 - 4
	 */
	/* OUTPUT: number of unicode characters written to pStrOrValue                   */
	/********************************************************************************/
	unsigned short fnfErrorLogStmtVars(void * pStrOrValue,
			unsigned short usVarID) {
		unsigned short recordIndex, pageIndex, sortIndex;
		ErrorLogEntry *pErrorEntry = NULL_PTR;
		char tStr[20] = { 0, 0, 0, 0, 0, 0, 0, 0 };
		ushort usLen = 0;

		pageIndex = fnGetErrorPageIndex(usVarID);
		sortIndex = ((wCurrentPageNumber - 1) * FiveEntriesPerPage) + pageIndex;
		recordIndex = fnGetErrorLogIndex(sortIndex);

		if (recordIndex < ERR_LOG_SZ) {
			pErrorEntry =
					(ErrorLogEntry*) &CMOSErrorLog.ErrorTable[recordIndex];

			// if date is 0 then the log entry is empty
			if (memcmp(&pErrorEntry->rDateTime, tStr,
					sizeof(pErrorEntry->rDateTime)) != 0) {
				switch (usVarID) {
				case GET_ERROR_LOG_DOS0 :
				case GET_ERROR_LOG_DOS1 :
				case GET_ERROR_LOG_DOS2 :
				case GET_ERROR_LOG_DOS3 :
				case GET_ERROR_LOG_DOS4 :
					if (fnFormatDate(tStr, &pErrorEntry->rDateTime,
							TRUE) == SUCCESSFUL)
						usLen = strlen(tStr);
					break;

				case GET_ERROR_LOG_TOS0 :
				case GET_ERROR_LOG_TOS1 :
				case GET_ERROR_LOG_TOS2 :
				case GET_ERROR_LOG_TOS3 :
				case GET_ERROR_LOG_TOS4 :
					if (fnFormatTime(tStr, &pErrorEntry->rDateTime,
							usVarID) == SUCCESSFUL)
						usLen = strlen(tStr);
					break;

				case GET_ERROR_LOG_ID0 :
				case GET_ERROR_LOG_ID1 :
				case GET_ERROR_LOG_ID2 :
				case GET_ERROR_LOG_ID3 :
				case GET_ERROR_LOG_ID4 :
					sprintf(tStr, "%02x%02x", pErrorEntry->bErrorClass,
							pErrorEntry->bErrorCode);
					usLen = strlen(tStr);
					break;

				case GET_ERROR_LOG_COUNT0 :
				case GET_ERROR_LOG_COUNT1 :
				case GET_ERROR_LOG_COUNT2 :
				case GET_ERROR_LOG_COUNT3 :
				case GET_ERROR_LOG_COUNT4 :
					sprintf(tStr, "%03d", pErrorEntry->bRepeatCount);
					usLen = strlen(tStr);
					break;

				default:
					break;
				}

				if (usLen) {
					(void) unistrcpyFromCharStr(pStrOrValue, tStr);
				}
			}
		}

		return (usLen);
	}

	/********************************************************************************/
	/* TITLE: fnGetErrorLogEntryCount()                                            */
	/* AUTHOR: Craig DeFilippo                                                      */
	/* INPUT: none              .                                                   */
	/* DESCRIPTION: Find the number of error log entries.                           */
	/* OUTPUT: returns Number of error log entries                                  */
	/********************************************************************************/
	unsigned char fnGetErrorLogEntryCount(void) {
		uchar count = 0, i = 0;

		while (CMOSErrorLog.ErrorTable[i++].rDateTime.bDay > 0)
			count++;

		return (count);
	}

	/********************************************************************************/
	/* TITLE:   fnGetMostRecentErrorLogIndex                                        */
	/* AUTHOR:  Craig DeFilippo                                                     */
	/* INPUT:   none                                                                */
	/* DESCRIPTION:  Scan log and return most recent log index                      */
	/* Output: index of most recent refill log entry                                */
	/********************************************************************************/
	unsigned char fnGetMostRecentErrorLogIndex(void) {
		uchar ucMRError = 0;

		if (CMOSErrorLog.LogIndex) {
			ucMRError = CMOSErrorLog.LogIndex - 1;
		} else {
			if (fnGetErrorLogEntryCount())
				ucMRError = ERR_LOG_SZ - 1;
		}
		return (ucMRError);
	}

	/********************************************************************************/
	/* TITLE:   fnGetNextMostRecentErrorLogIndex                                   */
	/* AUTHOR:  Craig DeFilippo                                                     */
	/* INPUT:   index value                                                         */
	/* DESCRIPTION:  Scan log and return the most recent log index after the date   */
	/*               of the passed reference index. If the passed reference index is*/
	/*               out of range return the most recent log index.                 */
	/* Output: index of next most recent refill log entry                           */
	/* Note : Assumes the log is created as a circular list.  I.E. new entries      */
	/*        appended to the end, and oldest entry overwritten when wrapping       */
	/********************************************************************************/
	unsigned char fnGetNextMostRecentErrorLogIndex(uchar ucRefIdx) {
		uchar ucNMREIndex;

		//default
		ucNMREIndex = fnGetMostRecentErrorLogIndex();

		if (ucRefIdx < ERR_LOG_SZ) {
			if (ucRefIdx > 0) {
				ucNMREIndex = ucRefIdx - 1;
			} else {
				ucNMREIndex = ERR_LOG_SZ - 1;
			}
		}

		return (ucNMREIndex);
	}

	/********************************************************************************/
	/* TITLE:   fnGetErrorPageIndex                                                 */
	/* AUTHOR:  Craig DeFilippo                                                     */
	/* INPUT:   index value                                                         */
	/* DESCRIPTION:  Scan the variable id to record index table to find the record  */
	/*               index ( 0...) for this error variable                          */
	/* Output: index of error log entry                                             */
	/********************************************************************************/
	unsigned char fnGetErrorPageIndex(ushort usVarID) {
		ushort i = 0;

		while ((rErrorVarId2RecordIndex[i][0] > 0)
				&& (rErrorVarId2RecordIndex[i][0] != usVarID)) {
			i++;
		}

		return ((UINT8) rErrorVarId2RecordIndex[i][1]);
	}

	/********************************************************************************/
	/* TITLE:   fnGetErrorLogIndex                                                  */
	/* AUTHOR:  Craig DeFilippo                                                     */
	/* INPUT:   index value                                                         */
	/* DESCRIPTION:  Scan log and return the index of the entry corresponding to    */
	/*               the passed ordinal reference.  e.g. 0 = most recent,           */
	/*               1 = second most recent, etc.                                   */
	/* Output: index of refill log entry                                            */
	/********************************************************************************/
	unsigned char fnGetErrorLogIndex(uchar ucWhich) {
		uchar ucIndex = 0, ucCount;

		ucIndex = fnGetMostRecentErrorLogIndex();

		if (ucWhich < ERR_LOG_SZ) {
			for (ucCount = 0; ucCount < ucWhich; ucCount++) {
				ucIndex = fnGetNextMostRecentErrorLogIndex(ucIndex);
			}
		}

		return (ucIndex);
	}

	/* *************************************************************************
	 // FUNCTION NAME: Latin1ToUpper
	 //
	 // DESCRIPTION:
	 //              Utility function to convert Latin1 chars to Upper case.
	 //
	 // INPUTS:
	 //
	 // RETURNS:
	 //
	 // NOTES:
	 //       0xE0~0xFF was encoded as -32~-1 in the system
	 //
	 // WARNINGS/NOTES:
	 //          None
	 //
	 // REVISION HISTORY:
	 //   Oct 10,2012  Fox Zhang  - Initial Revision.
	 //   11-Dec-12    S. Peterson - made it not so cryptic
	 // *************************************************************************/
	char Latin1ToUpper(register char uChar) {
		if (((unsigned char) uChar >= 0xE0)
				&& ((unsigned char) uChar <= 0xFF)) {
			// change from lower to upper by subtracting 0x20,
			// works for most of the accented characters.
			return ((char) ((unsigned char) uChar - 0x20));
		}

		return uChar;
	}

	/************************************************************************************************************/
	/*********************************BOB UITLS STUFF ************************************************************/
	/*************************************************************************************************************/
// *see versions of this file < 1.6 to see how this function used to compensate for different accounting methods
// this version disregards the accounting method...among other things, defilcj
// returns the length of the unicode string
	unsigned short BOBMainDriver(void * pStrOrValue, unsigned short usVarID) {
		unichar uniString[MAX_RESULT_STRING + 1] = { 0 };
		ushort retVal, usLen = 0;

		memset(uniString, 0, sizeof(uniString));

		/* if variable requires a currency symbol */
		if ((BobsReportVariableLookup[usVarID].wVarID == GET_VLT_DESCENDING_REG)
				|| (BobsReportVariableLookup[usVarID].wVarID
						== GET_VLT_CONTROL_SUM)
				|| (BobsReportVariableLookup[usVarID].wVarID
						== GET_VLT_ASCENDING_REG)) {
			/* Copy Currency Symbol, sets bPlacement and pCurrencySym */
			GetCurrencySymbol();

			if (bPlacement == BEFORE) {
				usLen = strlen(pCurrencySym);
				(void) unistrcpyFromCharStr(uniString, pCurrencySym);
			}

			if (fnGetUnicodeReportData(BOBAMAT0,
					BobsReportVariableLookup[usVarID].wVarID,
					(unichar*) uniString + usLen) == BOB_OK) {
				retVal = SUCCESSFUL;
				usLen = unistrlen(uniString);
			} else {
				retVal = ERR_FN_VALID_DATA;
				usLen = 0;
			}

			if ((retVal == SUCCESSFUL) && (bPlacement == AFTER)) {
				(void) unistrcpyFromCharStr((unichar*) uniString + usLen,
						pCurrencySym);
				usLen = unistrlen(uniString);
			}
		} else {
			/* else just get the data */
			if (fnGetUnicodeReportData(BOBAMAT0,
					BobsReportVariableLookup[usVarID].wVarID,
					(unichar *) uniString) == BOB_OK) {
				retVal = SUCCESSFUL;
				usLen = unistrlen(uniString);
			} else {
				retVal = ERR_FN_VALID_DATA;
			}
		}

		if (retVal == ERR_FN_VALID_DATA) {
			fnProcessSCMError();
		} else {
			(void) unistrcpy(pStrOrValue, uniString);
		}

		return (usLen);
	}

	/********************************************************************************/
	/* TITLE: fnGetDelConRecordCount()                                             */
	/* AUTHOR: WFB  ,  David changed from MEGA V10.0, Feb.20, 04                   */
	/* INPUT: none              .                                                   */
	/* DESCRIPTION: Find the number of ACCEPTED delivery confirmation entries ready
	 for print                                                                   */
	/* OUTPUT: Modifies global variable NumAccts to total number of entries         */
	/* Uses Globals:  NumAccts = number of valid records,                           */
	/********************************************************************************/
#if 0
	/* Obsolete since GTS and Delcon not supported.
	 unsigned short  fnGetDelConRecordCount(void)
	 {

	 unsigned char ucNumRec = 0;

	 return(ucNumRec);
	 }
	 */
#endif

	/******************************************************************************
	 FUNCTION NAME: fnfOCRVerificationCode
	 AUTHOR       : Craig DeFilippo
	 DESCRIPTION  : Calculate and format the OCR Verification Code used by LaPoste
	 PARAMETERS   : Standard report field function
	 RETURN       : number of unicode chars
	 NOTES        : MAC size can be 4 or 6,
	 : OCR Verification Code size can be 3 or 4
	 ******************************************************************************/
	unsigned short fnfOCRVerificationCode(void * pStrOrValue,
			unsigned short usVarID) {
		ushort usSize = 0, i;
		uchar aInput[44], *pInput, *pBarCodeData;
		ulong ulRem = 0, ulMod;
		char sOCRVerCode[5] = { ' ', ' ', ' ', ' ' };

		memset(aInput, 0, sizeof(aInput));

		pInput = aInput;
		pBarCodeData = pIPSD_barcodeData.pGlob;
		memcpy(pInput, pBarCodeData + LAPOST_BC_ISN_OFF, LAPOST_BC_ISN_SIZ);
		pInput += LAPOST_BC_ISN_SIZ;
		memcpy(pInput, pBarCodeData + LAPOST_BC_RCD_OFF, LAPOST_BC_RCD_SIZ);
		pInput += LAPOST_BC_RCD_SIZ;
		memcpy(pInput, pBarCodeData + LAPOST_BC_DOM_OFF, LAPOST_BC_DOM_SIZ);
		pInput += LAPOST_BC_DOM_SIZ;
		memcpy(pInput, pBarCodeData + LAPOST_BC_PST_OFF, LAPOST_BC_PST_SIZ);
		pInput += LAPOST_BC_PST_SIZ;
		memcpy(pInput, pBarCodeData + LAPOST_BC_ITC_OFF, LAPOST_BC_ITC_SIZ);
		pInput += LAPOST_BC_ITC_SIZ;
		memcpy(pInput, pBarCodeData + LAPOST_BC_PCT_OFF, LAPOST_BC_PCT_SIZ);
		pInput += LAPOST_BC_PCT_SIZ;

		// append 4 or 6 MAC characters according to debit cert's MAC size
		memcpy(pInput, pBarCodeData + LAPOST_BC_MAC_OFF,
				*(pBarCodeData + LAPOST_BC_MSZ_OFF));
		pInput += *(pBarCodeData + LAPOST_BC_MSZ_OFF);

		// append 3 or 4 zero's depending on bIPSD_frenchOCR_Sz from PSD Params
		memset(pInput, 0, bIPSD_frenchOCR_Sz);
		pInput += *(pBarCodeData + LAPOST_BC_MSZ_OFF);

		usSize = pInput - aInput - 1;

		if (bIPSD_frenchOCR_Sz == 3)
			ulMod = 991;
		else
			ulMod = 9967;

		// convert the buffer to 0-9 range
		// usSize should be 42 if MAC size is 4, or 44 if MAC size is 3
		for (i = 0; i < usSize; i++) {
			if (aInput[i] > 9) {
				if ((aInput[i] >= '0') && (aInput[i] <= '9'))
					aInput[i] = aInput[i] - '0';

				else if ((aInput[i] >= 'A') && (aInput[i] <= 'Z'))
					aInput[i] = (aInput[i] - 'A') % 10;

				else if ((aInput[i] >= 'a') && (aInput[i] <= 'z'))
					aInput[i] = (aInput[i] - 'a') % 10;

				else if (aInput[i] == ' ')
					aInput[i] = 0;
			}
		}

		// calculate the actual verification code
		for (i = 0; i < usSize; i++) {
			ulRem = ((ulRem * 10) + aInput[i]) % ulMod;
		}

		ulMod -= ulRem;
		for (i = bIPSD_frenchOCR_Sz; i > 0; i--) {
			sOCRVerCode[i - 1] = (ulMod % 10) + '0';
			ulMod /= 10;
		}
		sOCRVerCode[bIPSD_frenchOCR_Sz] = 0;

		(void) unistrcpyFromCharStr(pStrOrValue, sOCRVerCode);

		return (bIPSD_frenchOCR_Sz);
	}

	/******************************************************************************
	 FUNCTION NAME: fnfTextEntryVars
	 AUTHOR       : Craig DeFilippo
	 DESCRIPTION  : Get the text entry variables indicated by BOB's current setting
	 PARAMETERS   : Standard report field function
	 RETURN       : number of unicode chars
	 NOTES        : none
	 ******************************************************************************/
	unsigned short fnfTextEntryVars(void * pStrOrValue, unsigned short usVarID) {
		ushort usLen = 0, usIndex;
		uchar ucLine = 0;
		const unichar *ptrtextEntryLineOfText = NULL_PTR;
		UINT8 ubWritingDir;

		if (fnValidData(BOBAMAT0, READ_CURRENT_TEXTENTRY_SELECTION, &usIndex)
				== BOB_OK) {

			if (usIndex == TURN_OFF_IMAGE) {
				memset(pStrOrValue, 0, sizeof(unichar));
				usLen = 1;
			} else if ((usIndex > 0) && (usIndex <= MAX_TEXT_ENTRY)) {
				switch (usVarID) {
				case TEXT_ENTRY_LINE_0 :
					ucLine = 0;
					break;
				case TEXT_ENTRY_LINE_1 :
					ucLine = 1;
					break;
				case TEXT_ENTRY_LINE_2 :
					ucLine = 2;
					break;
				case TEXT_ENTRY_LINE_3 :
					ucLine = 3;
					break;
				case TEXT_ENTRY_LINE_4 :
					ucLine = 4;
					break;
				case TEXT_ENTRY_LINE_5 :
					ucLine = 5;
					break;
				case TEXT_ENTRY_LINE_6 :
					ucLine = 6;
					break;
				default:
					break;
				}
				ptrtextEntryLineOfText = fnGetTextEntryLineOfText(
						(UINT8) (usIndex - 1), ucLine);
				if (ptrtextEntryLineOfText) {
					(void) unistrcpy(pStrOrValue, ptrtextEntryLineOfText);
					usLen = unistrlen(pStrOrValue);
					ubWritingDir = fnGetWritingDir();
					if ((ubWritingDir == RIGHT_TO_LEFT)
							&& (usLen < fnGetTxtmsgMaxCharsAllowed())) {
						usLen = fnGetTxtmsgMaxCharsAllowed();
						fnUnicodePadSpace(pStrOrValue, unistrlen(pStrOrValue),
								usLen);
					}
				}
			}
		} else {
			fnProcessSCMError();
		}

		return (usLen);
	}

	/******************************************************************************
	 FUNCTION NAME: fnfCurrentPermitBatchCount
	 AUTHOR       : Craig DeFilippo
	 DESCRIPTION  : Get the Permit specific Batch Count
	 PARAMETERS   : Standard report field function
	 RETURN       : number of unicode chars
	 NOTES        : none
	 ******************************************************************************/
	unsigned short fnfCurrentPermitBatchCount(void * pStrOrValue,
			unsigned short usVarID) {
		ushort usLen = 0;
		unichar sPermitName[MAX_PERMIT_UNICHARS + 1];
		ulong ulPermitBatchCount = 0;

		memset(sPermitName, 0, sizeof(sPermitName));
		if (fnValidData(BOBAMAT0, READ_CURRENT_PERMIT_NAME, sPermitName)
				== BOB_OK) {
			// read permit batch count based on permit name
			if (GetPermitDataByName(sPermitName, &ulPermitBatchCount)) {
				// increment the batch count since permit batch counters don't
				//  get incremented until AFTER the print
				ulPermitBatchCount++;

				usLen = fnBin2Unicode(pStrOrValue, ulPermitBatchCount);
			}
		} else {
			fnProcessSCMError();
		}

		return (usLen);
	}

	/******************************************************************************
	 FUNCTION NAME: fnfPermitRptVars
	 AUTHOR       : Craig DeFilippo
	 DESCRIPTION  : Get Permit report variables
	 PARAMETERS   : Standard report field function
	 RETURN       : number of unicode chars
	 NOTES        : none
	 MODIFICATION HISTORY:
	 2012.08.14 Liu Jupeng   Merged the fixing issue from the branch fphx02.08_c1_secap.
	 2011.08.21 Deborah Kohl FPHX - 02.08
	 - Changed function fnfGetPreDebitItems() to print the batch count corresponding
	 to the selected permit when we are in permit mode (before we were always printing the indicia
	 batch count)
	 - Changed functions fnDetermineTotalPages(), fnfPermitRptVars(), added fnGetPermitIndex(), PermitVarId2RecordIndex[]
	 to support printing of permit report on multiple pages
	 ******************************************************************************/
	unsigned short fnfPermitRptVars(void * pStrOrValue, unsigned short usVarID) {
		ulong ulLongVar = 0;
		ushort usLen = 0;
		unichar sPermitName[MAX_PERMIT_UNICHARS + 1] = { 0, 0 };
		uchar ucPermitIndex = 0;
		uchar pageIndex;

		// determine what slot we are filling on the current page
		pageIndex = fnGetPermitIndex(usVarID);

		// determine the corresponding list index for the page we are on
		ucPermitIndex = ((wCurrentPageNumber - 1) * ThreeEntriesPerPage)
				+ pageIndex;

		if (ucPermitIndex) {
			ulLongVar = fnGetPermitDataByOrdinal(ucPermitIndex, sPermitName);
			switch (usVarID) {
			case PERMIT_1_BATCH_COUNT :
			case PERMIT_2_BATCH_COUNT :
			case PERMIT_3_BATCH_COUNT :
				if (unistrlen(sPermitName)) {
					usLen = fnBin2Unicode(pStrOrValue, ulLongVar);
				}
				break;

			case PERMIT_1_BATCH_NAME :
			case PERMIT_2_BATCH_NAME :
			case PERMIT_3_BATCH_NAME :
				(void) unistrcpy(pStrOrValue, sPermitName);
				usLen = unistrlen(sPermitName);
				break;

			case PERMIT_PIECE_COUNT :
				ulLongVar = GetPermitPieceCount();
				usLen = fnBin2Unicode(pStrOrValue, ulLongVar);
				break;

			default:
				break;
			}
		}

		return (usLen);
	}

	/******************************************************************************
	 FUNCTION NAME: fnGetPermitDataByOrdinal
	 AUTHOR       : Craig DeFilippo
	 DESCRIPTION  : Get the nth Permit data name and batch count in use by scanning
	 : the Permit array and skipping over the permits with null names.
	 PARAMETERS   : ucOrdinalIndex = ordinal index (1st, 2nd, etc)
	 : pwPermitName = passback pointer to a permit name
	 RETURN       : batch count
	 NOTES        : none
	 ******************************************************************************/
	unsigned long fnGetPermitDataByOrdinal(uchar ucOrdinalIndex,
			unsigned short *pwPermitName) {
		ushort i;
		ulong ulLongVar = 0;

		for (i = 1; ((ucOrdinalIndex != 0) && (i <= MAX_PERMITS)); i++) {
			ulLongVar = GetPermitDataByNum(i, pwPermitName);
			if (unistrlen(pwPermitName)) {
				ucOrdinalIndex--;
			}
		}

		return (ulLongVar);
	}

	/******************************************************************************************************************************/
	/* TITLE:   fnGetPermitIndex                                                                                                  */
	/* AUTHOR:  D. Kohl                                                                                                           */
	/* INPUT:   variable ID                                                                                                       */
	/* DESCRIPTION:  Scan the variable id to record index table to find the record                                                */
	/*               index ( 0...) for this permit variable                                                                       */
	/* Output: index of error log entry                                                                                           */
	/* MODIFICATION HISTORY:                                                                                                      */
	/*    2012.08.14 Liu Jupeng     Merged the fixing issue from the branch fphx02.08_c1_secap.                                   */
	/*       2011.08.21 Deborah Kohl FPHX - 02.08                                                                                 */
	/*       - Changed function fnfGetPreDebitItems() to print the batch count corresponding                                      */
	/*         to the selected permit when we are in permit mode (before we were always printing the indicia                      */
	/*         batch count)                                                                                                       */
	/*       - Changed functions fnDetermineTotalPages(), fnfPermitRptVars(), added fnGetPermitIndex(), PermitVarId2RecordIndex[] */
	/*         to support printing of permit report on multiple pages                                                             */
	/******************************************************************************************************************************/
	unsigned char fnGetPermitIndex(ushort usVarID) {
		ushort i = 0;

		while ((rPermitVarId2RecordIndex[i][0] > 0)
				&& (rPermitVarId2RecordIndex[i][0] != usVarID)) {
			i++;
		}

		return (rPermitVarId2RecordIndex[i][1]);
	}

	/******************************************************************************
	 FUNCTION NAME: fnfSuppleServiceVars
	 AUTHOR       : Bob Arsenault and Craig DeFilippo
	 DESCRIPTION  : Retrieve a specified supplementary services text string
	 : from the rating product configuration list.
	 PARAMETERS   : Unicode string destination pointer, variable ID
	 RETURN       : length of unicode string, number of unicode chars
	 NOTES        : None
	 ******************************************************************************/
	unsigned short fnfSuppleServiceVars(void * pStrOrValue,
			unsigned short usVarID) {
		ushort usLen = 0;
		unichar *pSupple = NULL_PTR;

#if defined (G900) || defined (K700)
//    const struct ind_output *pDCInfo = NULL;
//    SINT16       sRet;
#else
		RMAI_PCL_OUTPUT *pPCLOutput = NULL;
#endif

		switch (usVarID) {
		case INDICIA_PCL_STRING1 :         // indicia line 1, DCM Indicia_HRT[0]
			if (!fnDuckSwissProductText()) {
				pSupple = fnGetIndiciaLine(1);
			}
			break;

		case INDICIA_PCL_STRING2 :         // indicia line 2, DCM Indicia_HRT[1]
			if (!fnDuckSwissProductText()) {
				pSupple = fnGetIndiciaLine(2);
			}
			break;

		case INDICIA_DCM_STRING4 :           // DCM Indicia_HRT[3]
			pSupple = fnGetIndiciaLine(4);
			break;

		case INDICIA_DCM_STRING5 :           // DCM Indicia_HRT[4]
			pSupple = fnGetIndiciaLine(5);
			break;

		case VAS_SUP_PCL_STRING1 :           // supplemental service line 1
			pSupple = fnGetSupplementalLine(1);
			break;

		case VAS_SUP_PCL_STRING2 :           // supplemental service line 2
			pSupple = fnGetSupplementalLine(2);
			break;

		case TRACK_SERV_HRT2 :               // supplemental service line 3
			pSupple = fnGetSupplementalLine(3);
			break;

		case TRACK_SERV_HRT3 :               // supplemental service line 4
			pSupple = fnGetSupplementalLine(4);
			break;

		case TRACK_SERV_HRT4 :               // supplemental service line 5
			pSupple = fnGetSupplementalLine(5);
			break;

		case ITALY_FEECOMBOID :
#if defined (G900) || defined (K700)
			// not needed for G9 or K7 U.K. EIB because the data is sourced
			// from DCM Indicia_HRT[1]
#else
			pPCLOutput = (RMAI_PCL_OUTPUT *)fnRateGetProdConfigListInfo();
			if ((pPCLOutput) && (pPCLOutput->iError == 0) )
			{
				pSupple = pPCLOutput->pPCLInfo->FeeComboID;
			}
#endif
			break;

		default:                            // any other case
			pSupple = NULL_PTR;
			break;
		}

		if ((pStrOrValue != NULL_PTR ) && (pSupple != NULL_PTR )) {
			(void) unistrcpy(pStrOrValue, pSupple);
			usLen = unistrlen(pStrOrValue);
		}

		return (usLen);
	}

	/******************************************************************************
	 FUNCTION NAME: fnfGetGermManifestInfo
	 AUTHOR       : Sandra Peterson
	 DESCRIPTION  : Retrieve the data for the Germany manifest reports
	 PARAMETERS   : Unicode string destination pointer, variable ID
	 RETURN       : length of unicode string, number of unicode chars
	 NOTES        : None
	 ******************************************************************************/
	unsigned short fnfGetGermManifestInfo(void * pStrOrValue,
			unsigned short usVarID) {

		ushort usLen = 0;

		return (usLen);
	}

	/******************************************************************************
	 FUNCTION NAME: fnfReadDebitCertParmsSplitFont
	 AUTHOR       : Bob Arsenault and Craig DeFilippo
	 DESCRIPTION  : Read a parameter from the IPB_GET_INDICIA_DEF_REC and convert
	 : it to unicode.  Then convert it to split font Unicode.
	 PARAMETERS   : Unicode string destination pointer, variable ID
	 RETURN       : length of unicode string, number of unicode chars
	 NOTES        : None
	 ******************************************************************************/

	unsigned short fnfReadDebitCertParmsSplitFont(void * pStrOrValue,
			unsigned short usVarID) {
		/*   never implemented use of a split font

		 uchar           myStrOrValue[40];   // buffer for holding unicode conversion of the variable
		 ushort          myLen;              // length of unicode money string prior to font split
		 ushort          j;                  // loop variable
		 IPSD_DEF_REC    rFieldRec;          // structure for holding an ibi variable descriptor
		 uchar           *pData;             // pointer to a variable's location in the debit certificate
		 short           myShort;            // long conversion of a 4 character variable
		 double          myDouble;           // double conversion of a variable
		 uchar           *pDestPtr;          // pointer into the passed location pStrOrValue
		 uchar           *pSourcePtr;        // pointer to the unicode conversion of the variable
		 ushort          usLen = 0;          // returned unicode length
		 uchar           *pHalfChars;        // list of characters that are to be printed in half width form
		 BOOL            fHalfFlag;          // set true for half width characters
		 ushort          charcnt;            // index into pHalfChars
		 ushort          usListLen;

		 //get the IBI record from flash
		 if(fnFlashGetIBPRecVar(&rFieldRec,IPB_GET_INDICIA_DEF_REC, usVarID))
		 {
		 // get a pointer to the debit certificate
		 pData = (uchar*)pIPSD_barcodeData.pGlob;

		 // copy the variable from the debit certificate to a short
		 // then convert the short to a double
		 // then normalize the double to a three decimal currency
		 memcpy((uchar *)&myShort, (uchar *) (pData+rFieldRec.usFieldOff), rFieldRec.usFieldLen);
		 myDouble = myShort;
		 j = fnFlashGetIBByteParm(IBP_FIXED_ZEROS);
		 while(j--)
		 {
		 myDouble *= 10;
		 }

		 // convert the double to a unicode money string with decimal pt and money symbol
		 myLen = fnFormatRptDoubleMoney((void*)myStrOrValue, myDouble, MAILPIECE_FIELDS, TRUE);

		 // spread the unicode string into the split font format
		 pDestPtr = (uchar *)(pStrOrValue);
		 pSourcePtr = (uchar *)(myStrOrValue);
		 pHalfChars = fnFlashGetAsciiStringParm(ASP_HALF_WIDTH_CHAR_LIST);
		 if (pHalfChars)
		 {
		 usListLen = strlen((char *)(pHalfChars));
		 for (j = 0; j < myLen; j++)
		 {
		 // fill the first half of the split character
		 pDestPtr[0] = pSourcePtr[0];
		 pDestPtr[1] = pSourcePtr[1];
		 pDestPtr += 2;
		 usLen++;
		 fHalfFlag = FALSE;
		 for (charcnt = 0; charcnt < usListLen; charcnt++)
		 {
		 if (pHalfChars[charcnt] == pSourcePtr[1]) fHalfFlag = TRUE;

		 }
		 // if the character is not a half-character, fill the second half of the split character
		 if (!fHalfFlag)
		 {
		 pDestPtr[0] = 0xFF;
		 pDestPtr[1] = pSourcePtr[1];
		 pDestPtr += 2;
		 usLen++;
		 }
		 pSourcePtr += 2;
		 }
		 }
		 }
		 return(usLen);
		 */
		return (0);
	}

	/******************************************************************************
	 FUNCTION NAME: fnfZipCodeFieldData
	 AUTHOR       : blu
	 DESCRIPTION  : Retrieve the origin information that is stored as a subset within
	 : the zipcode array.  The format of the zipcode array is
	 : <ASCII POSTAL CODE> <ASCII ORIGIN INFORMATION> <STOW AWAY>
	 : where     ASCII POSTAL CODE is 6 bytes,
	 :           ASCII ORIGIN INFORMATION is typically 32 bytes
	 :
	 : STOW AWAY is used in some products and some PCNs if
	 : EMD parameters BP_UNZIP_AND_STRIP, and BP_MAX_ZIPS_TO_STRIP
	 : are set. This is typically used in China and sometimes in France.
	 : ============================================
	 : CURRENTLY IN JANUS, "STOW AWAY" IS NOT USED.
	 : ==============================================
	 PARAMETERS   : Unicode string destination pointer, variable ID
	 RETURN       : length of unicode string, number of unicode chars
	 NOTES        : None
	 ******************************************************************************/
	unsigned short fnfZipCodeFieldData(void * pStrOrValue,
			unsigned short usVarID) {
		char infoBuffer[IPSD_SZ_ZIPCODE + 1];

		// default to nothing in the string
		((unichar *) pStrOrValue)[0] = 0;

		// get the PBI serial number
		(void) memcpy(infoBuffer, pIPSD_zipCode, IPSD_SZ_ZIPCODE);
		infoBuffer[IPSD_SZ_ZIPCODE] = 0;    // make sure it's null-terminated

		switch (usVarID) {
		case ORIGIN_LINES_ID:
			(void) unistrcpyFromCharStr((unichar *) pStrOrValue,
					&infoBuffer[ORIGIN_LINES_OFFSET]);
			break;

		case POSTAL_SERIAL_NUMBER_ID:
			(void) unistrcpyFromCharStr((unichar *) pStrOrValue,
					&infoBuffer[POSTAL_SERIAL_NUMBER_OFFSET]);
			break;

		case POSTAL_UNIT_ID:
			(void) unistrcpyFromCharStr((unichar *) pStrOrValue,
					&infoBuffer[POSTAL_UNIT_OFFSET]);
			break;

		default:
			((unichar *) pStrOrValue)[0] = 0;
			break;
		}

		return ((unsigned short) unistrlen((unichar *) pStrOrValue));
	}

	/******************************************************************************
	 FUNCTION NAME: fnfGetPrintFailLogInfo
	 AUTHOR       : blu
	 DESCRIPTION  : Retrieve information from the CMOS Print Failure Incident Log,
	 : based on the passed variable ID and formats the information
	 : in unicode.  The CMOS Print Failure Incident Log is updated
	 : after every debit.  If the subsequent print fails, a report
	 : can be printed that indicates the lost postage.  The report is
	 : enabled on a per PCN basis by an EMD context parameter.
	 :
	 : This function is called with the variable ID of information that
	 : will appear in the report.
	 :
	 PARAMETERS   : Unicode string destination pointer, variable ID
	 RETURN       : length of unicode string, number of unicode chars
	 NOTES        : None
	 HISTORY:
	 Fix case for DATE_FORMAT_DDMMYYYY.  Was copying garbage into year.
	 ******************************************************************************/
	unsigned short fnfGetPrintFailLogInfo(void * pStrOrValue,
			unsigned short usVarID) {
		ushort usLen = 0;      // the returned length in number of characters
		PF_DEBIT_LOG_REC *tPtr;         // pointer to the CMOS print fail record
		char tmpStr[12];         // temporary container for a string value
		long lFunds;
		double dFunds;
		BOOL transferDone;
		unsigned char option;
		char DateSeparation;
		char pCentury[3] = "20";

		transferDone = FALSE;

		// set a pointer to the CMOS print fail record
		tPtr = (PF_DEBIT_LOG_REC *) &CMOSpfDebitLogRec.pfCMOSDebitRecord[0];
		memset(tmpStr, 0, sizeof(tmpStr));

		// In the following switch statement, the requested information is retrieved
		// and, if required, translated into ASCII.  The resulting string is stored
		// in local variable tmpStr.  After the switch statement, the information in
		// tmpStr is translated into unicode and transferred to the requested location.
		// In all instances local variable usLen is filled with the length of the
		// information
		switch (usVarID) {
		// LOGGED INDICIA SERIAL NUMBER
		case GLOBID_INDSN:
			usLen = sizeof(tPtr->zIndiciaSerialNumber);
			memcpy(tmpStr, (char *) &tPtr->zIndiciaSerialNumber[0], usLen);
			tmpStr[usLen] = '\0';
			break;

			// LOGGED POSTAGE VALUE
			// Copy the postage value from the log
		case GLOBID_PF_LOGPOSTAGE:
			usLen = sizeof(tPtr->iPostageValue);
			memcpy((char *) (&lFunds), (char *) &tPtr->iPostageValue[0], usLen);
			dFunds = lFunds;
			usLen = fnFormatRptDoubleMoney(pStrOrValue, dFunds, REPORT_FIELDS,
					TRUE);
			transferDone = TRUE;
			break;

			// LOGGED POSTAL CODE
		case GLOBID_PF_REGATECODE:
			usLen = sizeof(tPtr->zRegateCode);
			memcpy(tmpStr, (char *) &tPtr->zRegateCode[0], usLen);
			tmpStr[usLen] = '\0';
			break;

			// LOGGED PIECE COUNT
		case GLOBID_PF_PIECECOUNT:
			usLen = sizeof(tPtr->zPieceCount);
			memcpy(tmpStr, (char *) &tPtr->zPieceCount[0], usLen);
			tmpStr[usLen] = '\0';
			break;

			// LOGGED ITEM CATEGORY
		case GLOBID_PF_ITEMCAT:
			usLen = sizeof(tPtr->zItemCategory);
			memcpy(tmpStr, (char *) &tPtr->zItemCategory[0], usLen);
			tmpStr[usLen] = '\0';
			break;

			// LOGGED AUTHENTICATION CODE
		case GLOBID_PF_AUTHCODE:
			usLen = sizeof(tPtr->zFrAuthCode);
			memcpy(tmpStr, (char *) &tPtr->zFrAuthCode[0], usLen);
			tmpStr[usLen] = '\0';
			break;

			// LOGGED PRINT STATE
		case GLOBID_PF_PRINTSTAT:
			if (fnGetMailProgressState() == PF_PRINT_IN_PROGRESS)
				GetNativeWord(pStrOrValue, NATIVE_YES);
			else
				GetNativeWord(pStrOrValue, NATIVE_NO);
			transferDone = TRUE;
			usLen = 0;
			usLen = unistrlen(pStrOrValue);
			break;

			// LOGGED DATE OF PRINT
		case GLOBID_PF_DATE:
			usLen = sizeof(tPtr->zIndiciaDate);
			memcpy(tmpStr, &tPtr->zIndiciaDate[0], usLen);
			tmpStr[usLen] = '\0';
			option = fnFlashGetByteParm(BP_REPORT_DATE_FORMAT);
			DateSeparation = (char) fnFlashGetByteParm(
					BP_PRINTABLE_DATE_SEP_CHAR);
			switch (option) {
			case DATE_FORMAT_DDMMYY:
				memcpy(&tmpStr[0], &tPtr->zIndiciaDate[0], 2);
				tmpStr[2] = DateSeparation;
				memcpy(&tmpStr[3], &tPtr->zIndiciaDate[2], 2);
				tmpStr[5] = DateSeparation;
				memcpy(&tmpStr[6], &tPtr->zIndiciaDate[4], 2);
				tmpStr[8] = 0;
				usLen = strlen(tmpStr);
				break;

			case DATE_FORMAT_DDMMYYYY:
				memcpy(&tmpStr[0], &tPtr->zIndiciaDate[0], 2);
				tmpStr[2] = DateSeparation;
				memcpy(&tmpStr[3], &tPtr->zIndiciaDate[2], 2);
				tmpStr[5] = DateSeparation;
				memcpy(&tmpStr[6], pCentury, 2);
				memcpy(&tmpStr[8], &tPtr->zIndiciaDate[4], 2);
				tmpStr[10] = 0;
				usLen = strlen(tmpStr);
				break;

			default:
				break;
			}
			break;

		default:
			break;
		}

		if (transferDone == FALSE)
			(void) unistrcpyFromCharStr(pStrOrValue, tmpStr);
		return (usLen);
	}

	/******************************************************************************
	 FUNCTION NAME: fnfGetPostdatingLogInfo
	 AUTHOR       : Sandra Peterson
	 DESCRIPTION  : Get French Postdating report variables
	 PARAMETERS   : Standard report field function
	 RETURN       : number of unicode chars
	 NOTES        : none
	 ******************************************************************************/
	unsigned short fnfGetPostdatingLogInfo(void * pStrOrValue,
			unsigned short usVarID) {
		DATETIME dtLogEntryDate;
		double dFunds;
		long lCount;
		ushort usTmp;
		ushort usLen = 0;      // the returned length in number of characters
		uchar ucInx = 0;
		char tmpStr[20];         // temporary container for a string value
		char DateSeparation = '/';
		BOOL fDoCopy = TRUE;

		(void) memset(tmpStr, 0, sizeof(tmpStr));

		// calculate which record in the log is needed
		while ((rPostdatingVarId2RecordIndex[ucInx][0] > 0)
				&& (rPostdatingVarId2RecordIndex[ucInx][0] != usVarID))
			ucInx++;

		// If there are no summary records, use a space for everything but the first line
		if (!usFrenchNumRecs) {
			switch (usVarID) {
			case GET_POSTDATING_DATE0 :
			case GET_POSTDATING_AR0 :
			case GET_POSTDATING_PC0 :
				(void) strcpy(tmpStr, "****");
				break;

			default:
				(void) strcpy(tmpStr, " ");
				break;
			}

			usLen = (UINT16) strlen(tmpStr);
		} else {
			usTmp = (UINT16) (((wCurrentPageNumber - 1) * FiveEntriesPerPage)
					+ rPostdatingVarId2RecordIndex[ucInx][1]);

			// If request is for a record that doesn't exist, use a space for everything
			if (usTmp >= usFrenchNumRecs) {
				(void) strcpy(tmpStr, " ");
				usLen = 1;
			} else {
				// Determine the index
				ucFrenchIndex = (uchar) (ucFrenchStartIndex + usTmp);
				if (ucFrenchIndex >= POST_LOG_SZ)
					ucFrenchIndex -= POST_LOG_SZ;

				//TODO JAH remove dcperiod.c dtLogEntryDate = fnDCAP_GetPost_Dating_LogEntry(ucFrenchIndex, &dFunds, &lCount);

				switch (usVarID) {
				case GET_POSTDATING_DATE0 :
				case GET_POSTDATING_DATE1 :
				case GET_POSTDATING_DATE2 :
				case GET_POSTDATING_DATE3 :
				case GET_POSTDATING_DATE4 :
					(void) sprintf(tmpStr, "%02d%c%02d", dtLogEntryDate.bDay,
							DateSeparation, dtLogEntryDate.bMonth);
					usLen = (UINT16) strlen(tmpStr);
					break;

				case GET_POSTDATING_AR0 :
				case GET_POSTDATING_AR1 :
				case GET_POSTDATING_AR2 :
				case GET_POSTDATING_AR3 :
				case GET_POSTDATING_AR4 :
					fDoCopy = FALSE;
					usLen = fnFormatRptDoubleMoney(pStrOrValue, dFunds,
							REPORT_FIELDS, FALSE);
					break;

				case GET_POSTDATING_PC0 :
				case GET_POSTDATING_PC1 :
				case GET_POSTDATING_PC2 :
				case GET_POSTDATING_PC3 :
				case GET_POSTDATING_PC4 :
					(void) sprintf(tmpStr, "%d", lCount);
					usLen = (UINT16) strlen(tmpStr);
					break;

				default:
					fDoCopy = FALSE;
					break;
				}
			}
		}

		if (fDoCopy == TRUE)
			(void) unistrcpyFromCharStr(pStrOrValue, tmpStr);

		return (usLen);
	}

	/******************************************************************************
	 FUNCTION NAME: fnfGetFrenchCurrentAR
	 AUTHOR       : Sandra Peterson
	 DESCRIPTION  : Get current AR value for the French data log reports
	 PARAMETERS   : Standard report field function
	 RETURN       : number of unicode chars
	 NOTES        : none
	 ******************************************************************************/
	unsigned short fnfGetFrenchCurrentAR(void * pStrOrValue,
			unsigned short usVarID) {
		ushort usLen = 0;      // the returned length in number of characters
		unichar uniString[MAX_RESULT_STRING + 1];

		(void) memset(uniString, 0, sizeof(uniString));
		if (fnGetUnicodeReportData(BOBAMAT0, GET_VLT_ASCENDING_REG, uniString)
				== BOB_OK) {
			usLen = unistrlen(uniString);
			(void) unistrcpy(pStrOrValue, uniString);
		} else
			fnProcessSCMError();

		return (usLen);
	}

	/******************************************************************************
	 FUNCTION NAME: fnfFormatFrenchReportDate
	 AUTHOR       : Sandra Peterson
	 DESCRIPTION  : Formats the data for the French data log reports
	 PARAMETERS   : Standard report field function
	 RETURN       : number of unicode chars
	 NOTES        : none
	 // So why can't they set the report date format in the French EMD?
	 // I mean, why is this function necessary??? -cb
	 ******************************************************************************/
	static UINT16 fnfFormatFrenchReportDate(char * pStr,
			const DATETIME * DateTimePtr) {
		char DateSeparation = '/';
		UINT8 option = DATE_FORMAT_DDMMYY;

		retcode = fnFormatDateASCII(pStr, DateTimePtr, option, DateSeparation,
				DateSeparation);

		/*sprintf(pStr,"%02d%c%02d%c%02d", DateTimePtr->bDay, DateSeparation,
		 DateTimePtr->bMonth, DateSeparation,
		 DateTimePtr->bYear);
		 */

		return ((UINT16) strlen(pStr));
	}

	/******************************************************************************
	 FUNCTION NAME: fnfFormatFrenchReportTime
	 AUTHOR       : Sandra Peterson
	 DESCRIPTION  : Formats the time for the French data log reports
	 PARAMETERS   : Standard report field function
	 RETURN       : number of unicode chars
	 NOTES        : none
	 ******************************************************************************/
	static UINT16 fnfFormatFrenchReportTime(char * pStr,
			const DATETIME * DateTimePtr) {
		(void) sprintf(pStr, "%02d:%02d", DateTimePtr->bHour,
				DateTimePtr->bMinutes);
		return ((UINT16) strlen(pStr));
	}

	/******************************************************************************
	 FUNCTION NAME: fnfGetHRPostalLogo
	 AUTHOR       : Craig DeFilippo
	 DESCRIPTION  : Get the font character that indicates which indicia logo to
	 : print in the indicia.
	 PARAMETERS   : Standard report field function
	 RETURN       : number of unicode chars
	 NOTES        : none
	 ******************************************************************************/
	unsigned short fnfGetHRpostalLogo(void * pStrOrValue,
			unsigned short usVarID) {
		char *pLogoList = 0;
		int i;
		unichar uniLogoChar[2] = { 0x0041, 0x0000 };  // Set default to 'A'

		unsigned char bLogoCount = 0;
		unsigned char status;
		unsigned char index;
		char bLocalChar = 'A';                      // Set default to 'A'
		BOOL enabled;
		UNICHAR *pString;

		if (usVarID == UK_POSTAL_LOGO) {
			// for the U.K., the logic comes from the tablet and it sends one out four characters
			// to choose the font character that has
			// 'A' Royal Mail
			// 'B' Post Brenhinol
			// 'C' delivered by Royal Mail
			// 'D' Dosbarthwyd gan/Delivered by Post Brenhinol
			// else, use the default of 'A'.

			{
				pString = fnGetIndiciaString();
				uniLogoChar[0] = (char *) pString[0];
			}
		}
		/*}
		 else
		 {
		 pLogoList = (char *)fnFlashGetPackedByteParm(PBP_LOGO_SYM_MAP);
		 if(pLogoList)
		 bLogoCount = (UINT8)*(pLogoList++);

		 // if the parameter is available AND there are items in the list AND PbP set the ISO code,
		 //  search for a match.
		 // else, default to 'A"
		 if (pLogoList && bLogoCount && CMOSIndiciaLanguageCode[0] != 0)
		 {
		 for (i = 0; i < bLogoCount; i++)
		 {
		 // check the ISO code from PbP against the ones in the list.
		 // if it can't be found, we'll eventually default to 'A'.
		 if (strncmp(pLogoList, CMOSIndiciaLanguageCode, 3) != 0)
		 {
		 bLocalChar++;    // if not this one, bump next char in font
		 pLogoList += 3;  // point to next ISO code in list
		 }
		 else
		 {
		 uniLogoChar[0] = bLocalChar & 0x00FF;   // If we found a winner, claim it
		 break;                                  // exit the loop
		 }
		 }
		 }
		 }
		 */

		(void) unistrcpy(pStrOrValue, uniLogoChar);
		return (1);
	}

	/******************************************************************************
	 FUNCTION NAME: fnfGetIndiciaType
	 AUTHOR       : Sam Thillaikumaran
	 DESCRIPTION  : Retrieve the string to be printed in the Canadian indicia
	 : for different modes of Operation.
	 :
	 PARAMETERS   : Unicode string destination pointer, variable ID
	 RETURN       : length of unicode string, number of unicode chars
	 NOTES        : Modified to get the indicia string from the PCL ASCII structure
	 that was populated from the Data Capture Engine. Tom Dometios 7/25/2005
	 ******************************************************************************/
	unsigned short fnfGetIndiciaType(void * pStrOrValue, unsigned short usVarID) {
		UINT16 usLength = 0;
		unichar *indiciaString = fnfGetSelectedIndiciaString(0);

//  Get the length of the selected indicia string and copy to output buffer
		usLength = unistrlen(indiciaString);

		memcpy(pStrOrValue, indiciaString, (usLength * sizeof(unichar)));

		return (usLength);
	}

	/******************************************************************************
	 FUNCTION NAME: fnfGetSwissBarcodeInfo
	 AUTHOR       : Craig DeFilippo
	 DESCRIPTION  : Format data for the Swiss 2D barcode.  All data is ASCII.
	 : Some fields must be zero when postage equals zero.
	 PARAMETERS   : string destination pointer, variable ID
	 RETURN       : length of string, number of chars
	 NOTES    : None
	 ******************************************************************************/
	unsigned short fnfGetSwissBarcodeInfo(void * pStrOrValue,
			unsigned short usVarID) {
		uchar* pTemp = NULL_PTR;
		DATETIME dtTemp;
		const struct ind_output *pDCInfo;
		SINT16 sRet;
		long lLen;
		ulong ulPostageValue, ulTemp;
		ushort usLen = 0, usTemp;

		if (fnValidData(BOBAMAT0, VAULT_DEBIT_VALUE, (void *) &ulPostageValue)
				!= BOB_OK) {
			fnProcessSCMError();
		} else {
			switch (usVarID) {
			case NON_ZERO_PIECE_COUNT :
				// duck season?
				if (ulPostageValue != 0) {
					if (fnValidData(BOBAMAT0, GET_LOCAL_PIECE_COUNT, &ulTemp)
							!= BOB_OK) {
						fnProcessSCMError();
					} else {
						// The local piece count is not updated yet so need to add 1
						// to have the piece count corresponding to the franking
						// For version 02.11 for Switzerland,it seems that this change is no longer
						// necessary and that the local piece count is already updated.
//                      ulTemp++;
						lLen = sprintf(pStrOrValue, "%u", ulTemp);
						if (lLen > 0)
							usLen = (unsigned short) lLen;
					}
				}
				break;

			case POST_OFFICE_ID :
				pTemp = fnFlashGetIBPackedByteParm(IPB_POSTAL_AUTH_CODE);
				(void) memcpy(pStrOrValue, pTemp, 3);
				usLen = 3;
				break;

			case STUFEN_NUMMER :
//                sRet = fnRateGetIndiciaInfo(&pDCInfo);
				/*
				 if (sRet == RSSTS_NO_ERROR && ( ulPostageValue != 0 ))
				 {
				 lLen = sprintf(pStrOrValue, "%010ld", pDCInfo->BarcodeData->ProductIDNumeric);
				 if(lLen > 0)
				 usLen = (unsigned short)lLen;
				 }
				 */
				break;

			case DCAP_PERIOD_NUMBER :
				// duck season?
				if (ulPostageValue != 0) {
					//TODO JAH remove dcperiod.c usTemp = fnDCAP_GetNext_ReportID();
					lLen = sprintf(pStrOrValue, "%u", usTemp);
					if (lLen > 0)
						usLen = (unsigned short) lLen;
				}
				break;

			case PBP_ACCOUNT_NUMBER :
				lLen = sprintf(pStrOrValue, "%x",
						CMOSSetupParams.lwPBPAcctNumber);
				if (lLen > 0)
					usLen = (unsigned short) lLen;
				break;

			case SWISS_BARCODE_MAILDATE :
				if (GetSysDateTime(&dtTemp, ADDOFFSETS, GREGORIAN)) {
					lLen = sprintf(pStrOrValue, "%d%02d%02d%02d",
							dtTemp.bCentury, dtTemp.bYear, dtTemp.bMonth,
							dtTemp.bDay);
					if (lLen > 0)
						usLen = (unsigned short) lLen;
				}
				break;

			case CUSTOMERID_RRN :
				(void) strncpy(pStrOrValue, CMOSCustomerID_RRN,
						CUSTOMERID_RRN_SZ);
				*((UINT8 *) pStrOrValue + CUSTOMERID_RRN_SZ) = 0;
				usLen = strlen(pStrOrValue);
				break;

			default:
				break;
			}
		}

		return (usLen);
	}

	/******************************************************************************
	 FUNCTION NAME: fnfGetASRLogInfo
	 AUTHOR       : Bruno Debuire
	 DESCRIPTION  : Get Ad Slogan Register report variables
	 PARAMETERS   : Standard report field function
	 RETURN       : number of unicode chars
	 NOTES    : none
	 ******************************************************************************/
	unsigned short fnfGetASRLogInfo(void * pStrOrValue, unsigned short usVarID) {
		UINT16 usTmp;
		UINT16 usLen = 0;
		UINT8 ucInx = 0;
		char tmpStr[20];     // temporary container for a string value
		BOOL fDoCopy = TRUE;
		UINT16 usMaxRecCount;
		UINT32 ulPieceCount = 0;

		memset(tmpStr, 0, sizeof(tmpStr));

		// calculate which record in the log is needed
		while ((rASRVarId2RecordIndex[ucInx][0] > 0)
				&& (rASRVarId2RecordIndex[ucInx][0] != usVarID)) {
			ucInx++;
		}

		usTmp = ((wCurrentPageNumber - 1) * FiveEntriesPerPage)
				+ rASRVarId2RecordIndex[ucInx][1];

		//TODO JAH remove asrapi.c usMaxRecCount = (unsigned short)fnASRNbCmosASRLogEntryUsed(0);
		// If request is for a record that doesn't exist, use a space for everything
		if (usTmp >= usMaxRecCount) {
			strcpy(tmpStr, " ");
			usLen = 1;
		} else {
			//TODO JAH remove asrapi.c fnASRGetCmosASRLogEntry(usTmp, (UINT8 *)tmpStr, &ulPieceCount);

			switch (usVarID) {
			case GET_ASR_ID0 :
			case GET_ASR_ID1 :
			case GET_ASR_ID2 :
			case GET_ASR_ID3 :
			case GET_ASR_ID4 :
				usLen = (UINT16) strlen(tmpStr);
				break;

			case GET_ASR_PC0 :
			case GET_ASR_PC1 :
			case GET_ASR_PC2 :
			case GET_ASR_PC3 :
			case GET_ASR_PC4 :
				sprintf(tmpStr, "%d", ulPieceCount);
				usLen = (UINT16) strlen(tmpStr);
				break;

			default:
				fDoCopy = FALSE;
				break;
			}
		}

		if (fDoCopy == TRUE)
			(void) unistrcpyFromCharStr(pStrOrValue, tmpStr);

		return (usLen);
	}

	/******************************************************************************
	 FUNCTION NAME: fnfGetBrazilInfos
	 AUTHOR       : Andy Mo
	 DESCRIPTION  : Get the Human Readable Barcode Data specific to Brazil.
	 PARAMETERS   : Standard report field function
	 RETURN       : number of unicode chars
	 NOTES    : none
	 HISTORY:
	 2008.04.25  Clarisa Bellamy - Fix bug that can occur on Brazil, at the DH
	 cross-over time.  For the DH calculation, do not retrieve a new system
	 time, use the system time that was saved during the flex debit caclucation
	 instead.

	 ******************************************************************************/
	UINT16 fnfGetBrazilInfo(void * pStrOrValue, UINT16 usVarID) {
		UINT32 ulRunTimeSeconds = 0;
		UINT32 ulDHOnTime = 0;
		UINT16 usLen = 0;
		UINT16 usDHStr[] = { 'D', 'H', 0 };
		char tmpStr[MAX_RESULT_STRING + 1]; // temporary container for a string value

		switch (usVarID) {
		// After Hours/DH Indicator
		case INDICIA_BRAZIL_DH_INDICATOR :
			// Start with the runtime that was used to print the time in the
			//  barcode. Convert it to seconds, and compare it to the DH time
			//  (which is in seconds already.)  If it is DH time or later,
			//   then print "DH" else set string to empty string.
			ulRunTimeSeconds = fnTodToSecs(&stRunTime);
			ulDHOnTime = fnGetCMOSLongDHOnTime();
			if (ulRunTimeSeconds >= ulDHOnTime) {
				(void) unistrcpy(pStrOrValue, usDHStr);
				usLen = (UINT16) unistrlen(pStrOrValue);
			} else {
				memset(pStrOrValue, 0, sizeof(ushort));
			}
			break;

		case GLOBID_CMOSPRNTD_IND_NUM:
			// Use the same function that will put it into the barcode, then
			//  copy from ASCII to unistring.   We need to do this, instead of reading
			//  it from the modified barcode data because it is static data.  It only
			//  gets updated at the BEGINNING of a run, and at that time, the data is
			//  not yet IN the modified barcode glob.
			usLen = fnfFlexDebitBrazilPostAscii(tmpStr, usVarID);
			(void) unistrcpyFromCharStr(pStrOrValue, tmpStr);
			break;

		default:
			break;
		}

		return (usLen);
	}

	/******************************************************************************
	 FUNCTION NAME: fnfGetAjoutInfo
	 AUTHOR       : Ivan Le Goff
	 DESCRIPTION  : Get French Ajout report variables
	 PARAMETERS   : Standard report field function
	 RETURN       : number of unicode chars
	 NOTES    : none
	 ******************************************************************************/
	unsigned short fnfGetAjoutInfo(void * pStrOrValue, unsigned short usVarID) {
		char tmpStr[20];     // temporary container for a string value
		UINT16 usLen = 0;
		BOOL fDoCopy = TRUE;

		memset(tmpStr, 0, sizeof(tmpStr));

		switch (usVarID) {
		// Get Ajout Piece Count
		case GET_AJOUT_PC :
			sprintf(tmpStr, "%d", uiCMOSAjoutPieceCount);
			usLen = (UINT16) strlen(tmpStr);
			break;

		default:
			fDoCopy = FALSE;
			break;
		}

		if (fDoCopy == TRUE)
			(void) unistrcpyFromCharStr(pStrOrValue, tmpStr);

		return (usLen);
	}

	/******************************************************************************
	 FUNCTION NAME: fnfGetBarcodeVCRInfo
	 AUTHOR       : Sandra Peterson
	 DESCRIPTION  : Set the action code for what to load in the VCR area for the
	 : 2D barcode.
	 :
	 PARAMETERS   : Unicode string destination pointer, variable ID
	 RETURN       : length of unicode string, number of unicode chars
	 NOTES        :
	 ******************************************************************************/
	unsigned short fnfGetBarcodeVCRInfo(void * pStrOrValue,
			unsigned short usVarID) {
		ushort usLen = 1;

		// For now, we only need to indicate that the barcode should be loaded.
		// In the future, we may need to load other characters to indicate other
		// actions to take.
		((unichar *) pStrOrValue)[0] = BARCODE_ACTION_LOAD_BARCODE;

		((unichar *) pStrOrValue)[1] = 0;        // null terminate

		return (usLen);
	}

	/******************************************************************************
	 FUNCTION NAME: fnfGetMiscCounts
	 AUTHOR       : Sandra Peterson
	 DESCRIPTION  : Get various miscellaneous counts that might be coming from
	 : the PSD
	 :
	 PARAMETERS   : Unicode string destination pointer, variable ID
	 RETURN       : length of unicode string, number of unicode chars
	 NOTES        :
	 ******************************************************************************/
	unsigned short fnfGetMiscCounts(void * pStrOrValue, unsigned short usVarID) {
		ulong ulRefillCount;
		char tStr[30] = { 0 };

		switch (usVarID) {
		case AR_ROLLOVER_CNT_ID:
			// processing TBD
			break;

		case REFILL_FAILURE_CNT_ID:
			// processing TBD
			break;

		case REFILL_CNT_ID:
#if !defined(K700) && !defined(G900)
			(void)memcpy(&ulRefillCount, pIPSD_refillCount,sizeof(pIPSD_refillCount));
			(void)sprintf(tStr, "%d", ulRefillCount);
			(void)unistrcpyFromCharStr((unichar *)(pStrOrValue), tStr);
#endif
			break;

		default:
			break;
		}

		return ((unsigned short) strlen(tStr));
	}

	/******************************************************************************
	 FUNCTION NAME: fnfGetWeight
	 AUTHOR       : Sandra Peterson
	 DESCRIPTION  : Get the current weight.
	 :
	 PARAMETERS   : Unicode string destination pointer, variable ID
	 RETURN       : length of unicode string, number of unicode chars
	 NOTES        :
	 ******************************************************************************/
	unsigned short fnfGetWeight(void * pStrOrValue, unsigned short usVarID) {
#if !defined (G900) && !defined (K700)
		unsigned long ulWeight;
		uchar ucWeight[LENWEIGHT];
		char cWeightString[(LENWEIGHT*2)+1];

		fnRateGetWeight(ucWeight);

		switch(fnRateGetWeightUnit())
		{
			case AVOIRDUPOIS:
			// no need for this yet, so don't know how we want it formatted, so return nothing
			cWeightString[0] = 0;
			break;

			case METRIC:
			// convert the weight to a long & remove the tens of grams
			ulWeight = (RatePackedBcdWeightToLong(ucWeight)) / 10;
			(void)sprintf(cWeightString, "%d", ulWeight);
			break;

			default:
			// unknown weight units, so return nothing
			cWeightString[0] = 0;
			break;
		}

		(void)unistrcpyFromCharStr((unichar *)(pStrOrValue), cWeightString);

		return((unsigned short)strlen(cWeightString));
#else
		return (0);
#endif
	}

	/******************************************************************************
	 FUNCTION NAME: fnfGetCurrentRefillAmount
	 AUTHOR       : Sandra Peterson
	 DESCRIPTION  : Get the refill amount for the to be performed refill.
	 :
	 PARAMETERS   : Unicode string destination pointer, variable ID
	 RETURN       : length of unicode string, number of unicode chars
	 NOTES        :
	 ******************************************************************************/
	unsigned short fnfGetCurrentRefillAmount(void * pStrOrValue,
			unsigned short usVarID) {
		ushort usLen = 0;
		char tStr[30] = { 0 };
		unsigned char ucRefillAmt[SPARK_MONEY_SIZE];

		GetCurrencySymbol();
		if (bPlacement == BEFORE) {
			(void) strcpy(tStr, pCurrencySym);
			usLen = (unsigned short) strlen(tStr);
		}

		// changed the amount into a 5 byte value
		ucRefillAmt[0] = 0;
		(void) memcpy(&ucRefillAmt[1], m4IPSD_postageRequested, 4);

		// convert the amount from hex to ascii
		(void) fnMoney2Ascii(&usLen, tStr + usLen, ucRefillAmt);

		if (bPlacement == AFTER) {
			(void) strcat(tStr + usLen, pCurrencySym);
		}

		(void) unistrcpyFromCharStr((unichar *) (pStrOrValue), tStr);

		return ((unsigned short) strlen(tStr));
	}

	/******************************************************************************
	 FUNCTION NAME: fnfGetKeypadPhoneNumber
	 AUTHOR       : Sandra Peterson
	 DESCRIPTION  : Get the phone number used for keypad refills & withdrawals
	 :
	 PARAMETERS   : Unicode string destination pointer, variable ID
	 RETURN       : length of unicode string, number of unicode chars
	 NOTES        :
	 ******************************************************************************/
	unsigned short fnfGetKeypadPhoneNumber(void * pStrOrValue,
			unsigned short usVarID) {
#if !defined (K700) && !defined (G900)
		CMOSSetupParams.KeypadPhoneNumber[MAX_PHONE_LEN] = 0; // make sure null terminated
		(void)unistrcpyFromCharStr((unichar *)(pStrOrValue), CMOSSetupParams.KeypadPhoneNumber);
		return((unsigned short)strlen(CMOSSetupParams.KeypadPhoneNumber));
#else
		return (0);
#endif
	}

	/******************************************************************************
	 FUNCTION NAME: fnfGetWithdrawalPassword
	 AUTHOR       : Sandra Peterson
	 DESCRIPTION  : Get the password for keypad withdrawals
	 :
	 PARAMETERS   : Unicode string destination pointer, variable ID
	 RETURN       : length of unicode string, number of unicode chars
	 NOTES        :
	 ******************************************************************************/
	unsigned short fnfGetWithdrawalPassword(void * pStrOrValue,
			unsigned short usVarID) {
#if !defined (K700) && !defined (G900)
//  char cFakePassword[] = {'0', '5', '1', '3', '1', '9', '8', '3', 0};

//  (void)unistrcpyFromCharStr((unichar *)pStrOrValue, cFakePassword);
		fnGetMasterPassword((unichar *)pStrOrValue);
		return(unistrlen((unichar *)pStrOrValue));
#else
		return (0);
#endif
	}

	/******************************************************************************
	 FUNCTION NAME: fnPreRatesDnldReport
	 AUTHOR       : Sandra J Peterson
	 DESCRIPTION  : Setup Rates Download report data
	 PARAMETERS   : none
	 RETURN       : none
	 NOTES        : none
	 ******************************************************************************/
	void fnPreRatesDnldReport(void) {
		unsigned char bInx;
		unsigned char bLoopIdx;
		BOOL fFindMatch = FALSE;
		UINT16 wCountryCode = fnGetDefaultRatingCountryCode();

		(void) memset((unsigned char *) &sRatesRptData, 0,
				sizeof(sRatesRptData));

		for (bInx = 0; bInx < newRatesDownloadedIndex; bInx++) {
#ifdef G900 
			// Adam - fixed fraca GMSE00124752.
			//  For Non-US rate update, the actual new rate info is used.
			//
			if (wCountryCode == UNITEDSTATES) {
				fFindMatch = FALSE;

				for (bLoopIdx = 0; bLoopIdx < MAX_CMOS_RATE_COMPONENTS;
						bLoopIdx++) {
					// find the matched
					if (!strcmp(newRatesInfo[bInx].version,
							(char *) CMOSRateCompsMap.CMOSRateComponents[bLoopIdx].SWVersion)) {
						fFindMatch = TRUE;
						break;
					}
				}

				if (fFindMatch == TRUE) {
					(void) memcpy(
							sRatesRptData.sRecordData[sRatesRptData.bNumRecords].SWPartNO,
							CMOSRateCompsMap.CMOSRateComponents[bLoopIdx].SWPartNO,
							SIZE_CMOS_PARTNO);
				}
				// may use hard coded part number for US
				else {
					(void) memcpy(
							sRatesRptData.sRecordData[sRatesRptData.bNumRecords].SWPartNO,
							newRatesInfo[bInx].desc, SIZE_CMOS_PARTNO);
				}
			} else {
				(void) memcpy(
						sRatesRptData.sRecordData[sRatesRptData.bNumRecords].SWPartNO,
						newRatesInfo[bInx].desc, SIZE_CMOS_PARTNO);
			}
#else
			(void)memcpy(sRatesRptData.sRecordData[sRatesRptData.bNumRecords].SWPartNO,
					newRatesInfo[bInx].desc, SIZE_CMOS_PARTNO);
#endif

			(void) memcpy(
					sRatesRptData.sRecordData[sRatesRptData.bNumRecords].SWVersion,
					newRatesInfo[bInx].version, RDL_VERSION_LEN);

			(void) memcpy(
					sRatesRptData.sRecordData[sRatesRptData.bNumRecords].EffectiveDate,
					newRatesInfo[bInx].effectiveDate, SIZE_CMOS_EFFDATE);

			sRatesRptData.bNumRecords++;
		}
	}

	/******************************************************************************
	 FUNCTION NAME: fnPreRatesSumReport
	 AUTHOR       : Sandra J Peterson
	 DESCRIPTION  : Setup Rates Summary report data
	 PARAMETERS   : none
	 RETURN       : none
	 NOTES        : none
	 ******************************************************************************/
	void fnPreRatesSumReport(void) {
		unsigned char bInx;
		sCMOSRateComponents *pThisComponent = NULL_PTR;

		(void) memset((unsigned char *) &sRatesRptData, 0,
				sizeof(sRatesRptData));

		// First find all the rates modules...
		for (bInx = 0; bInx < MAX_CMOS_RATE_COMPONENTS; bInx++) {
			pThisComponent = &CMOSRateCompsMap.CMOSRateComponents[bInx];
			if ((pThisComponent->DeviceID == RATES_DATA_COMPONENT)
					&& (pThisComponent->SWPartNO[0] != 0)) {
				sRatesRptData.sRecordData[sRatesRptData.bNumRecords].bRateComponentActive =
						pThisComponent->bRateComponentActive;

				(void) memcpy(
						sRatesRptData.sRecordData[sRatesRptData.bNumRecords].SWPartNO,
						pThisComponent->SWPartNO, SIZE_CMOS_PARTNO);

				(void) memcpy(
						sRatesRptData.sRecordData[sRatesRptData.bNumRecords].SWVersion,
						pThisComponent->SWVersion, SIZE_CMOS_PARTNO);

				(void) memcpy(
						sRatesRptData.sRecordData[sRatesRptData.bNumRecords].EffectiveDate,
						pThisComponent->EffectiveDate, SIZE_CMOS_EFFDATE);

				sRatesRptData.bNumRecords++;
			}
		}

		// Next find all the DCAP modules...
		for (bInx = 0; bInx < MAX_CMOS_RATE_COMPONENTS; bInx++) {
			pThisComponent = &CMOSRateCompsMap.CMOSRateComponents[bInx];
			if ((pThisComponent->SWPartNO[0] != 0)
					&& ((pThisComponent->DeviceID == DCAP_DATA_COMPONENT)
							|| (pThisComponent->DeviceID
									== COMPRESSED_DCAP_DATA_COMPONENT))) {
				sRatesRptData.sRecordData[sRatesRptData.bNumRecords].bRateComponentActive =
						pThisComponent->bRateComponentActive;

				(void) memcpy(
						sRatesRptData.sRecordData[sRatesRptData.bNumRecords].SWPartNO,
						pThisComponent->SWPartNO, SIZE_CMOS_PARTNO);

				(void) memcpy(
						sRatesRptData.sRecordData[sRatesRptData.bNumRecords].SWVersion,
						pThisComponent->SWVersion, SIZE_CMOS_PARTNO);

				(void) memcpy(
						sRatesRptData.sRecordData[sRatesRptData.bNumRecords].EffectiveDate,
						pThisComponent->EffectiveDate, SIZE_CMOS_EFFDATE);

				sRatesRptData.bNumRecords++;
			}
		}
	}

	/******************************************************************************
	 FUNCTION NAME: fnfRatesRptVars
	 AUTHOR       : Sandra J Peterson
	 DESCRIPTION  : Get Rates report variables
	 PARAMETERS   : Standard report field function
	 RETURN       : number of unicode chars
	 NOTES        : none
	 ******************************************************************************/
	unsigned short fnfRatesRptVars(void * pStrOrValue, unsigned short usVarID) {
		DATETIME tempDateTime;
		unsigned short usLen = 0, usInx = 0, usTmp;
		char tStr[20] = { 0, 0 };
		BOOL fDoConvert = TRUE;

		switch (usVarID) {
		case RATES_DOWNLOAD_DATE :
			if (fnFormatDate(tStr, &newRatesLoadTime, TRUE) == SUCCESSFUL) {
				usLen = (UINT16) strlen(tStr);
			}
			break;

		case RATES_DOWNLOAD_TIME :
			if (fnFormatTime(tStr, &newRatesLoadTime, usVarID) == SUCCESSFUL) {
				usLen = (UINT16) strlen(tStr);
			}
			break;

		default:
			// determine what slot we are filling on the current page
			while ((rRatesVarId2RecordIndex[usInx][0] > 0)
					&& (rRatesVarId2RecordIndex[usInx][0] != usVarID)) {
				usInx++;
			}

			// determine the corresponding list index for the page we are on
			usTmp = (unsigned short) (((wCurrentPageNumber - 1)
					* FiveEntriesPerPage) + rRatesVarId2RecordIndex[usInx][1]);

			if (usTmp < sRatesRptData.bNumRecords) {
				switch (usVarID) {
				case GET_RATE_MODULE_NAME0 :
				case GET_RATE_MODULE_NAME1 :
				case GET_RATE_MODULE_NAME2 :
				case GET_RATE_MODULE_NAME3 :
				case GET_RATE_MODULE_NAME4 :
					(void) memcpy((unsigned char *) tStr,
							sRatesRptData.sRecordData[usTmp].SWPartNO,
							SIZE_CMOS_PARTNO);
					usLen = (UINT16) strlen(tStr);
					break;

				case GET_RATE_MODULE_VERSION0 :
				case GET_RATE_MODULE_VERSION1 :
				case GET_RATE_MODULE_VERSION2 :
				case GET_RATE_MODULE_VERSION3 :
				case GET_RATE_MODULE_VERSION4 :
					(void) memcpy((unsigned char *) tStr,
							sRatesRptData.sRecordData[usTmp].SWVersion,
							SIZE_CMOS_PARTNO);
					usLen = (UINT16) strlen(tStr);
					break;

				case GET_RATE_MODULE_DATE0 :
				case GET_RATE_MODULE_DATE1 :
				case GET_RATE_MODULE_DATE2 :
				case GET_RATE_MODULE_DATE3 :
				case GET_RATE_MODULE_DATE4 :
					(void) memset((unsigned char *) tStr, 0, 3);
					(void) strncpy(tStr,
							(char *) sRatesRptData.sRecordData[usTmp].EffectiveDate,
							2);
					tempDateTime.bCentury = (UINT8) atoi(tStr);

					(void) strncpy(tStr,
							(char *) &sRatesRptData.sRecordData[usTmp].EffectiveDate[2],
							2);
					tempDateTime.bYear = (UINT8) atoi(tStr);

					(void) strncpy(tStr,
							(char *) &sRatesRptData.sRecordData[usTmp].EffectiveDate[4],
							2);
					tempDateTime.bMonth = (UINT8) atoi(tStr);

					(void) strncpy(tStr,
							(char *) &sRatesRptData.sRecordData[usTmp].EffectiveDate[6],
							2);
					tempDateTime.bDay = (UINT8) atoi(tStr);

					if (fnFormatDate(tStr, &tempDateTime, TRUE) == SUCCESSFUL) {
						usLen = (UINT16) strlen(tStr);
					}
					break;

				case GET_RATE_MODULE_ACTIVE0 :
				case GET_RATE_MODULE_ACTIVE1 :
				case GET_RATE_MODULE_ACTIVE2 :
				case GET_RATE_MODULE_ACTIVE3 :
				case GET_RATE_MODULE_ACTIVE4 :
					if (sRatesRptData.sRecordData[usTmp].bRateComponentActive
							== TRUE)
						GetNativeWord(pStrOrValue, NATIVE_YES);
					else
						GetNativeWord(pStrOrValue, NATIVE_NO);

					usLen = unistrlen(pStrOrValue);
					fDoConvert = FALSE;
					break;

				default:
					break;
				}
			}
			break;
		}

		if (usLen && (fDoConvert == TRUE)) {
			(void) unistrcpyFromCharStr(pStrOrValue, tStr);
		}

		return (usLen);
	}

	/******************************************************************************
	 FUNCTION NAME: fnfGetCachedPieceCount
	 AUTHOR       : Craig DeFilippo
	 DESCRIPTION  : Format all kinds of piece counts for human readable indicia
	 : and reports.
	 PARAMETERS   : string destination pointer, variable ID
	 RETURN       : length of string, number of chars
	 NOTES        : None
	 ******************************************************************************/
	unsigned short fnfGetCachedPieceCount(void * pStrOrValue,
			unsigned short usVarID) {
		char tStr[16] = { 0, 0 };
		ulong ulPc = 0;
		ushort usLen;

#if defined (K700) || defined (G900)
		unsigned long ulSomReportDebits = 0;
#endif

		switch (usVarID) {
		case NON_ZERO_PIECE_COUNT : //same as derived piece count, set BP_NO_ZPRINTS_IN_PCNT to 1 to get non-zero count
			if (fnValidData(BOBAMAT0, GET_LOCAL_PIECE_COUNT, &ulPc) != BOB_OK) {
				fnProcessSCMError();
			}
			break;

		case LOCAL_ZERO_PIECE_COUNT :
			if (fnValidData(BOBAMAT0, GET_LOCAL_ZERO_PIECE_COUNT, &ulPc)
					!= BOB_OK) {
				fnProcessSCMError();
			}
			break;

		case LOCAL_TOTAL_PIECE_COUNT :
			if (fnValidData(BOBAMAT0, GET_LOCAL_TOTAL_PIECE_COUNT, &ulPc)
					!= BOB_OK) {
				fnProcessSCMError();
			} else {
				ulPc -= ulSomReportDebits;
			}
			break;

		default:
			break;
		}

		(void) sprintf(tStr, "%d", ulPc);
		usLen = (unsigned short) strlen(tStr);
		(void) unistrcpyFromCharStr((unichar *) (pStrOrValue), tStr);

		return (usLen);
	}

	/******************************************************************************
	 FUNCTION NAME: fnfGetCachedPieceCountAscii
	 AUTHOR       : Craig DeFilippo
	 DESCRIPTION  : Format all kinds of piece counts for human readable indicia
	 : and reports, in ASCII.
	 PARAMETERS   : string destination pointer, variable ID
	 RETURN       : length of string, number of chars
	 NOTES        : intended for building barcode data
	 ******************************************************************************/
	unsigned short fnfGetCachedPieceCountAscii(void * pStrOrValue,
			unsigned short usVarID) {
		char tStr[16] = { 0, 0 };
		ulong ulPc = 0;
		ushort usLen;

#if defined (K700) || defined (G900)
		unsigned long ulSomReportDebits = 0;
#endif

		switch (usVarID) {
		case NON_ZERO_PIECE_COUNT : //same as derived piece count, set BP_NO_ZPRINTS_IN_PCNT to 1 to get non-zero count
			if (fnValidData(BOBAMAT0, GET_LOCAL_PIECE_COUNT, &ulPc) != BOB_OK) {
				fnProcessSCMError();
			}
			break;

		case LOCAL_ZERO_PIECE_COUNT :
			if (fnValidData(BOBAMAT0, GET_LOCAL_ZERO_PIECE_COUNT, &ulPc)
					!= BOB_OK) {
				fnProcessSCMError();
			}
			break;

		case LOCAL_TOTAL_PIECE_COUNT :
			if (fnValidData(BOBAMAT0, GET_LOCAL_TOTAL_PIECE_COUNT, &ulPc)
					!= BOB_OK) {
				fnProcessSCMError();
			} else {
				ulPc -= ulSomReportDebits;
			}
			break;

		default:
			break;
		}

		(void) sprintf(tStr, "%d", ulPc);
		usLen = (unsigned short) strlen(tStr);
		(void) strcpy((char*) pStrOrValue, tStr);

		return (usLen);
	}

	/******************************************************************************
	 FUNCTION NAME: fnfGetPreDebitItems
	 AUTHOR       : Sandra J Peterson
	 DESCRIPTION  : Get pre-debit items for G900 from the current value of the
	 variables instead of from the debit certificate. This is only
	 for data that doesn't change during a mailrun or isn't only
	 available in the debit certificate.
	 PARAMETERS   : Standard report field function
	 RETURN       : number of unicode chars
	 NOTES        : none
	 MODIFICATION HISTORY:
	 2012.08.14 Liu Jupeng     Merged the fixing issue from the branch fphx02.08_c1_secap.
	 2011.08.21 Deborah Kohl FPHX - 02.08
	 - Changed function fnfGetPreDebitItems() to print the batch count corresponding
	 to the selected permit when we are in permit mode (before we were always printing the indicia
	 batch count)
	 - Changed functions fnDetermineTotalPages(), fnfPermitRptVars(), added fnGetPermitIndex(), PermitVarId2RecordIndex[]
	 to support printing of permit report on multiple pages
	 ******************************************************************************/
	unsigned short fnfGetPreDebitItems(void * pStrOrValue,
			unsigned short usVarID) {
		DATETIME tempDateTime;
		UINT32 ulPieceID;
		unsigned short usLen = 0;
		unichar uniString[20];
		UINT8 i;
		char cString[20] = { 0 };    //Default initialize to empty string.
		BOOL fDoConvert = FALSE;

		switch (usVarID) {
#ifdef G900
		case INDICIA_DEF_REC_CAN_POSTAGE_ID:
			fDoConvert = FALSE;
			memset(uniString, 0, sizeof(uniString));
			if (fnGetUnicodeReportData(BOBAMAT0, VAULT_DEBIT_VALUE, uniString)
					== BOB_OK) {
				unsigned short usTemp;
				unsigned char ucInx1, ucInx2 = 0;
				IPSD_DEF_REC rFieldRec;

				//get the IBI record from flash
				if (fnFlashGetIBPRecVar(&rFieldRec, IPB_GET_INDICIA_DEF_REC,
						usVarID)) {
					if ((rFieldRec.ucFieldOp == DRFOP_BE_MON_2DEC)
							|| (rFieldRec.ucFieldOp == DRFOP_LE_MON_2DEC))
// !!                    if(   (rFieldRec.ucFieldOp == MON_BE_2DEC) 
// !!                       || (rFieldRec.ucFieldOp == MON_LE_2DEC) )
							{
						// remove the 3rd decimal digit
						usLen = unistrlen(uniString) - 1;
					}

					// get the number of postage digits
					usTemp = fnFlashGetByteParm(BP_MIN_PRINTED_INTEGERS)
							+ fnFlashGetByteParm(BP_MIN_PRINTED_DECIMALS);

					// pad w/ Unicode zeros on the front for any missing digits
					if (usTemp > usLen) {
						usTemp -= usLen;
						while (usTemp) {
							((unichar *) pStrOrValue)[ucInx2++] = 0x0030;
							usTemp--;
						}
					}

					// make sure there is no decimal separator in the value
					for (ucInx1 = 0; ucInx1 < usLen; ucInx1++) {
						if ((uniString[ucInx1] != '.')
								&& (uniString[ucInx1] != ',')) {
							((unichar *) pStrOrValue)[ucInx2++] =
									uniString[ucInx1];
						}
					}

					((unichar *) pStrOrValue)[ucInx2] = 0; // put in the null terminator
					usLen = ucInx2;
				}
			} else {
				fnProcessSCMError();
			}
			break;

		case INDICIA_DEF_REC_CAN_CREAT_DATE_ID:
			if (GetSysDateTime(&tempDateTime, ADDOFFSETS,
					GREGORIAN) == SUCCESSFUL) {
				cString[0] = (char) ((tempDateTime.bMonth / 10) + 0x30);
				cString[1] = (char) ((tempDateTime.bMonth % 10) + 0x30);
				cString[2] = (char) ((tempDateTime.bDay / 10) + 0x30);
				cString[3] = (char) ((tempDateTime.bDay % 10) + 0x30);
				cString[4] = 0;
				usLen = 4;
				fDoConvert = TRUE;
			}
			break;

		case INDICIA_DEF_REC_CAN_MAIL_DATE_ID:
			if (GetPrintedDate(&tempDateTime, GREGORIAN) == SUCCESSFUL) {
				if (fnFormatIndiciaDate(cString, &tempDateTime) == SUCCESSFUL) {
					fDoConvert = TRUE;
				}
			}
			break;
#endif

		case INDICIA_GER_PIECE_ID :
#ifdef G900
			if (fnOITGetPrintMode() == PMODE_PERMIT)
#else
			if (fnOITGetJanusPrintMode() == PMODE_PERMIT)
#endif
			{
				unichar sPermitName[MAX_PERMIT_UNICHARS + 1];
				ulong ulPermitBatchCount = 0;

				memset(sPermitName, 0, sizeof(sPermitName));
				if (fnValidData(BOBAMAT0, READ_CURRENT_PERMIT_NAME, sPermitName)
						== BOB_OK) {
					// read permit batch count based on permit name
					if (GetPermitDataByName(sPermitName, &ulPermitBatchCount)) {
						// increment the batch count since permit batch counters don't
						//  get incremented until AFTER the print
						ulPermitBatchCount++;

						sprintf(cString, "%010u", ulPermitBatchCount);
					} else {
						for (i = 0; i < 10; i++) {
							cString[i] = 0x20;
						}
					}

					cString[10] = 0;
					fDoConvert = TRUE;
				}
			} else {
				for (i = 0; i < 10; i++) {
					cString[i] = 0x20;
				}

				cString[10] = 0;
				fDoConvert = TRUE;
			}
			break;

		case UK_INDICIA_SERIAL_NUMBER:
			(void) fnfFlexDebitIndiaPostAscii(cString, usVarID);
			fDoConvert = TRUE;
			break;

		default:
			break;
		}

		if (fDoConvert == TRUE) {
			(void) unistrcpyFromCharStr(pStrOrValue, cString);
			usLen = (UINT16) strlen(cString);
		}

		return (usLen);
	}

	/******************************************************************************
	 FUNCTION NAME: fnfDCAPStringsAscii
	 AUTHOR       : Craig DeFilippo
	 DESCRIPTION  : Get the ASCII DCAP information strings for the current rating
	 : configuration.
	 PARAMETERS   : unichar string destination pointer, variable ID
	 RETURN       : length of unichar string, number of unichars
	 NOTES        : intended for building barcode data
	 ******************************************************************************/
	unsigned short fnfDCAPStringsAscii(void * pStrOrValue,
			unsigned short usVarID) {
		ushort usLen = 0;

#if !defined (K700) && !defined (G900)
		char *pDCAPStr = NULL;

		switch (usVarID)
		{
			case FIRST_DCAP_PRINTER_STRING:
			//TODO JAH Remove dcapi.c pDCAPStr = fnDCAPGetProductDescriptionLine(0);
			break;

			case SECOND_DCAP_PRINTER_STRING:
			//TODO JAH Remove dcapi.c pDCAPStr = fnDCAPGetProductDescriptionLine(1);
			break;

			default:
			break;
		}

		if (pDCAPStr != NULL)
		{
			usLen = (unsigned short)strlen(pDCAPStr);
			(void)strcpy((char*)pStrOrValue,pDCAPStr);
		}

#endif
		return (usLen);
	}

	/******************************************************************************
	 FUNCTION NAME: fnfGetBobVariableByteArray
	 AUTHOR       : Craig DeFilippo
	 DESCRIPTION  : Interpret usVarID as a BOB variable ID.  Get that variable,
	 : treat it as uchar byte array, and copy it verbatim to
	 : pStrOrValue.
	 PARAMETERS   : string destination pointer, variable ID
	 RETURN       : number of bytes copied to dest
	 NOTES         : !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	 : This was an architectural mistake
	 Read this--> : Don't use this function ever again.
	 : use fnfGetGlob2BobVariableByteArray instead to eliminate
	 : any dependency on the absolute value of BOB ID's
	 : !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	 ******************************************************************************/
	unsigned short fnfGetBobVariableByteArray(void * pStrOrValue,
			unsigned short usVarID) {
//    ushort  usLen = 0;

//    if (usVarID < INVALID_VARIABLE_NAME)
//    { 
//        (void)memcpy((uchar*) pStrOrValue, (uchar*) bobaVarAddresses[usVarID].addr, (ushort)bobaVarAddresses[usVarID].siz);
//        usLen = (ushort)bobaVarAddresses[usVarID].siz;
//    }
		return (fnfGetGlob2BobVariableByteArray(pStrOrValue, usVarID));
	}

	/******************************************************************************
	 FUNCTION NAME: fnfGetGlob2BobVariableByteArray
	 AUTHOR       : Craig DeFilippo
	 DESCRIPTION  : Use the glob2bob cross reference table (pMapGlobVarToBobVar)
	 : to get that Bob variable, treat it as uchar byte array,
	 : and copy it verbatim to pStrOrValue.
	 PARAMETERS   : string destination pointer, variable ID
	 RETURN       : number of bytes copied to dest
	 NOTES         : intended for building barcode data
	 ******************************************************************************/
	unsigned short fnfGetGlob2BobVariableByteArray(void * pStrOrValue,
			unsigned short usVarID) {
		extern const tG2B2GIDmap pMapGlobVarToBobVar[];
		extern ushort usGlob2BobArrayLen;

		const tG2B2GIDmap *pGlob2Var;
		UINT16 usLen = 0;
		UINT16 i;
		UINT16 usGlobID;
		UINT16 usBobID;

		for (i = 0; i < usGlob2BobArrayLen; i++) {
			pGlob2Var = &pMapGlobVarToBobVar[i];
			usGlobID = (pGlob2Var->pGlobVarID[0] << 8)
					+ pGlob2Var->pGlobVarID[1];

			if ((usGlobID == usVarID)) {
				usBobID = (pGlob2Var->pBobVarID[0] << 8)
						+ pGlob2Var->pBobVarID[1];
				switch (usBobID) {
				case BOBID_IPSD_INDICIADATAGLOB:
					usLen = wIPSD_barcodeDataGlobSize;
					break;

				case BOBID_IPSD_FLEXDEBIT_DATAGLOB:
					usLen = wIPSD_flexDebitDataSize;
					break;

				default:
					usLen = bobaVarAddresses[usBobID].siz;
					break;
				}
				EndianAwareCopy(pStrOrValue, bobaVarAddresses[usBobID].addr,usLen);
				break;
			}
		}

		return (usLen);
	}

	/******************************************************************************
	 FUNCTION NAME: fnfGetSOMInfo
	 AUTHOR       : Sandra Peterson
	 DESCRIPTION  : Retrieve the data for the Statement of Mailing report
	 PARAMETERS   : Unicode string destination pointer, variable ID
	 RETURN       : length of unicode string, number of unicode chars
	 NOTES        : None
	 ******************************************************************************/
	unsigned short fnfGetSOMInfo(void * pStrOrValue, unsigned short usVarID) {
#if !defined(G900) && !defined(K700)
		ushort usLen = 0, usSomId;
		double dblTemp;
		ulong ulTemp;
		char tmpStr[30];         // temporary container for a string value
		BOOL bDoCopy = TRUE;
		SOMBatch *ptrSOMBatch = NULL;

		(void)memset(tmpStr, 0, sizeof(tmpStr));

		usSomId = fnSOMGetReportID();
		if (fnSOMGetInfo(SOM_BY_SOMNUM , usSomId, &ptrSOMBatch) == SUCCESS)//lint !e605
		{
			switch (usVarID)
			{
				case SOM_RPT_NUMBER:
				(void)sprintf(tmpStr, "%04u", usSomId);
				break;

				case SOM_RPT_DATE:
				(void)fnFormatDate(tmpStr, &ptrSOMBatch->dtSOMDate, TRUE);
				break;

				case SOM_AR:
				dblTemp = fnBinFive2Double(ptrSOMBatch->bAR);
				usLen = fnFormatRptDoubleMoney(pStrOrValue, dblTemp, REPORT_FIELDS, TRUE);
				bDoCopy = FALSE;
				break;

				case SOM_DR:
				(void)memcpy(&ulTemp, ptrSOMBatch->bDR, sizeof(ulTemp));
				dblTemp = (double) ulTemp;
				usLen = fnFormatRptDoubleMoney(pStrOrValue, dblTemp, REPORT_FIELDS, TRUE);
				bDoCopy = FALSE;
				break;

				case SOM_PC:
				(void)sprintf(tmpStr, "%u", ptrSOMBatch->ulPieceCount);
				break;

				case SOM_BV:
				dblTemp = (double) ptrSOMBatch->ulBatchValue;
				usLen = fnFormatRptDoubleMoney(pStrOrValue, dblTemp, REPORT_FIELDS, TRUE);
				bDoCopy = FALSE;
				break;

				case SOM_BC:
				(void)sprintf(tmpStr, "%u", ptrSOMBatch->ulBatchCount);
				break;

				default:
				bDoCopy = FALSE;
				break;
			}
		}
		else
		{
			bDoCopy = FALSE;
		}

		if (bDoCopy)
		{
			usLen = (unsigned short)strlen(tmpStr);
			(void)unistrcpyFromCharStr(pStrOrValue, tmpStr);
		}

		return(usLen);
#else
		return (0);
#endif
	}

	/******************************************************************************
	 FUNCTION NAME: fnfGetNetSet2ReceiptInfo
	 AUTHOR       : Sandra Peterson
	 DESCRIPTION  : Get the data unique to the NetSet2 customer receipt
	 PARAMETERS   : None
	 RETURN       : None
	 NOTES        : None
	 ******************************************************************************/
	unsigned short fnfGetNetSet2ReceiptInfo(void * pStrOrValue,
			unsigned short usVarID) {
		ushort usLen = 0;

		return (usLen);
	}

	/******************************************************************************
	 This is a special class of formating functions used for builing the
	 Flex Debit Barcode Output from the flex debit barcode input.  These
	 functions have priviledged access to BOB's private variables.
	 ******************************************************************************/

	/******************************************************************************
	 FUNCTION NAME: fnfReadPSDParmsAscii
	 AUTHOR       : Craig DeFilippo
	 DESCRIPTION  : Read a parameter from the IPB_PSD_PARM_DEF_REC and convert
	 : it to ascii.
	 PARAMETERS   : Ascii string destination pointer, variable ID
	 RETURN       : length of ascii string, number of ascii chars
	 NOTES        : None
	 ******************************************************************************/
	unsigned short fnfReadPSDParmsAscii(void * pStrOrValue,
			unsigned short usVarID) {
		IPSD_DEF_REC rFieldRec;
		char* pData;
		ushort usLen = 0;
		uchar ucConvType;

		//get the PSD record from flash
		if (fnFlashGetIBPRecVar(&rFieldRec, IPB_PSD_PARM_DEF_REC, usVarID)) {
			pData = (char*) pIPSD_paramListGlob;     //aPSDParam;

			if (fnFindConvType(rFieldRec.usFieldLen, rFieldRec.ucFieldOp,
					DRFOP_ASKII, &ucConvType))
// !!        if( fnFindConvType(rFieldRec.usFieldLen, rFieldRec.ucFieldOp, ASKII, &ucConvType) )
					{
				if (fnPleaseConvertThis2Ascii(pData + rFieldRec.usFieldOff,
						rFieldRec.usFieldLen, ucConvType, pStrOrValue)) {
					usLen = (unsigned short) strlen(pStrOrValue);
				}
			}
		}

		return (usLen);
	}

	/******************************************************************************
	 FUNCTION NAME: fnfReadFlexDebitParms
	 AUTHOR       : Sam Thillaikumaran
	 DESCRIPTION  : Read a parameter from the IPB_CREATE_FLEXDEBIT_DEF_REC and convert
	 : it to unicode. Use the pIPSD_flexDebitDataGlob as data source.
	 PARAMETERS   : Unicode string destination pointer, variable ID
	 RETURN       : length of unicode string, number of unicode chars
	 NOTES        : None
	 ******************************************************************************/
	unsigned short fnfReadFlexDebitParms(void * pStrOrValue,
			unsigned short usVarID) {
		IPSD_FLEXDEBIT_DEF_REC rFieldRec;
		char* pData;
		ushort usLen = 0;
		uchar ucConvType;

		//get the IBI record from flash
		if (fnFlashGetIBPBcDefRecVar(&rFieldRec, IPB_CREATE_FLEXDEBIT_DEF_REC,
				usVarID)) {
			pData = (char*) pIPSD_flexDebitDataGlob;
			if (fnFindConvType(rFieldRec.usFieldLen, rFieldRec.ucFieldOp,
					DRFOP_ASKII, &ucConvType)) {
				usLen = fnPleaseConvertThis2Unicode(
						pData + rFieldRec.usFieldOff, rFieldRec.usFieldLen,
						ucConvType, pStrOrValue);
			}
		}

		return (usLen);
	}

//******************************************************************************
//   FUNCTION NAME: fnfAdjustRegisterForFlexDebitAscii
//   AUTHOR       : Clarisa Bellamy
//
//   DESCRIPTION  : Get a register value from Bob, adjust for what the value will
//                  be after a successful debit, then convert to ASCII.
//   PARAMETERS   : ACII string destination pointer, variable ID
//   RETURN       : length of ascii string, number of ascii chars 
//   NOTES        : None
// ----------------------------------------------------------------------------
	UINT16 fnfAdjustRegisterForFlexDebitAscii(void * pStrOrValue,
			UINT16 usVarID) {
		double dblTemp;
		ulong ulTemp1;
		ulong ulTemp2;
		ushort usLen = 0;
		char tmpStr[30];

#if defined (K700) || defined (G900)
		unsigned long ulSomReportDebits = 0;
#endif

		switch (usVarID) {
		// Get the current Ascending Register, add the postage, then convert to
		// ASCII string.
		case GLOBID_PREINC_AR:
			dblTemp = fnBinFive2Double(m5IPSD_ascReg);
			(void) memcpy(&ulTemp1, bobsPostageValue, sizeof(ulTemp1));
			dblTemp += ulTemp1;
			(void) sprintf(pStrOrValue, "%.0f", dblTemp);
			break;

			// Get the current Descending Register, subtract the postage, then
			// convert to ASCII string.
		case GLOBID_PREDEC_DR:
			(void) memcpy(&ulTemp1, m4IPSD_descReg, sizeof(ulTemp1));
			(void) memcpy(&ulTemp2, bobsPostageValue, sizeof(ulTemp2));
			ulTemp1 -= ulTemp2;
			(void) sprintf(pStrOrValue, "%u", ulTemp1);
			break;

			// Get the current piece count, increment, and convert to ASCII.
		case GLOBID_PREINC_PC:
			// the piece count shell game, watch closely
			if (fnFlashGetByteParm(BP_NO_ZPRINTS_IN_PCNT) == 0) {
				//for most markets the derived pc is the same as the total pc
				(void) memcpy((void *) &ulTemp1, pIPSD_pieceCount, 4);
				ulTemp1 -= ulSomReportDebits;
			} else {
				//master chocolatiers and watchmakers prefer derived pc = total pc - zero pc
				unsigned long ulTotalPc, ulZeroPc;

				(void) memcpy((void *) &ulTotalPc, pIPSD_pieceCount, 4);
				(void) memcpy((void *) &ulZeroPc, pIPSD_ZeroPostagePieceCount,
						4);
				(void) memcpy((void *) &ulTemp1, bobsPostageValue,
						sizeof(ulTemp1));

				// if the postage is zero, we want to make it look like the piece count
				// isn't incrementing, so we have to decrement by 1 to compensate for the
				// increment later.
				if (!ulTemp1) {
					ulTemp1 = (ulTotalPc - ulZeroPc) - 1;
				} else {
					ulTemp1 = ulTotalPc - ulZeroPc;
				}
			}

			ulTemp1++;
			(void) sprintf(pStrOrValue, "%u", ulTemp1);
			break;

		default:
			*(UINT16 *) pStrOrValue = 0;
			break;
		}

		usLen = (UINT16) strlen(pStrOrValue);
		return (usLen);
	}

//******************************************************************************
//   FUNCTION NAME: fnfMailingDateFlex
//   AUTHOR       : Clarisa Bellamy
//
//   DESCRIPTION  : Uses the mailing date saved by bob in the LoadIndicia function
//                  and formats it according to the variable ID.
//   PARAMETERS   : pStrOrValue - ACII string OR unicode destination pointer 
//                  variable ID - indicates what the data is used for.
//   RETURN       : length of ascii string, number of ascii chars 
//   NOTES        : 
//      This works because the DATETIME structure msgPrintTime is loaded when the
//      fnIPSDLoadCreateIndiciaData function calls fnMsgDateTime( IPSD_PRECREATE_INDICIUM ),
//      AND because the Flex PreCreate Indicia and Flex Create Indicia messages are 
//      NOT in the WhenAmI table.  If they were, the msgPrintTime structure would
//      be updated, and possibly changed, BETWEEN the loading of the Barcode date and 
//      the loading of the Human Readable date. 
//       
// ----------------------------------------------------------------------------
	UINT16 fnfMailingDateFlex(void * pStrOrValue, UINT16 usVarID) {
		const DATETIME * pBCPrintedDateTime;
		UINT16 usLen = 0;
		UINT8 ubFormatOption = 0;
		BOOL bDoCopy = FALSE;
		char tmpStr[MAX_RESULT_STRING + 1] = { 0 }; // temporary container for a string value

		// Get a pointer to the mailing date.
		pBCPrintedDateTime = &msgPrintTime;

		// IF the variable ID is listed, convert the mailing date and store it.
		switch (usVarID) {
		// Put the Mailing Date into the Barcode, in ASCII, without date separators,
		//  in the format specified in the I-button byte parameter for the Flex
		// Barcode data
		case GLOBID_BC_MAILDATE_ASCII:
			ubFormatOption = fnFlashGetIBByteParm(
					IBP_FLEX_DEBIT_BC_DATE_FORMAT);
			if (fnFormatDateASCII(pStrOrValue, pBCPrintedDateTime,
					ubFormatOption,
					NO_SEPARATOR, NO_SEPARATOR) == SUCCESSFUL) {
				usLen = (UINT16) strlen(pStrOrValue);
			}
			break;

			// This is the Human Readable one:      Convert it to Unicode using the
			//  format parameter for the indicia date, without date separators.
		case INDICIA_DATE_WITHOUT_DATE_SEP :
			ubFormatOption = fnFlashGetByteParm(BP_INDICIA_DATE_FORMAT);
			if (fnFormatDateASCII(tmpStr, pBCPrintedDateTime, ubFormatOption,
			NO_SEPARATOR, NO_SEPARATOR) == SUCCESSFUL) {
				bDoCopy = TRUE;
			}
			break;

		default:    // Nothing to do, just satisfies lint.
			break;
		}

		if (bDoCopy) {
			usLen = (UINT16) strlen(tmpStr);
			(void) unistrcpyFromCharStr(pStrOrValue, tmpStr);
		}

		return (usLen);
	}

//******************************************************************************
//   FUNCTION NAME: fnfFlexDebitBrazilPostAscii
//   AUTHOR       : Clarisa Bellamy
//
//   DESCRIPTION  : Retrieve the data for the Brazil Flex Debit Data and convert
//                    it to ascii.
//   PARAMETERS   : ACII string destination pointer, variable ID
//   RETURN       : length of ascii string, number of ascii chars 
//   NOTES        : None
// HISTORY:
//   2008.04.25  Clarisa Bellamy - Fix bug that can occur on Brazil, at the DH 
//    cross-over time.  When retrieving the time for the flex debit, save it in 
//    DATETIME format for use by the function that does the DH calculation later.
//
// ----------------------------------------------------------------------------
	UINT16 fnfFlexDebitBrazilPostAscii(void * pStrOrValue, UINT16 usVarID) {
		UINT8 *pTmpStr = NULL_PTR;
		UINT32 ulTemp1;
		UINT16 usLen = 0;
		char tmpStr[MAX_RESULT_STRING + 1]; // temporary container for a string value
		UINT8 ucNumDigits;
		BOOL bDoCopy = FALSE;
		BOOL fGetLength = FALSE;

		(void) memset(tmpStr, 0, sizeof(tmpStr));

		switch (usVarID) {
		// MCU Code (Customer Number) in Brazil.
		case CUSTOMERID_RRN :
			(void) strncpy(pStrOrValue, CMOSCustomerID_RRN, CUSTOMERID_RRN_SZ);
			*((UINT8 *) pStrOrValue + CUSTOMERID_RRN_SZ) = 0;
			fGetLength = TRUE;
			break;

			// Printed Indicia Number (a.k.a Late Registration or Brazil Enrollment number)
		case GLOBID_CMOSPRNTD_IND_NUM:
			(void) strcpy(pStrOrValue, "PB");
			(void) strncat(pStrOrValue, CMOSPrintedIndiciaNum,
					PRINTED_INDICIA_NUM_SZ);
			usLen = (UINT16) strlen(pStrOrValue);
			break;

			// PBP Account Number.
		case PBP_ACCOUNT_NUMBER :
			pTmpStr = fnfGetCMOSPackedByteParam(CMOS_PB_PBP_ACCT_NBR);
			// Do check if CMOS Index is valid
			if (pTmpStr != NULL_PTR) {
				ucNumDigits = fnGetLengthOfPB(CMOS_PB_PBP_ACCT_NBR);
				bDoCopy = fnPleaseConvertThis2Ascii((char *) pTmpStr,
						(ucNumDigits + 1) / 2, PBCD_TO_STRING, tmpStr);
			}
			break;

			// Using this function ID with this Variable ID means it is in the Barcode.
		case INDICIA_TIME_WITHOUT_TIME_SEP :
			// For the time we use the "Current Time" (or runTime) instead of
			//  the mailing time.
			// For Brazil, we save the run time, so that later, when we create
			//  the HR portion of the indicia, this saved run-time can be
			//  compared with the DH time to determine whether to print "DH".
			if (GetSysDateTime(&stRunTime, ADDOFFSETS, GREGORIAN) == SUCCESSFUL) {
				if (fnFormatTime(pStrOrValue, &stRunTime, usVarID) == SUCCESSFUL) {
					usLen = (UINT16) strlen(pStrOrValue);
				}
			}
			// Successful or not, we are done here.
			break;

		case GLOBID_POSTAGEVAL:
		case POSTAGE_W_SIGN_WO_FIXED_ZEROS:
			// for the U.K. we need to remove the fixed zero from the postage value
			// when we get it for use in the 2D barcode, so we're using a different
			// VCR ID.  Aside from removing the fixed zero, all the other processing
			// is the same as for the GLOBID_POSTAGEVAL ID, so need to give this
			// specific ID to the Bob Task.
			(void) fnfGetGlob2BobVariableByteArray(&ulTemp1, GLOBID_POSTAGEVAL);

			if (usVarID == POSTAGE_W_SIGN_WO_FIXED_ZEROS) {
				ucNumDigits = fnFlashGetIBByteParm(IBP_FIXED_ZEROS);
				while (ucNumDigits > 0) {
					ulTemp1 /= 10;
					ucNumDigits--;
				}
			}

			(void) sprintf(pStrOrValue, "%u", ulTemp1);
			fGetLength = TRUE;
			break;

			// This is used to append the output of the flex debit to the input of the flexdebit,
			//  to create the modified barcode data glob.
			// In other words, it converts the MAC digits from 2 hex digits per byte to a string
			//  of ASCII characters that are the ASCII representation of each hex value, e.g.,
			//	0x4A72 is converted to "4A72".
		case GLOBID_IPSD_FLEXDEBIT_DESMAC:
		case GLOBID_IPSD_FLEXDEBIT_HMAC:
			// Retrieve the binary data.
			usLen = fnfGetGlob2BobVariableByteArray((uchar *) tmpStr, usVarID);

			// Convert the binary data to ASCII
			if (fnPleaseConvertThis2Ascii(tmpStr, usLen, PBCD_TO_STRING,
					pStrOrValue) != BOB_OK) {
				usLen = 0;
			} else {
				// since the conversion worked, the length of the string is > the number of MAC bytes,
				// so indicate that we need to get the new length.
				fGetLength = TRUE;
			}
			break;

		default:
			break;
		} // End of switch on usVarID

		if (bDoCopy) {
			usLen = (UINT16) strlen(tmpStr);
			(void) strcpy(pStrOrValue, tmpStr);
		}

		if (fGetLength) {
			usLen = (UINT16) strlen(pStrOrValue);
		}

		return (usLen);
	}

	/******************************************************************************
	 FUNCTION NAME: fnfFlexDebitIndiaPostAscii
	 AUTHOR       : Craig DeFilippo
	 DESCRIPTION  : Format data for India post indicia flex debit
	 PARAMETERS   : Ascii string destination pointer, variable ID
	 RETURN       : length of ascii string, number of ascii chars
	 NOTES         : has access to BOB's private variables
	 ******************************************************************************/
	unsigned short fnfFlexDebitIndiaPostAscii(void * pStrOrValue,
			unsigned short usVarID) {
		uchar *pTemp;
		ushort usLen = 0;
		ushort usTemp;
		uchar ucTemp, ucTemp2, ucTemp3 = 0;
		char tmpStr[30];
		BOOL bDoCopy = TRUE;
		UINT32 uiTemp;
		char pTempAscii[ BOBS_RATE_CLASS_SZ +1 ];


		(void) memset(tmpStr, 0, sizeof(tmpStr));

		switch (usVarID) {
		case GLOBID_NETSET_SECURITYCODEVER:
			pTemp = fnFlashGetAsciiStringParm(ASP_SECURITY_CODE_VERSION);
			if (pTemp && (strlen((const char *) pTemp) > 0)) {
				(void) strncpy(tmpStr, (char*) pTemp, sizeof(tmpStr));
			}
			break;

		case GLOBID_DATEOFMAILING:
			usTemp = fnDayOfYear((const DATETIME*) &msgPrintTime);
			ucTemp = msgPrintTime.bYear % 10;
			(void) sprintf(tmpStr, "%03u%u", usTemp, ucTemp);
			break;

		case GLOBID_PREINC_PC:
			bDoCopy = FALSE;
			usLen = fnfAdjustRegisterForFlexDebitAscii(pStrOrValue,
					GLOBID_PREINC_PC);
			break;

		case GLOBID_PREINC_AR:
			bDoCopy = FALSE;
			usLen = fnfAdjustRegisterForFlexDebitAscii(pStrOrValue,
					GLOBID_PREINC_AR);
			break;

		case GLOBID_POSTAGEVAL:
		case POSTAGE_W_SIGN_WO_FIXED_ZEROS:
			bDoCopy = FALSE;
			usLen = fnfFlexDebitBrazilPostAscii(pStrOrValue, usVarID);
			break;

		case GLOBID_PCL_PRODID:
			(void) strcpy( tmpStr, fnGetProdId());
			break;

		case PCL_SERVICE_ID_SINGLE_DIGITS:
			(void) memcpy(&ucTemp, fnfGetSelectedMiscData(0), sizeof(ucTemp));
      (void) memcpy(&ucTemp2, fnfGetSelectedMiscData(1), sizeof(ucTemp));
			(void) sprintf(tmpStr, "%c%c", ucTemp, ucTemp2);
			break;

		case GLOBID_HMACDESMAC_KEYID:
			(void) memcpy(&usTemp, pIPSD_HmacDesMacKeyID, sizeof(usTemp));
			(void) sprintf(tmpStr, "%u", usTemp);
			break;

		case UK_KEY_ID_SERIES:
			// convert the key ID number to a series of values that runs
			// as 0-9, then A-Z, then back to 0-9, etc.
			(void) memcpy(&usTemp, pIPSD_HmacDesMacKeyID, sizeof(usTemp));

			// reduce the number to the range of 0-35
			ucTemp = usTemp % 36;

			// if the number is in the range of 0-9, convert to ASCII '0' - '9'.
			// else, convert to ASCII 'A' - 'Z'.
			if (ucTemp < 10) {
				tmpStr[0] = (char) (ucTemp + '0');
			} else {
				tmpStr[0] = (char) (ucTemp - 10 + 'A');
			}

			tmpStr[1] = 0;		// nul terminate the string
			break;

		case UK_INDICIA_SERIAL_NUMBER:
			pTemp = fnFlashGetIBPackedByteParm(IPB_POSTAL_AUTH_CODE);
			if ((pTemp)
					&& (fnValidData(BOBAMAT0, VLT_INDI_FILE_INDICIA_SERIAL,
							(void *) tmpStr) == BOB_OK)) {
				// make sure it's properly nul-terminated
				tmpStr[IPSD_SZ_SN_INDICIA] = 0;

				// get the length of the serial number
				ucTemp2 = (unsigned char) strlen(tmpStr);

				for (ucTemp = 0; ucTemp < ucTemp2; ucTemp++) {
					// determine how many leading characters and/or leading zeros there are
					if ((tmpStr[ucTemp] >= 0x31) && (tmpStr[ucTemp] <= 0x39)) {
						break;
					}
				}

				if (ucTemp != ucTemp2) {
					if (ucTemp != 2) {
						// need to shift.
						// whether it's a shift right or a shift left will
						// automatically be determined by the value of ucTemp.
						(void) memmove(tmpStr + 2, tmpStr + ucTemp, ucTemp2);
					}

					// load the two leading characters
					(void) memcpy(tmpStr, pTemp, 2);

					// make sure the string is nul-terminated
					tmpStr[2 + ucTemp2 - ucTemp] = 0;
				} else {
					// couldn't find what I was looking for, so make tmpStr an empty string
					tmpStr[0] = 0;
				}
			}
			break;

		default:
			bDoCopy = FALSE;
			break;
		}

		if (bDoCopy) {
			usLen = (unsigned short) strlen(tmpStr);
			(void) strcpy(pStrOrValue, tmpStr);
		}

		return (usLen);
	}

	/******************************************************************************
	 FUNCTION NAME: fnfGetSwedenInfo
	 AUTHOR       : Sam Thillaikumaran
	 DESCRIPTION  : Format data for Sweden post indicia flex debit nad 2d Barcode.
	 PARAMETERS   : Ascii string destination pointer, variable ID
	 RETURN       : length of ascii string, number of ascii chars
	 NOTES         : has access to BOB's private variables
	 ******************************************************************************/
	unsigned short fnfGetSwedenInfo(void * pStrOrValue, unsigned short usVarID) {
		ushort usLen = 0;
		BOOL status = FALSE;
		char tmpStr[30];
		BOOL bDoCopy = FALSE;

		(void) memset(tmpStr, 0, sizeof(tmpStr));

		switch (usVarID) {
		case SWEDEN_PACKED_DATA_BEFORE_DEBIT :
			status = fnGetSwedenPackedData(pStrOrValue, TRUE);
			if (status == TRUE)
				usLen = 19;
			break;

		case SWEDEN_PACKED_DATA_AFTER_DEBIT :
			status = fnGetSwedenPackedData(pStrOrValue, FALSE);
			if (status == TRUE)
				usLen = 19;
			break;

		case SWEDEN_SECURITY_INFO_VER :
			//Currently returning zero length so the padding bytes will include binary 0.
			break;

		case SWEDEN_SECURITY_INFO :
			//Currently returning zero length so the padding bytes will include ascii '0's.
			break;

		case GLOBID_POSTAGEVAL:
			bDoCopy = fnPleaseConvertThis2Ascii((char *) bobsPostageValue,
					sizeof(bobsPostageValue), BE_INT_MONEY4_TO_STRING, tmpStr);
			break;

		default:
			break;
		}

		if (bDoCopy) {
			usLen = (UINT16) strlen(tmpStr);
			(void) unistrcpyFromCharStr(pStrOrValue, tmpStr);
		}
		return (usLen);
	}

	/******************************************************************************
	 FUNCTION NAME: fnfFlexDebitPhilippinesPostAscii
	 AUTHOR       : Sam Thillaikumaran
	 DESCRIPTION  : Format data for Philippines post indicia flex debit
	 PARAMETERS   : Ascii string destination pointer, variable ID
	 RETURN       : length of ascii string, number of ascii chars
	 NOTES         : has access to BOB's private variables
	 ******************************************************************************/
	unsigned short fnfFlexDebitPhilippinesPostAscii(void * pStrOrValue,
			unsigned short usVarID) {
		ushort usLen = 0;
		char tmpStr[30];
		BOOL bDoCopy = TRUE;

		switch (usVarID) {
		case POSTAL_SERIAL_NUMBER_ID:
			// can't use fnfZipCodeFieldData because it generates a unicode string
			// and we need an ASCII string
			(void) strcpy(tmpStr,
					(char *) &pIPSD_zipCode[POSTAL_SERIAL_NUMBER_OFFSET]);
			break;

		default:
			bDoCopy = FALSE;
			break;
		}

		if (bDoCopy) {
			usLen = (unsigned short) strlen(tmpStr);
			(void) strcpy((char *) pStrOrValue, tmpStr);
		}

		return (usLen);
	}

	/******************************************************************************
	 FUNCTION NAME: fnfGetUnipostaInfo
	 AUTHOR       : Sandra Peterson
	 DESCRIPTION  : Retrieve the data for the Uniposta indicia & reports.
	 On G9, it's used to retrieve the license Number for
	 Slovakia.
	 PARAMETERS   : Unicode string destination pointer, variable ID
	 RETURN       : length of unicode string, number of unicode chars
	 NOTES        : None
	 ******************************************************************************/
	unsigned short fnfGetUnipostaInfo(void * pStrOrValue,
			unsigned short usVarID) {
		ushort usLen = 0;
		char tmpStr[45];         // temporary container for a string value
		unsigned char tmpVal[45];
		BOOL bDoCopy = TRUE;

		(void) memset(tmpStr, 0, sizeof(tmpStr));

		switch (usVarID) {
#if defined(JANUS) && !defined(K700) && !defined(G900)
		case CUSTOMER_SUPPLIER_NAME:
		(void)strcpy(tmpStr, CMOSCustomerName);
		break;
#endif

		case CUSTOMERID_RRN :
			(void) strcpy(tmpStr, CMOSCustomerID_RRN);
			break;

			// This is used to get the HMAC output of the flex debit in Unicode so it can
			// be printed in the indicia.
			// In other words, it converts the MAC digits from 2 hex digits per byte to a string
			//  of Unicode characters that are the Unicode representation of each hex value.
		case GLOBID_IPSD_FLEXDEBIT_DESMAC:
		case GLOBID_IPSD_FLEXDEBIT_HMAC:
			// Retrieve the binary data.
			usLen = fnfGetGlob2BobVariableByteArray(tmpVal, usVarID);

			// Convert the binary data to ASCII, so it can be converted to Unicode at
			//	the end of this function
			if (fnPleaseConvertThis2Ascii((char *) tmpVal, usLen,
					PBCD_TO_STRING, tmpStr) != BOB_OK) {
				tmpStr[0] = 0;
			}
			break;

		default:
			bDoCopy = FALSE;
			break;
		}

		if (bDoCopy) {
			usLen = (unsigned short) strlen(tmpStr);
			(void) unistrcpyFromCharStr(pStrOrValue, tmpStr);
		}

		return (usLen);
	}

	/******************************************************************************
	 FUNCTION NAME: fnfGetTownName
	 DESCRIPTION  : Retrieve the town name for the indicia & reports
	 PARAMETERS   : Unicode string destination pointer, variable ID
	 RETURN       : length of unicode string
	 ******************************************************************************/
	unsigned short fnfGetTownName(void * pStrOrValue, unsigned short usVarID) {
		ushort usLen = 0;

		switch (usVarID) {
		case TOWN_NAME_1 :
			(void) unistrcpy(pStrOrValue, exp_mtr_cmos.town_name_1);
			usLen = unistrlen(exp_mtr_cmos.town_name_1);
			break;

		case TOWN_NAME_2 :
			(void) unistrcpy(pStrOrValue, exp_mtr_cmos.town_name_2);
			usLen = unistrlen(exp_mtr_cmos.town_name_2);
			break;

		default:
			break;
		}
		return (usLen);
	}
	/******************************************************************************
	 FUNCTION NAME: fnfGetExpressMeterBarcodeInfo
	 AUTHOR       : Simon Fox
	 DESCRIPTION  : Data for express meter debit and 2d Barcode.
	 PARAMETERS   : Destination pointer (usually an ASCII string as most barcode
	 info is in ASCII)
	 variable ID
	 RETURN       : Length of returned data, (usually the number of ASCII chars)
	 COMMENTS     : Only the code to retrieve DESMAC has been merged from Express
	 Meter
	 As DESMAC and HMAC share the same memory because the buffers
	 are declared in an union, DESMAC variable ID can be use to
	 retrieve HMAC (needed for UK)
	 ******************************************************************************/
	unsigned short fnfGetExpressMeterBarcodeInfo(void *pStrOrValue,
			unsigned short usVarID) {
		char *retVal = (char *) pStrOrValue;
		retVal[0] = '\0';

		switch (usVarID) {
		case GLOBID_IPSD_FLEXDEBIT_DESMAC:
			// Retrieve the binary data and return the length.  Don't convert
			// to ASCII as the DESMAC in the express meter barcode is in
			// binary.
			return fnfGetGlob2BobVariableByteArray((char *) pStrOrValue,
					usVarID);

		default:
			break;
		}

		return (unsigned short) strlen(retVal);
	}

	/******************************************************************************
	 FUNCTION NAME: fnfGetRefundReceiptInfo
	 AUTHOR       : Connie Carey
	 DESCRIPTION  : Data for the refund receipt report after a system withdraw.
	 PARAMETERS   : Destination pointer (usually an ASCII string as most barcode
	 info is in ASCII)
	 variable ID
	 RETURN       : Length of returned data, (usually the number of ASCII chars)
	 COMMENTS     :
	 ******************************************************************************/
	unsigned short fnfGetRefundReceiptInfo(void * pStrOrValue,
			unsigned short usVarID) {
		double dblCS, dblAR, dblDR;
		unsigned short usLen = 1;
		char pStr[4];

		switch (usVarID) {
		case WITHDRAW_ASR:
			/* initialize the field to all spaces */
			dblAR = fnBinFive2Double(CMOS2RefundData.rAR);
			usLen = fnFormatRptDoubleMoney(pStrOrValue, dblAR, REPORT_FIELDS,
					TRUE);
			break;

		case WITHDRAW_DSR:
			dblDR = fnBinFive2Double(CMOS2RefundData.rDR);
			/* convert it to Unicode */
			usLen = fnFormatRptDoubleMoney(pStrOrValue, dblDR, REPORT_FIELDS,
					TRUE);
			break;

		case WITHDRAW_PIECE_CNT:
			(void) sprintf(pStr, "%d", CMOS2RefundData.pCnt);
			usLen = (unsigned short) strlen(pStr);
			(void) unistrcpyFromCharStr((unichar *) (pStrOrValue), pStr);
			break;

		case WITHDRAW_CTRL_SUM:
			dblCS = fnBinFive2Double(CMOS2RefundData.cSum);
			usLen = fnFormatRptDoubleMoney(pStrOrValue, dblCS, REPORT_FIELDS,
					TRUE);
			break;

		case WITHDRAW_DIGI_SIGN:
			for (int i = 0; i < 128; i++)
				(void) unistrcpyFromCharStr((unichar*) pStrOrValue + i, " ");
			break;

		case WITHDRAW_PBP_ACCT:
			(void) unistrcpyFromCharStr(pStrOrValue, CMOS2RefundData.pAcct);
			usLen = (unsigned short) strlen(CMOS2RefundData.pAcct);
			break;

		default:
			break;
		}

		return usLen;
	}

	/******************************************************************************
	 FUNCTION NAME: fnfGetCSDIndiciaString
	 AUTHOR       : Ivan Le Goff
	 DESCRIPTION  : Retrieve the string from the rates Indicia Strings (CSD) .
	 :
	 PARAMETERS   : Unicode string destination pointer, variable ID
	 RETURN       : length of unicode string, number of unicode chars
	 NOTES        :
	 ******************************************************************************/
	unsigned short fnfGetCSDIndiciaString(void * pStrOrValue,
			unsigned short usVarID) {
		UINT16 usLength = 0;
		unichar *indiciaString = NULL_PTR;

		switch (usVarID) {
		case CSD_INDICIA_STRING_0:           // IndStr[0]
			indiciaString = fnfGetSelectedIndiciaString(0);
			break;
		case CSD_INDICIA_STRING_1:           // IndStr[1]
			indiciaString = fnfGetSelectedIndiciaString(1);
			break;
		case CSD_INDICIA_STRING_2:           // IndStr[2]
			indiciaString = fnfGetSelectedIndiciaString(2);
			break;
		case CSD_INDICIA_STRING_3:           // IndStr[3]
			indiciaString = fnfGetSelectedIndiciaString(3);
			break;
		case CSD_INDICIA_STRING_4:           // IndStr[4]
			indiciaString = fnfGetSelectedIndiciaString(4);
			break;
		case CSD_INDICIA_STRING_5:           // IndStr[5]
			indiciaString = fnfGetSelectedIndiciaString(5);
			break;
		case CSD_INDICIA_STRING_6:           // IndStr[6]
			indiciaString = fnfGetSelectedIndiciaString(6);
			break;
		case CSD_INDICIA_STRING_7:           // IndStr[7]
			indiciaString = fnfGetSelectedIndiciaString(7);
			break;
		case CSD_INDICIA_STRING_8:           // IndStr[8]
			indiciaString = fnfGetSelectedIndiciaString(8);
			break;
		case CSD_INDICIA_STRING_9:           // IndStr[9]
			indiciaString = fnfGetSelectedIndiciaString(9);
			break;
		default:
			break;
		}

//  Get the length of the selected indicia string and copy to output buffer
		usLength = unistrlen(indiciaString);

		memcpy(pStrOrValue, indiciaString, (usLength * sizeof(unichar)));

		return (usLength);
	}

	/*****************************************************************************
	 End Flex debit functions, print head report functions should be placed
	 above this
	 *****************************************************************************/
