//=====================================================================
//
//	FileName:	HAL_Panel.c
//
//	Description: Hardware abstraction layer implementation for Panel LED & key interface
//=====================================================================
#include "commontypes.h"
#include "stdio.h"
#include "stddef.h"
#include "hal.h"
#include "am335x_pbdefs.h"
#include "HAL_Panel.h"
#include "nucleus_gen_cfg.h"

extern void HALInitGPIOPins(P_GPIO_INFO pGPIOTable, unsigned numPinsUsed);
extern void setGPIOPin(GPIO_INFO pinInfo, unsigned char value);
extern void setGPIOPinHigh(GPIO_INFO pinInfo);
extern void setGPIOPinLow(GPIO_INFO pinInfo);
extern unsigned char readGPIOPin(GPIO_INFO pinInfo);

// Global variables

// The order of the entries in the following table must match the order of the entries in
// the GPIO_INFO enum in HAL_Panel.h
// After a reset, a pin configured for output will output a 0.


// CSD Panel programming - pins should match schematic
static GPIO_INFO CSD_GPIO_Table[] = {
		{AM335X_CTRL_PADCONF_MCASP0_ACLKR, AM335X_GPIO_BIT_18, GPIO_BANK_3, DIR_INPUT, GPIO_PULL_UP}, // PANEL_POWER_KEY
		{AM335X_CTRL_PADCONF_MII1_RXD3, AM335X_GPIO_BIT_18, GPIO_BANK_2, DIR_OUTPUT, GPIO_PULL_OFF}, // PANEL_LED0_CPU
		{AM335X_CTRL_PADCONF_MII1_RXD2, AM335X_GPIO_BIT_19, GPIO_BANK_2, DIR_OUTPUT, GPIO_PULL_OFF} // PANEL_LED1_CPU
};
static const unsigned numPinsUsed = sizeof(CSD_GPIO_Table) / sizeof(GPIO_INFO);
static P_GPIO_INFO pGPIOTable = &CSD_GPIO_Table[0];




/* **********************************************************************
// DESCRIPTION: Init Panel LEDs and Keys.
//
// INPUTS:
// RETURNS:
// **********************************************************************/
void HALInitPanelInterface(void)
{
	//Init GPIO pins used to interface with Panel
	HALInitGPIOPins(pGPIOTable, numPinsUsed);

}

void HALSetWhiteLED(BOOL turnOn)
{
	int old_level;

	//disable interrupts so that glitch in colors is minimized
    old_level = NU_Local_Control_Interrupts(NU_DISABLE_INTERRUPTS);
	if (turnOn)
	{
		setGPIOPin(*(pGPIOTable + PANEL_IDX_LED0), 1);
		setGPIOPin(*(pGPIOTable + PANEL_IDX_LED1), 1);
	}
	else
	{
		setGPIOPin(*(pGPIOTable + PANEL_IDX_LED0), 0);
		setGPIOPin(*(pGPIOTable + PANEL_IDX_LED1), 0);
	}
    NU_Local_Control_Interrupts(old_level);

}

void HALSetRedLED(BOOL turnOn)
{
	int old_level;

	//disable interrupts so that glitch in colors is minimized
    old_level = NU_Local_Control_Interrupts(NU_DISABLE_INTERRUPTS);
	if (turnOn)
	{
		setGPIOPin(*(pGPIOTable + PANEL_IDX_LED0), 1);
		setGPIOPin(*(pGPIOTable + PANEL_IDX_LED1), 0);
	}
	else
	{
		setGPIOPin(*(pGPIOTable + PANEL_IDX_LED0), 0);
		setGPIOPin(*(pGPIOTable + PANEL_IDX_LED1), 0);
	}
    NU_Local_Control_Interrupts(old_level);

}

void HALSetBlueLED(BOOL turnOn)
{
	int old_level;

	//disable interrupts so that glitch in colors is minimized
    old_level = NU_Local_Control_Interrupts(NU_DISABLE_INTERRUPTS);
	if (turnOn)
	{
		setGPIOPin(*(pGPIOTable + PANEL_IDX_LED0), 0);
		setGPIOPin(*(pGPIOTable + PANEL_IDX_LED1), 1);
	}
	else
	{
		setGPIOPin(*(pGPIOTable + PANEL_IDX_LED0), 0);
		setGPIOPin(*(pGPIOTable + PANEL_IDX_LED1), 0);
	}
    NU_Local_Control_Interrupts(old_level);

}


