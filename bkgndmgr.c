/************************************************************************
   PROJECT:        Horizon CSD
   MODULE NAME:    bkgndmgr.c

   DESCRIPTION:    This file contains the Postage By Phone
                   Services Task.  This task interfaces with
                   Data Center and other servers to provide
                   Postage By Phone services such as refills and refunds.

 ----------------------------------------------------------------------
                   Copyright (c) 2016 Pitney Bowes Inc.
                   37 Executive Drive
                   Danbury, Connecticut  06810
 ----------------------------------------------------------------------

*************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "..\include\exception.h"
#include "networking/externs.h"
#include "pbos.h"
#include "sys.h"
#include "datdict.h"
#include "bkgndmgr.h"
#include "errcode.h"
#include "utils.h"
#include "clock.h"
#include "oit.h"

#include "trmdefs.h"
#include "trmstrg.h"
#include "trm.h"

#include "sysdata.h"
#include "sysdatadefines.h"
#include "api_temporary.h"
#include "api_status.h"
#include "api_funds.h"

#include "networkmonitor.h"
#include "cm.h"
#include "fdrcm.h"
#include "datdict.h"

#define THISFILE "bkgndmgr.c"


extern PeriodDefDetails_t  trmPeriod;
extern BOOL     fDCAPUploadReqShown;
extern BOOL     fDCAPUploadDueShown;
extern BOOL     fUploadInProgress;
extern ulong 	trmMAXMailPiecesToUpload;
extern CM_SYSTEM_STATUS_t sSystemStatus;

extern BOOL bGoToReady;
extern uint32_t gTrmFatalError;
extern BOOL     fTrmFatalError;
DATETIME trmPkgEndDateTime;
char uploadWaitCount = 2;

long graceSeconds = MAX_UPLOAD_WAIT_PERIOD * (3600 * 24);
extern BOOL bMidnightProcessingStart;

BOOL fnGetUpdateNow (UINT8 bServiceCode, INT32 value, INT32 value2);

//Task entry point...
void BackgroundManager_Task(unsigned long argc, void *argv)
{
	//Local variables...
	INTERTASK msgIn;
	char *recvBuffer;
	const int recvBufferSize = 8192;
    char pLogBuf[40];
    STATUS result = 0;
	DATETIME dateTimeStamp;
	DATETIME dateTimeStampPrev;
	DATETIME emptyDate;
	BOOL bValidDate = false;
	BOOL DayChangedFlag = FALSE;
	long lDateDiff;
	int count = 0;

	memset(&emptyDate, 0, sizeof(emptyDate));

//	//this is to delay upload after midnight event
//	uploadWaitCount = rand() % 2; // Randomize uploads around 3 hrs after midnight
//	if(uploadWaitCount < 2)

	uploadWaitCount = 2;

    //Get the memory
    NU_Allocate_Memory(MEM_Cached, (void **)&recvBuffer, recvBufferSize, NU_NO_SUSPEND);
    if(!recvBuffer)
    {
    	taskException("Out of Memory!", THISFILE, __LINE__);
    }
    else
    {
        (void)memset(recvBuffer, 0, recvBufferSize);   //clear everything
    }

    msgIn.bMsgId = 0;

    fnSysLogTaskStart( "BackgroundManager_Task", BKGRND );

    fnSysLogTaskLoop( "BackgroundManager_Task", BKGRND );
    //Now hang out and process messages..........................
    while(1)
    {
    	if(OSReceiveIntertask(BKGRND, &msgIn, OS_SUSPEND) == SUCCESSFUL)
        {
            switch (msgIn.bMsgId)
            {
                case WRITE_TRX_FILES:
                {
                	dbgTrace(DBG_LVL_INFO, "WRITE_TRX_FILES:");
                	if(msgIn.IntertaskUnion.bByteData[0] == CMOSTrmPendingBucketToUpload)
                	{
                		int count = 0;
                		do{

                			result = trmWriteTrmBucket(msgIn.IntertaskUnion.bByteData[0]);
                			if((result != SUCCESS) && (bGoToReady == TRUE))
                			{
                				dbgTrace(DBG_LVL_INFO, "WRITE_TRX_FILES: trmWriteTrmBucket failed status:%d, bucket:%d ",result, msgIn.IntertaskUnion.bByteData[0]);
                				fnLogError( ERR_TYPE_NORMAL, ERROR_CLASS_TRM, E_DATA_WRITE_FAILED );
                				//Stop mail run
                				bGoToReady = FALSE;
                				fnStopRun(STOP_MAIN_ONLY, PM_STOP_NOW, GRACEFUL_STOP);
                			}
                			dbgTrace(DBG_LVL_INFO, "WRITE_TRX_FILES: trmWriteTrmBucket Attempt:%d ", count);
                			if( (count > 2) )
                			{
                				if(gTrmFatalError == 0)
                					gTrmFatalError  = E_DATA_WRITE_FAILED;

                				fTrmFatalError =  TRUE;
                				fnPostDisabledStatus(DCOND_TRM_FATAL_ERROR);
                				SendBaseEventToTablet(BASE_EVENT_TRM_FATAL_ERROR, NULL);

                				//return System error
                				fnLogError( ERR_TYPE_FATAL, ERROR_CLASS_TRM, E_DATA_WRITE_FAILED );

                				break;
                			}

                			count++;
                		}while(result != SUCCESS);


                	}
                	else
                	{
                		dbgTrace(DBG_LVL_INFO, "WRITE_TRX_FILES: Invalid bucket ID %d, %d ", msgIn.IntertaskUnion.bByteData[0], msgIn.IntertaskUnion.bByteData[0]);
                	}

                	if(result)
                	{
                		(void)sprintf(pLogBuf, "WRITE_TRX_FILES - bucket: %d Failed = %d", msgIn.IntertaskUnion.bByteData[0], result);
                	}
                	else
                	{
                		(void)sprintf(pLogBuf, "WRITE_TRX_FILES -bucket:%d Success", msgIn.IntertaskUnion.bByteData[0]);

                	}

                	fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
                	break;
                }
                case MIDNIGHT_ACTIVITY:
                {
                	uint8 bucket = msgIn.IntertaskUnion.bByteData[0];
                	dbgTrace(DBG_LVL_INFO, "MIDNIGHT_ACTIVITY:1 bucket:%d", bucket);

                	//Log if active bucket is different from bucket
                	if( CMOSTrmActiveBucket != bucket )
                		dbgTrace(DBG_LVL_INFO, "Active bucket is different from bucket to write: bucket:%d, CMOSTrmActiveBucket:%d ", bucket, CMOSTrmActiveBucket);

                	if( CFG_BUCKET0 != bucket && CFG_BUCKET1 != bucket )
					{
						dbgTrace(DBG_LVL_ERROR, "Error!: Invalid bucket: %d", bucket);
						//Use Active bucket
						bucket = CMOSTrmActiveBucket;
					}

                	CMOSTrmMidnightFlag = TRUE;
                	bGoToReady = FALSE;
                	trmSendMsgToBackground (CHECK_FOR_UPLOAD_DUE_REQUIRED, bucket, 0, 0);

                	//if running or stopping wait until it is completed
                	GetSysDateTime(&dateTimeStampPrev, ADDOFFSETS, GREGORIAN);
                	while ( (sSystemStatus.fRunning == TRUE) || (sSystemStatus.fStopping == TRUE) )
					{
                		NU_Sleep(20);
						GetSysDateTime(&dateTimeStamp, ADDOFFSETS, GREGORIAN);
						bValidDate = CalcDateTimeDifference( &dateTimeStampPrev, &dateTimeStamp, &lDateDiff );
						if( (bValidDate == false) || ((bValidDate == true) && (lDateDiff > 2)) ) //wait for 2 sec
							break;

					}

					dbgTrace(DBG_LVL_INFO, "MIDNIGHT_ACTIVITY:2");
                	//Check for upload in progress if upload is in progress wait until it is completed.
                	count = 0;
                	GetSysDateTime(&dateTimeStampPrev, ADDOFFSETS, GREGORIAN);

                	while( (CMOSTrmUploadState >= TRM_STATE_CREATE_XML) && (CMOSTrmUploadState <= TRM_STATE_CLEANUP) )
					{
                		NU_Sleep(20);
                		GetSysDateTime(&dateTimeStamp, ADDOFFSETS, GREGORIAN);
						bValidDate = CalcDateTimeDifference( &dateTimeStampPrev, &dateTimeStamp, &lDateDiff );
						if( (bValidDate == false) || ((bValidDate == true) && (lDateDiff > 120)) )
							break;
					}
					//
					dbgTrace(DBG_LVL_INFO, "MIDNIGHT_ACTIVITY:3");
                	//Following statemetn is moved from fnLocalMidniteEventHandler(void) due to 0x200 error
                	SetPrintedDateAdvanceDays( 0 );     /* clear current date advance amount */

                	if( (CMOSTrmUploadDueReqd & TRM_UPLOAD_AVAILABLE) == TRM_UPLOAD_AVAILABLE)
                	{
                		int count = 0;

                		dbgTrace(DBG_LVL_INFO, "MIDNIGHT_ACTIVITY: Uploads available mark Upload Due");

                		//Move all TRs to Flash if available
						//result = trmWriteTrmBucket(trmGetActiveBucket());
                		//bucket = trmGetActiveBucket();
                		do{

							result = trmWriteTrmBucket(bucket);
							if(result != SUCCESS)
							{
								dbgTrace(DBG_LVL_INFO, "MIDNIGHT_ACTIVITY: trmWriteTrmBucket failed status:%d, bucket:%d ",result, bucket);
								fnLogError( ERR_TYPE_NORMAL, ERROR_CLASS_TRM, E_DATA_WRITE_FAILED );
							}

							dbgTrace(DBG_LVL_INFO, "MIDNIGHT_ACTIVITY: trmWriteTrmBucket Attempt:%d , bucket: %d", count, bucket);
							if( (count > 2) )
							{
								if(gTrmFatalError  == 0)
									gTrmFatalError = E_DATA_WRITE_FAILED;

								fnPostDisabledStatus(DCOND_TRM_FATAL_ERROR);
								SendBaseEventToTablet(BASE_EVENT_TRM_FATAL_ERROR, NULL);

								//return System error
								fnLogError( ERR_TYPE_FATAL, ERROR_CLASS_TRM, E_DATA_WRITE_FAILED );

								break;
							}

							count++;

						}while(result != SUCCESS);

						fDCAPUploadDueShown = TRUE;
						CMOSTrmUploadDueReqd |= TRM_UPLOAD_DUE;

						SendBaseEventToTablet(BASE_EVENT_TX_UPLOAD_DUE, NULL);

                	}
                	else
                	{
                		dbgTrace(DBG_LVL_INFO, "MIDNIGHT_ACTIVITY: Nothing to upload");
                	}

                	break;
                }
                case CHECK_FOR_UPLOAD_DUE_REQUIRED:
                {
                	uint8 bucket = msgIn.IntertaskUnion.bByteData[0];
                	dbgTrace(DBG_LVL_INFO, "CHECK_FOR_UPLOAD_DUE_REQUIRED: bucket %d", bucket);

                	//Log if active bucket is different from bucket
                   	if( CMOSTrmActiveBucket != bucket )
                    	dbgTrace(DBG_LVL_INFO, "Active bucket is different from bucket to write: bucket:%d, CMOSTrmActiveBucket:%d ", bucket, CMOSTrmActiveBucket);


                	if( CFG_BUCKET0 != bucket && CFG_BUCKET1 != bucket )
					{
						dbgTrace(DBG_LVL_ERROR, "Error!: Invalid bucket: %d", bucket);
						//Use Active bucket
						bucket = CMOSTrmActiveBucket;
					}

                	//Check for date change and upload if date is changed
                	GetSysDateTime(&dateTimeStamp, ADDOFFSETS, GREGORIAN);

                	if(trmMAXMailPiecesToUpload > MAX_MP_TO_UPLOAD)
                		trmMAXMailPiecesToUpload = MAX_MP_TO_UPLOAD;

					if( CMOSTrmMailPiecesToUpload >= trmMAXMailPiecesToUpload )
					{
						//fDCAPUploadDueShown = TRUE;
						fDCAPUploadReqShown = TRUE;
						CMOSTrmUploadDueReqd |= TRM_UPLOAD_REQUIRED;

						//Stop run gracefully
						bGoToReady = FALSE;
						//fnStopRun(STOP_MAIN_ONLY, PM_STOP_NOW, GRACEFUL_STOP);

						//SendBaseEventToTablet(BASE_EVENT_TX_UPLOAD_REQUIRED, NULL);
						dbgTrace(DBG_LVL_INFO, "CHECK_FOR_UPLOAD_DUE_REQUIRED: Upload Required MP count:%d", CMOSTrmMailPiecesToUpload);
					}
                	//ignore if upload is in progress, CMOSTrmMpAfterUploadis cleared after upload


					bValidDate = CalcDateTimeDifference( &CMOSTrmTimeStampOfOldestMP, &dateTimeStamp, &lDateDiff );
					dbgTrace(DBG_LVL_INFO, "CHECK_FOR_UPLOAD_DUE_REQUIRED: CurTime: %02d%02d-%02d-%02d %02d:%02d:%02d, CMOSTrmTimeStampOfOldestMP: %02d%02d-%02d-%02d %02d:%02d:%02d, Valid:%d, Dif:%d",
																	dateTimeStamp.bCentury,
																	dateTimeStamp.bYear,
																	dateTimeStamp.bMonth,
																	dateTimeStamp.bDay,
																	dateTimeStamp.bHour,
																	dateTimeStamp.bMinutes,
																	dateTimeStamp.bSeconds,
																	////
																	CMOSTrmTimeStampOfOldestMP.bCentury,
																	CMOSTrmTimeStampOfOldestMP.bYear,
																	CMOSTrmTimeStampOfOldestMP.bMonth,
																	CMOSTrmTimeStampOfOldestMP.bDay,
																	CMOSTrmTimeStampOfOldestMP.bHour,
																	CMOSTrmTimeStampOfOldestMP.bMinutes,
																	CMOSTrmTimeStampOfOldestMP.bSeconds,
																	bValidDate, lDateDiff);
					if( (bValidDate == true) &&  					//valid dates
						(lDateDiff > TRM_UPLOAD_TIME_REQUIRED) ) 	//Time diff is greater that 72 hrs
					{
						//Day changed
						DayChangedFlag = TRUE;

						//Check for mail availability
						if( ((CMOSTrmUploadDueReqd & TRM_UPLOAD_AVAILABLE) == TRM_UPLOAD_AVAILABLE) &&
							((CMOSTrmUploadDueReqd & TRM_UPLOAD_REQUIRED) != TRM_UPLOAD_REQUIRED) )
						{
							fDCAPUploadReqShown = TRUE;
							CMOSTrmUploadDueReqd |= TRM_UPLOAD_REQUIRED;

							//Stop run gracefully
							bGoToReady = FALSE;
							//fnStopRun(STOP_MAIN_ONLY, PM_STOP_NOW, GRACEFUL_STOP);

							//SendBaseEventToTablet(BASE_EVENT_UPLOAD_REQ, NULL);
							dbgTrace(DBG_LVL_INFO, "CHECK_FOR_UPLOAD_DUE_REQUIRED: Upload Required time");
						}
					}
					else if((bValidDate == true) &&
							(lDateDiff > TRM_UPLOAD_TIME_DUE) ) 								//Last updload is more than 24 hrs

					{
						//Day changed
						DayChangedFlag = TRUE;

						//Check for mail availability
						if( ((CMOSTrmUploadDueReqd & TRM_UPLOAD_AVAILABLE) == TRM_UPLOAD_AVAILABLE) &&
							((CMOSTrmUploadDueReqd & TRM_UPLOAD_DUE) != TRM_UPLOAD_DUE) &&		// Due is not set
							((CMOSTrmUploadDueReqd & TRM_UPLOAD_REQUIRED) != TRM_UPLOAD_REQUIRED) ) //Reqd is not set
						{
							fDCAPUploadDueShown = TRUE;
							CMOSTrmUploadDueReqd |= TRM_UPLOAD_DUE;
							//SendBaseEventToTablet(BASE_EVENT_TX_UPLOAD_DUE, NULL);
							dbgTrace(DBG_LVL_INFO, "CHECK_FOR_UPLOAD_DUE_REQUIRED: Upload Due");
						}
					}
					else
					{
						dbgTrace(DBG_LVL_INFO, "CHECK_FOR_UPLOAD_DUE_REQUIRED-CMOSTrmUploadDueReqd: %d", CMOSTrmUploadDueReqd);
					}


					if( (CMOSTrmMidnightFlag == TRUE) || (DayChangedFlag == TRUE) ||
						(memcmp(&CMOSTrmTimeStampOfMPIDReset, &dateTimeStamp, offsetof(DATETIME, bDayOfWeek)) != 0))
					{
						dbgTrace(DBG_LVL_INFO, "CHECK_FOR_UPLOAD_DUE_REQUIRED-Date Change-MPID ResetDate: %02d%02d-%02d-%02d %02d:%02d:%02d",
													CMOSTrmTimeStampOfMPIDReset.bCentury,
													CMOSTrmTimeStampOfMPIDReset.bYear,
													CMOSTrmTimeStampOfMPIDReset.bMonth,
													CMOSTrmTimeStampOfMPIDReset.bDay,
													CMOSTrmTimeStampOfMPIDReset.bHour,
													CMOSTrmTimeStampOfMPIDReset.bMinutes,
													CMOSTrmTimeStampOfMPIDReset.bSeconds);


						//At powerup check for Day change and set mp id to 1
						//Makesure that it happens only once in a day
						//avoid invalid Dates incase of ibutton connectivity issues/invalid dates.
						if(memcmp(&CMOSTrmTimeStampOfMPIDReset, &dateTimeStamp, offsetof(DATETIME, bDayOfWeek)) < 0)
						{
							//Move all TRs to Flash
							result = trmWriteTrmBucket(bucket);
							if(result != SUCCESS)
							{
								dbgTrace(DBG_LVL_INFO, "CHECK_FOR_UPLOAD_DUE_REQUIRED-WriteBucket failed: %d", result);
							}

							dbgTrace(DBG_LVL_INFO, "CHECK_FOR_UPLOAD_DUE_REQUIRED: CMOSTrmMidnightFlag resetting mp id, seq count");
							CMOSTrmDcapFileSeqCount = 1;
							reset_cur_mp_id();
							memcpy(&CMOSTrmTimeStampOfMPIDReset, &dateTimeStamp, sizeof(dateTimeStamp));
						}

						DayChangedFlag = FALSE; // clear flag in case it was set
					}

					//Check for Upload due /Upload required and send necessary event
					if((CMOSTrmUploadDueReqd & TRM_UPLOAD_REQUIRED) == TRM_UPLOAD_REQUIRED)
					{
						fDCAPUploadReqShown = TRUE;
						SendBaseEventToTablet(BASE_EVENT_TX_UPLOAD_REQUIRED, NULL);
					}
					else if((CMOSTrmUploadDueReqd & TRM_UPLOAD_DUE) == TRM_UPLOAD_DUE)
					{
						fDCAPUploadDueShown = TRUE;
						SendBaseEventToTablet(BASE_EVENT_TX_UPLOAD_DUE, NULL);
					}

					if (CMOSTrmMidnightFlag)
					{
						CMOSTrmMidnightFlag = FALSE;
						bMidnightProcessingStart = FALSE;
						// Send midnight event after upload required so that tablet can know if it should go to ready upon receipt
						SendBaseEventToTablet(BASE_EVENT_MIDNIGHT, NULL);
						}
		            break;
                }
                case PREPARE_TRX_PKG_FILE:
				{
					INT32 bucket;
					memcpy(&bucket, &msgIn.IntertaskUnion.bByteData[1], sizeof(INT32));
					dbgTrace(DBG_LVL_INFO, "PREPARE_TRX_PKG_FILE: preparing new package: state %d, period:%d bucket:%d", trmGetUploadState(), msgIn.IntertaskUnion.bByteData[0], bucket);

					//Log if active bucket is different from bucket
			       	if( CMOSTrmActiveBucket != bucket )
			        	dbgTrace(DBG_LVL_INFO, "Active bucket is different from bucket to write: bucket:%d, CMOSTrmActiveBucket:%d ", bucket, CMOSTrmActiveBucket);

			       	if( CFG_BUCKET0 != bucket && CFG_BUCKET1 != bucket )
					{
						dbgTrace(DBG_LVL_ERROR, "Error!: Invalid bucket: %d", bucket);
						//Use Active bucket
						bucket = CMOSTrmActiveBucket;
					}

                	count = 0;
					//Complete any pending writes in the background
					dbgTrace(DBG_LVL_INFO, "PREPARE_TRX_PKG_FILE-CMOSTrmPendingBucketToUpload :%d", CMOSTrmPendingBucketToUpload);
					while ( CMOSTrmPendingBucketToUpload != -1 )
					{
						  NU_Sleep(20);
						  count++;
						  if(count ==150) //approx 2 sec wait for completion
								 break;
					}

					//If previous upload interrupted due to failure or power failure try to complete confirmation
					if(trmGetUploadState() >= TRM_STATE_UPLOAD_CONFIRMED )
					{
						//If there is any pending uploading confirmation activity complete it.
						//this could happen due to communication problem/power failure etc.
						result = trmProcessTRUploadConfirmation();
						if(result != SUCCESS)
							dbgTrace(DBG_LVL_INFO, "PREPARE_TRX_PKG_FILE: Failed to complete pending cleanup-result:%d\n", result);
					}



					result = trmCompletePendingWrites();
					if(result != SUCCESS)
						dbgTrace(DBG_LVL_INFO, "PREPARE_TRX_PKG_FILE-Attempts to complete pending writes failed result:%d", result);
					else
					{
						//Move any transaction records to flash
						count = 0;
						do{
							if(count++ > 2)
							{
								if(	gTrmFatalError == 0)
								{
									gTrmFatalError = E_DATA_WRITE_FAILED;
								}
								fTrmFatalError =  TRUE;

								dbgTrace(DBG_LVL_INFO, "PREPARE_TRX_PKG_FILE-Attempt:%d, gTrmFatalError :%d", count, gTrmFatalError );
								fnLogError( ERR_TYPE_FATAL, ERROR_CLASS_TRM, E_DATA_WRITE_FAILED );

								fnPostDisabledStatus(DCOND_TRM_FATAL_ERROR);
								SendBaseEventToTablet(BASE_EVENT_TRM_FATAL_ERROR, NULL);

								break;
							}

							result = trmWriteTrmBucket(bucket);
							if(result != SUCCESS)
							{
								dbgTrace(DBG_LVL_INFO, "PREPARE_TRX_PKG_FILE-Attempt:%d, result:%d", count, result);
								fnLogError( ERR_TYPE_NORMAL, ERROR_CLASS_TRM, E_DATA_WRITE_FAILED );
							}

						}while(result != SUCCESS);
					}

                	//Prepare XML package
                	trmSetUploadState(TRM_STATE_CREATE_XML);
                	if( (msgIn.IntertaskUnion.bByteData[0]) >= 0 && (msgIn.IntertaskUnion.bByteData[0] <= 3) )
                	{
                		result = trmPrepareXMLPackageFile(msgIn.IntertaskUnion.bByteData[0]);
                	}
                	else
                		result = trmPrepareXMLPackageFile(0);


                	if(result != SUCCESS)
                	{
                		dbgTrace(DBG_LVL_ERROR, "PREPARE_TRX_PKG_FILE: failed to prepare package : result %d ", result);
                	}

                	break;
				}
                case UPLOAD_TIMEOUT:
                {
                	dbgTrace(DBG_LVL_ERROR, "UPLOAD_TIMEOUT: ");
                	trmUploadFailed(ECSYS_MESSAGE_TIMEOUT_OTHER);

                	break;
                }
                default:
                {
                    sprintf(pLogBuf, "Unexpected message Src %d ID -> %d", msgIn.bSource, msgIn.bMsgId);
                    if(msgIn.IntertaskUnion.bByteData[0] == 0)
                    fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
                    break;
                }
            }
        }

    }

    //Free the buffers...
	if(NU_Deallocate_Memory(recvBuffer) != NU_SUCCESS)
    {
		sprintf(pLogBuf, "BackgroundManager - failed \n");
		fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
    }

}



